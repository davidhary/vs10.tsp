''' <summary>
''' Encapsulate the node information.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/02/2009" by="David" revision="3.0.3348.x">
''' Created
''' </history>
Public Class NodeEntity

    Implements INodeEntity

    ''' <summary>
    ''' Constructs the class.
    ''' </summary>
    ''' <param name="number">Specifies the node number.</param>
    ''' <param name="modelNumber">Specified the model number for the node.</param>
    Public Sub New(ByVal number As Integer, ByVal modelNumber As String, ByVal controllerNodeNumber As Integer)
        MyBase.New()
        Me._controllerNodeNumber = controllerNodeNumber
        Me._number = number
        Me._uniqueKey = CStr(number)
        Me._modelNumber = modelNumber
        Me._instrumentModelFamily = ParseModelNumber(modelNumber)
        Me._firmwareVersion = ""
        Me._serialNumber = ""
    End Sub

    ''' <summary>
    ''' Parses the model number to get the model family
    ''' </summary>
    Public Shared Function ParseModelNumber(ByVal value As String) As Tsp.InstrumentModelFamily
        For Each item As Tsp.InstrumentModelFamily In [Enum].GetValues(GetType(Tsp.InstrumentModelFamily))
            If item <> 0 AndAlso ScriptEntity.IsModelMatch(value, ModelFamilyMask(item)) Then
                Return item
            End If
        Next
        Return Tsp.InstrumentModelFamily.None
    End Function

    ''' <summary>
    ''' Returns the mask for the model family.
    ''' </summary>
    Public Shared Function ModelFamilyMask(ByVal value As Tsp.InstrumentModelFamily) As String
        Select Case value
            Case Tsp.InstrumentModelFamily.K2600
                Return "26%%"
            Case Tsp.InstrumentModelFamily.K2600A
                Return "26%%%"
            Case Tsp.InstrumentModelFamily.K3700
                Return "37*"
            Case Else
                Throw New ArgumentOutOfRangeException("value", value, "Unhandled model family")
        End Select
    End Function

    Private _bootScriptSaveRequired As Boolean
    ''' <summary>
    ''' Gets or sets the condition to indicate that the boot script must be re-saved because a  
    ''' script reference changes as would happened if a new binary script was created with a table
    ''' reference that differs from the table reference of the previous script that was 
    ''' used in the previous boot script. 
    ''' </summary>
    Public Property BootScriptSaveRequired() As Boolean Implements INodeEntity.BootScriptSaveRequired
        Get
            Return Me._bootScriptSaveRequired
        End Get
        Set(ByVal value As Boolean)
            Me._bootScriptSaveRequired = value
        End Set
    End Property

    Private _controllerNodeNumber As Integer
    ''' <summary>
    ''' Returns the controller node number.
    ''' </summary>
    Public ReadOnly Property ControllerNodeNumber() As Integer Implements INodeEntity.ControllerNodeNumber
        Get
            Return Me._controllerNodeNumber
        End Get
    End Property

    Private _firmwareVersion As String
    ''' <summary>
    ''' Gets or sets the firmware version.
    ''' </summary>
    Public Property FirmwareVersion() As String Implements INodeEntity.FirmwareVersion
        Get
            Return Me._firmwareVersion
        End Get
        Set(ByVal value As String)
            Me._firmwareVersion = value
        End Set
    End Property

    Private _instrumentModelFamily As InstrumentModelFamily
    ''' <summary>
    ''' Gets the <see cref="isr.Tsp.InstrumentModelFamily">instrument model family.</see> 
    ''' </summary>
    ReadOnly Property InstrumentModelFamily() As InstrumentModelFamily Implements INodeEntity.InstrumentModelFamily
        Get
            Return Me._instrumentModelFamily
        End Get
    End Property

    Public ReadOnly Property IsController() As Boolean Implements INodeEntity.IsController
        Get
            Return Me._number = Me.ControllerNodeNumber
        End Get
    End Property

    Private _modelNumber As String
    Public ReadOnly Property ModelNumber() As String Implements INodeEntity.ModelNumber
        Get
            Return Me._modelNumber
        End Get
    End Property

    Private _number As Integer
    Public ReadOnly Property Number() As Integer Implements INodeEntity.Number
        Get
            Return Me._number
        End Get
    End Property

    Private _serialNumber As String
    ''' <summary>
    ''' Gets or sets the serial number.
    ''' </summary>
    Public Property SerialNumber() As String Implements INodeEntity.SerialNumber
        Get
            Return Me._serialNumber
        End Get
        Set(ByVal value As String)
            Me._serialNumber = value
        End Set
    End Property

    Private _uniqueKey As String
    Public ReadOnly Property UniqueKey() As String Implements INodeEntity.UniqueKey
        Get
            Return Me._uniqueKey
        End Get
    End Property

End Class
