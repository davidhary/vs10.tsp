Imports isr.Core
''' <summary>
''' Encapsulate the script information.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/02/2009" by="David" revision="3.0.3348.x">
''' Created
''' </history>
Public Class ScriptEntity

    Implements IScriptEntity

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private Sub New()

        MyBase.new()
        Me._name = ""
        Me._modelMask = ""
        Me._embeddedFirmwareVersion = ""
        'Me._isBootScript = False
        'Me._isSupportScript = False
        'Me._isPrimaryScript = False
        Me._fileName = ""
        Me._firmwareVersionCommand = ""
        Me.namespaceListSetter("")
        Me._releasedFirmwareVersion = ""
        'Me._requiresReadParseWrite = False
        'Me._savedToFile = False
        Me._source = ""
        Me._sourceFormat = ScriptFileFormats.None
        Me._timeout = 10000

    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="name">Specifies the script name.</param>
    ''' <param name="modelMask">Specifies the model families for this script.</param>
    Public Sub New(ByVal name As String, ByVal modelMask As String)

        Me.New()
        Me._name = name
        Me._modelMask = modelMask

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._source = ""

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Returns the compressed code prefix.
    ''' </summary>
    Public Shared ReadOnly Property CompressedPrefix() As String
        Get
            Return "<COMPRESSED>"
        End Get
    End Property

    ''' <summary>
    ''' Returns the compressed code suffix.
    ''' </summary>
    Public Shared ReadOnly Property CompressedSuffix() As String
        Get
            Return "</COMPRESSED>"
        End Get
    End Property


#End Region

#Region " FIRMWARE "

    Private _embeddedFirmwareVersion As String
    Public Property EmbeddedFirmwareVersion() As String Implements IScriptEntity.EmbeddedFirmwareVersion
        Get
            Return Me._embeddedFirmwareVersion
        End Get
        Set(ByVal value As String)
            Me._embeddedFirmwareVersion = value
        End Set
    End Property

    Private _releasedFirmwareVersion As String
    Public Property ReleasedFirmwareVersion() As String Implements IScriptEntity.ReleasedFirmwareVersion
        Get
            Return Me._releasedFirmwareVersion
        End Get
        Set(ByVal value As String)
            Me._releasedFirmwareVersion = value
        End Set
    End Property

    Private _firmwareVersionCommand As String
    Public Property FirmwareVersionCommand() As String Implements IScriptEntity.FirmwareVersionCommand
        Get
            Return Me._firmwareVersionCommand
        End Get
        Set(ByVal value As String)
            Me._firmwareVersionCommand = value
        End Set
    End Property

    Public Function FirmwareVersionDefined(ByVal instrument As ITspInstrument) As Boolean Implements IScriptEntity.FirmwareVersionDefined
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        Return Not instrument.IsNil(Me._firmwareVersionCommand.TrimEnd("()".ToCharArray))
    End Function

    Public Function FirmwareVersionDefined(ByVal nodeNumber As Integer, ByVal instrument As ITspInstrument) As Boolean Implements IScriptEntity.FirmwareVersionDefined
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        Return Not instrument.IsNil(nodeNumber, Me._firmwareVersionCommand.TrimEnd("()".ToCharArray))
    End Function

    ''' <summary>
    ''' Returns true if the firmware version command exists.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="instrument">Specifies reference to the instrument</param>
    Public Function FirmwareVersionDefined(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean Implements IScriptEntity.FirmwareVersionDefined
        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        If node.IsController Then
            Return Me.FirmwareVersionDefined(instrument)
        Else
            Return Me.FirmwareVersionDefined(node.Number, instrument)
        End If
    End Function

    Private Const missingEmbeddedFirmwareVersion As String = "nil"
    Public Function ReadFirmwareVersion(ByVal instrument As ITspInstrument) As String Implements IScriptEntity.ReadFirmwareVersion
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        If Me.FirmwareVersionDefined(instrument) Then
            Me._embeddedFirmwareVersion = instrument.QueryTrimEnd("_G.print({0})", Me._firmwareVersionCommand)
        Else
            Me._embeddedFirmwareVersion = missingEmbeddedFirmwareVersion
        End If
        Return Me._embeddedFirmwareVersion
    End Function

    Public Function ReadFirmwareVersion(ByVal nodeNumber As Integer, ByVal instrument As ITspInstrument) As String Implements IScriptEntity.ReadFirmwareVersion
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        Dim synopsis As String = "Read Firmware Version"
        If Me.FirmwareVersionDefined(nodeNumber, instrument) Then
            Me._embeddedFirmwareVersion = instrument.GetString(nodeNumber, Me._firmwareVersionCommand)
        Else
            Me._embeddedFirmwareVersion = missingEmbeddedFirmwareVersion
        End If
        If instrument.ReportDeviceOperationOkay(nodeNumber, synopsis, "failed reading firmware version on node {0}", nodeNumber) Then
        End If
        Return Me._embeddedFirmwareVersion
    End Function

    ''' <summary>
    ''' Reads the embedded firmware version from a remote node and saves it
    ''' to <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public Function ReadFirmwareVersion(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As String Implements IScriptEntity.ReadFirmwareVersion
        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        If node.IsController Then
            Return ReadFirmwareVersion(instrument)
        Else
            Return ReadFirmwareVersion(node.Number, instrument)
        End If
    End Function

    ''' <summary>
    ''' Validates the released against the embedded firmware.
    ''' </summary>
    Public Function ValidateFirmware() As FirmwareVersionStatus Implements IScriptEntity.ValidateFirmware
        If String.IsNullOrWhiteSpace(Me._releasedFirmwareVersion) Then
            Return FirmwareVersionStatus.ReferenceUnknown
        ElseIf String.IsNullOrWhiteSpace(Me._embeddedFirmwareVersion) Then
            Return FirmwareVersionStatus.Unknown
        ElseIf Me._embeddedFirmwareVersion = missingEmbeddedFirmwareVersion Then
            Return FirmwareVersionStatus.Missing
        Else
            Select Case New System.Version(Me._embeddedFirmwareVersion).CompareTo(New System.Version(Me._releasedFirmwareVersion))
                Case Is > 0
                    Return FirmwareVersionStatus.Newer
                Case 0
                    Return FirmwareVersionStatus.Current
                Case Else
                    Return FirmwareVersionStatus.Older
            End Select
        End If
    End Function

#End Region

#Region " FILE "

    Private _fileName As String
    <CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")>
    Public Property FileName() As String Implements IScriptEntity.FileName
        Get
            Return Me._fileName
        End Get
        Set(ByVal value As String)
            Me._fileName = value
            If String.IsNullOrWhiteSpace(value) Then
                Me._fileName = ""
            End If
        End Set
    End Property

    Private _fileFormat As ScriptFileFormats
    ''' <summary>
    ''' Gets or sets the file format which to write. 
    ''' Defaults to uncompressed format. 
    ''' </summary>
    Public Property FileFormat() As ScriptFileFormats Implements IScriptEntity.FileFormat
        Get
            Return Me._fileFormat
        End Get
        Set(ByVal value As ScriptFileFormats)
            Me._fileFormat = value
        End Set
    End Property

    Private _savedToFile As Boolean
    ''' <summary>
    ''' Gets or sets the condition indicating if the script was already saved to file.
    ''' This property is set true if the script source is already in the correct format so no
    ''' new file needs to be saved.
    ''' </summary>
    Public Property SavedToFile() As Boolean Implements IScriptEntity.SavedToFile
        Get
            Return Me._savedToFile
        End Get
        Set(ByVal value As Boolean)
            Me._savedToFile = value
        End Set
    End Property

    Private _sourceFormat As ScriptFileFormats
    ''' <summary>
    ''' Gets the format of the contents that was used to set the source. 
    ''' </summary>
    ReadOnly Property SourceFormat() As ScriptFileFormats Implements IScriptEntity.SourceFormat
        Get
            Return Me._sourceFormat
        End Get
    End Property

#End Region

#Region " SCRIPT MANAGEMENT "

    Private _isDeleted As Boolean
    ''' <summary>
    ''' Gets or sets the condition inditing if this script was deleted. 
    ''' </summary>
    Public Property IsDeleted() As Boolean Implements IScriptEntity.IsDeleted
        Get
            Return Me._isDeleted
        End Get
        Set(ByVal value As Boolean)
            Me._isDeleted = value
        End Set
    End Property

    Private _requiresDeletion As Boolean
    ''' <summary>
    ''' Gets or sets the condition inditing if this scripts needs to be deleted on
    ''' the instrument. 
    ''' At this time this is used in design mode. It might be used later for refreshing the stored scripts.
    ''' </summary>
    Public Property RequiresDeletion() As Boolean Implements IScriptEntity.RequiresDeletion
        Get
            Return Me._requiresDeletion
        End Get
        Set(ByVal value As Boolean)
            Me._requiresDeletion = value
        End Set
    End Property

    Private _requiresReadParseWrite As Boolean
    ''' <summary>
    ''' Returns true the script requires update from file.
    ''' </summary>
    Public Function RequiresReadParseWrite() As Boolean Implements IScriptEntity.RequiresReadParseWrite
        Return Me._requiresReadParseWrite
    End Function

#End Region

#Region " MODEL AMANGEMENT "

    Private _modelMask As String
    ''' <summary>
    ''' Specifies the family of instrument models for this script
    ''' </summary>
    Public ReadOnly Property ModelMask() As String Implements IScriptEntity.ModelMask
        Get
            Return Me._modelMask
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the <paramref name="model">model</paramref> matches the <see cref="ModelMask">mask</see>.
    ''' </summary>
    ''' <param name="model">Actual mode.</param>
    ''' <param name="mask">Mode mask using '%' to signify ignored characters and * to specify wildcard suffix.</param>
    Public Shared Function IsModelMatch(ByVal model As String, ByVal mask As String) As Boolean

        Dim wildcard As Char = "*"c
        Dim ignore As Char = "%"c
        If String.IsNullOrWhiteSpace(mask) Then
            Return True
        ElseIf String.IsNullOrWhiteSpace(model) Then
            Return False
        ElseIf mask.Contains(wildcard) Then
            Dim length As Integer = mask.IndexOf(wildcard)
            Dim m As Char() = mask.Substring(0, length).ToCharArray
            Dim candidate As Char() = model.Substring(0, length).ToCharArray
            For i As Integer = 0 To m.Length - 1
                Dim c As Char = m(i)
                If c <> ignore AndAlso c <> candidate(i) Then
                    Return False
                End If
            Next
        ElseIf mask.Length <> model.Length Then
            Return False
        Else
            Dim m As Char() = mask.ToCharArray
            Dim candidate As Char() = model.ToCharArray
            For i As Integer = 0 To m.Length - 1
                Dim c As Char = m(i)
                If c <> ignore AndAlso c <> candidate(i) Then
                    Return False
                End If
            Next
        End If
        Return True
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="model">model</paramref> matches the <see cref="ModelMask">mask</see>.
    ''' </summary>
    Public Function IsModelMatch(ByVal model As String) As Boolean Implements IScriptEntity.IsModelMatch
        Return ScriptEntity.IsModelMatch(model, Me._modelMask)
    End Function

#End Region

#Region " SCRIPT SPECIFICATIONS "

    Private _isBinaryScript As Boolean
    ''' <summary>
    ''' Returns true if this is a binary script. This is determined when setting the source.
    ''' </summary>
    Public ReadOnly Property IsBinaryScript() As Boolean Implements IScriptEntity.IsBinary
        Get
            Return Me._isBinaryScript
        End Get
    End Property

    Private _isBootScript As Boolean
    ''' <summary>
    ''' Gets or sets the script as a boot script. 
    ''' Also turns off <see cref="RequiresReadParseWrite">requiring reading a new script from file.</see>
    ''' </summary>

    Public Property IsBootScript() As Boolean Implements IScriptEntity.IsBootScript
        Get
            Return Me._isBootScript
        End Get
        Set(ByVal value As Boolean)
            Me._isBootScript = value
        End Set
    End Property

    Private _isPrimaryScript As Boolean
    Public Property IsPrimaryScript() As Boolean Implements IScriptEntity.IsPrimaryScript
        Get
            Return Me._isPrimaryScript
        End Get
        Set(ByVal value As Boolean)
            Me._isPrimaryScript = value
        End Set
    End Property

    Private _isSupportScript As Boolean
    Public Property IsSupportScript() As Boolean Implements IScriptEntity.IsSupportScript
        Get
            Return Me._isSupportScript
        End Get
        Set(ByVal value As Boolean)
            Me._isSupportScript = value
        End Set
    End Property

    Private _name As String
    Public ReadOnly Property Name() As String Implements IScriptEntity.Name
        Get
            Return Me._name
        End Get
    End Property

    Private _source As String
    Public Property Source() As String Implements IScriptEntity.Source
        Get
            Return Me._source
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then value = ""
            Me._source = ""
            Me._isBinaryScript = False
            Me._sourceFormat = ScriptFileFormats.None
            Me._requiresReadParseWrite = Me._releasedFirmwareVersion.Trim.StartsWith("+", True, Globalization.CultureInfo.CurrentCulture)
            Me._sourceFormat = ScriptFileFormats.None

            If Not Me._requiresReadParseWrite Then

                If value.StartsWith(ScriptEntity.CompressedPrefix, False, Globalization.CultureInfo.CurrentCulture) Then
                    Dim fromIndex As Integer = value.IndexOf(ScriptEntity.CompressedPrefix, StringComparison.OrdinalIgnoreCase) +
                                               ScriptEntity.CompressedPrefix.Length
                    Dim toIndex As Integer = value.IndexOf(ScriptEntity.CompressedSuffix, StringComparison.OrdinalIgnoreCase) - 1
                    Me._source = value.Substring(fromIndex, toIndex - fromIndex + 1)
                    Me._source = Me._source.Decompress()
                    Me._sourceFormat = Me._sourceFormat Or ScriptFileFormats.Compressed
                Else
                    Me._source = value
                End If
                If Not String.IsNullOrWhiteSpace(Me._source) Then
                    Dim snippet As String = Me._source.Substring(0, 50).Trim
                    Me._isBinaryScript = snippet.StartsWith("{", True, Globalization.CultureInfo.CurrentCulture) OrElse
                                         snippet.StartsWith("loadstring", True, Globalization.CultureInfo.CurrentCulture) OrElse
                                         snippet.StartsWith("loadscript", True, Globalization.CultureInfo.CurrentCulture)
                End If
                If Not Me._source.EndsWith(" ", True, Globalization.CultureInfo.CurrentCulture) Then
                    Me._source = Me._source.Insert(Me._source.Length, " ")
                End If

                If Me._isBinaryScript Then
                    Me._sourceFormat = Me._sourceFormat Or ScriptFileFormats.Binary
                End If

            End If

            ' tag file as saved if source format and file format match.
            Me._savedToFile = Me._sourceFormat = Me._fileFormat

        End Set

    End Property

    Private _timeout As Integer
    Public Property Timeout() As Integer Implements IScriptEntity.Timeout
        Get
            Return Me._timeout
        End Get
        Set(ByVal value As Integer)
            Me._timeout = value
        End Set
    End Property

    Private Sub namespaceListSetter(ByVal value As String)
        Me._namespaceList = value
        If String.IsNullOrWhiteSpace(value) Then
            Me._namespaces = New String() {}
        Else
            Me._namespaces = Me._namespaceList.Split(","c)
        End If
    End Sub

    Private _namespaceList As String
    Public Property NamespaceList() As String Implements IScriptEntity.NamespaceList
        Get
            Return Me._namespaceList
        End Get
        Set(ByVal value As String)
            namespaceListSetter(value)
        End Set
    End Property

    Private _namespaces() As String
    Public Function Namespaces() As String() Implements IScriptEntity.Namespaces
        Return Me._namespaces
    End Function

#End Region

End Class

