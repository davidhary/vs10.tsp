''' <summary>
''' Provides an interface for a Node Entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/02/2009" by="David" revision="3.0.3348.x">
''' Created
''' </history>
Public Interface INodeEntity

    ''' <summary>
    ''' Gets or sets the condition to indicate that the boot script must be re-saved because a  
    ''' script reference changes as would happened if a new binary script was created with a table
    ''' reference that differs from the table reference of the previous script that was 
    ''' used in the previous boot script. 
    ''' </summary>
    Property BootScriptSaveRequired() As Boolean

    ''' <summary>
    ''' Gets the controller node number.
    ''' </summary>
    ReadOnly Property ControllerNodeNumber() As Integer

    ''' <summary>
    ''' Gets the <see cref="isr.Tsp.InstrumentModelFamily">instrument model family.</see> 
    ''' </summary>
    ReadOnly Property InstrumentModelFamily() As InstrumentModelFamily

    ''' <summary>
    ''' Gets the condition telling if this is the controller node.
    ''' </summary>
    ReadOnly Property IsController() As Boolean

    ''' <summary>
    ''' Gets the node number.
    ''' </summary>
    ReadOnly Property Number() As Integer

    ''' <summary>
    ''' Gets or sets the firmware version.
    ''' </summary>
    Property FirmwareVersion() As String

    ''' <summary>
    ''' Gets the model number for the node.
    ''' </summary>
    ReadOnly Property ModelNumber() As String

    ''' <summary>
    ''' Gets or sets the serial number.
    ''' </summary>
    Property SerialNumber() As String

    ''' <summary>
    ''' Makes this a collectible item.
    ''' </summary>
    ReadOnly Property UniqueKey() As String

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="INodeEntity">Node entity</see> 
''' items keyed by the <see cref="INodeEntity.UniqueKey">unique key.</see>
''' </summary>
Public Class NodeEntityCollection
    Inherits NodeEntityBaseCollection(Of INodeEntity)
    Protected Overloads Overrides Function GetKeyForItem(ByVal item As INodeEntity) As String
        Return MyBase.GetKeyForItem(item)
    End Function
End Class

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IScriptEntity">script entity</see> 
''' items keyed by the <see cref="INodeEntity.UniqueKey">unique key.</see>
''' </summary>
Public Class NodeEntityBaseCollection(Of TItem As INodeEntity)
    Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

    Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
        Return item.UniqueKey
    End Function

End Class

