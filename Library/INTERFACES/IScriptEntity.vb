''' <summary>
''' Provides an interface for a Script Entity.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/02/2009" by="David" revision="3.0.3348.x">
''' Created
''' </history>
Public Interface IScriptEntity

    Inherits IDisposable

#Region " FIRMWARE "

    ''' <summary>
    ''' Gets or sets the firmware version embedded in this script.
    ''' </summary>
    Property EmbeddedFirmwareVersion() As String

    ''' <summary>
    ''' Gets or sets the firmware version released for this script.
    ''' </summary>
    Property ReleasedFirmwareVersion() As String

    ''' <summary>
    ''' Gets or sets the command for retrieving the firmware version for this script.
    ''' </summary>
    Property FirmwareVersionCommand() As String

    ''' <summary>
    ''' Returns true if the firmware version command exists.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function FirmwareVersionDefined(ByVal instrument As ITspInstrument) As Boolean

    ''' <summary>
    ''' Returns true if the firmware version command exists.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function FirmwareVersionDefined(ByVal nodeNumber As Integer, ByVal instrument As ITspInstrument) As Boolean

    ''' <summary>
    ''' Returns true if the firmware version command exists.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function FirmwareVersionDefined(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean

    ''' <summary>
    ''' Reads the embedded firmware version and saves it
    ''' to <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function ReadFirmwareVersion(ByVal instrument As ITspInstrument) As String

    ''' <summary>
    ''' Reads the embedded firmware version from a remote node and saves it
    ''' to <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function ReadFirmwareVersion(ByVal nodeNumber As Integer, ByVal instrument As ITspInstrument) As String

    ''' <summary>
    ''' Reads the embedded firmware version from a remote node and saves it
    ''' to <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    Function ReadFirmwareVersion(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As String

    ''' <summary>
    ''' Validates the released against the embedded firmware.
    ''' </summary>
    Function ValidateFirmware() As FirmwareVersionStatus

#End Region

#Region " FILE "

    ''' <summary>
    ''' Gets or sets the script file name. 
    ''' </summary>
    Property FileName() As String

    ''' <summary>
    ''' Gets or sets the file format which to write. 
    ''' Defaults to uncompressed format. 
    ''' </summary>
    Property FileFormat() As ScriptFileFormats

    ''' <summary>
    ''' Gets or sets the condition indicating if the script was already saved to file.
    ''' This property is set true if the script source is already in the correct format so no
    ''' new file needs to be saved.
    ''' </summary>
    Property SavedToFile() As Boolean

    ''' <summary>
    ''' Gets the format of the contents that was used to set the source. 
    ''' </summary>
    ReadOnly Property SourceFormat() As ScriptFileFormats

#End Region

#Region " SCRIPT SPECIFICATIONS "

    ''' <summary>
    ''' Returns true if the source of the script is in binary format. This is determined when setting the source.
    ''' </summary>
    ReadOnly Property IsBinary() As Boolean

    ''' <summary>
    ''' Gets or sets the indicator telling if this is a boot script.
    ''' </summary>
    Property IsBootScript() As Boolean

    ''' <summary>
    ''' Gets or set the indicator telling if this is a support script.
    ''' Support scripts are automatically loaded to remote nodes.
    ''' </summary>
    Property IsSupportScript() As Boolean

    ''' <summary>
    ''' Gets or set the indicator telling if this is the primary script.
    ''' </summary>
    Property IsPrimaryScript() As Boolean

    ''' <summary>
    ''' Gets or sets the time it might take to load, run or save the script.
    ''' </summary>
    Property Timeout() As Integer

    ''' <summary>
    ''' Gets the script name
    ''' </summary>
    ReadOnly Property Name() As String

    ''' <summary>
    ''' Returns the list of namespaces for this script.
    ''' </summary>
    Function Namespaces() As String()

    ''' <summary>
    ''' Gets or sets the comma-separated list of namespace for this script.
    ''' </summary>
    Property NamespaceList() As String

    ''' <summary>
    ''' Gets or sets the source code for the script.
    ''' </summary>
    Property Source() As String

#End Region

#Region " SCRIPT MANAGEMENT "

    ''' <summary>
    ''' Returns true the script requires update from file.
    ''' </summary>
    Function RequiresReadParseWrite() As Boolean

    ''' <summary>
    ''' Gets or sets the condition inditing if this script needs to be deleted on
    ''' the instrument. 
    ''' At this time this is used in design mode. It might be used later for refreshing the stored scripts.
    ''' </summary>
    Property RequiresDeletion() As Boolean

    ''' <summary>
    ''' Gets or sets the condition inditing if this script was deleted. 
    ''' </summary>
    Property IsDeleted() As Boolean

#End Region

#Region " MODEL MANAGEMENT "

    ''' <summary>
    ''' Returns true if the <paramref name="model">model</paramref> matches the <see cref="ModelMask">mask</see>.
    ''' </summary>
    Function IsModelMatch(ByVal model As String) As Boolean

    ''' <summary>
    ''' Specifies the family of instrument models for this script
    ''' </summary>
    ReadOnly Property ModelMask() As String

#End Region

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IScriptEntity">script entity</see> 
''' items keyed by the <see cref="IScriptEntity.Name">name.</see>
''' </summary>
Public Class ScriptEntityCollection
    Inherits ScriptEntityBaseCollection(Of IScriptEntity)
    Protected Overloads Overrides Function GetKeyForItem(ByVal item As IScriptEntity) As String
        Return MyBase.GetKeyForItem(item)
    End Function
    Private _allowDeletingNewerScripts As Boolean
    ''' <summary>
    ''' Gets or sets the condition indicating if scripts that are newer than the
    ''' scripts specified by the program can be deleted. This is required to allow the 
    ''' program install the scripts it considers current.
    ''' </summary>
    Public Property AllowDeletingNewerScripts() As Boolean
        Get
            Return Me._allowDeletingNewerScripts
        End Get
        Set(ByVal value As Boolean)
            Me._allowDeletingNewerScripts = value
        End Set
    End Property

End Class

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IScriptEntity">script entity</see> 
''' items keyed by the <see cref="IScriptEntity.Name">name.</see>
''' </summary>
Public Class ScriptEntityBaseCollection(Of TItem As IScriptEntity)
    Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

    Implements IDisposable

#Region " I DISPOSABLE "

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

    Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
        Return item.Name & item.ModelMask
    End Function

    ''' <summary>
    ''' Returns reference to the boot script for the specified node or nothing if a boot script does not exist.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public ReadOnly Property BootScript(ByVal node As INodeEntity) As TItem
        Get
            If node IsNot Nothing Then
                For Each script As TItem In MyBase.Items
                    If script.IsModelMatch(node.ModelNumber) AndAlso script.IsBootScript Then
                        Return script
                    End If
                Next
            End If
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' Returns reference to the Serial Number script for the specified node or nothing if a serial number script does not exist.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public ReadOnly Property SerialNumberScript(ByVal node As INodeEntity) As TItem
        Get
            If node Is Nothing Then
                Return Nothing
            End If
            For Each script As TItem In MyBase.Items
                If script.IsModelMatch(node.ModelNumber) AndAlso script.IsPrimaryScript Then
                    Return script
                End If
            Next
            Return Nothing
        End Get
    End Property

    ''' <summary>
    ''' Returns reference to the support script for the specified node or nothing if a support script does not exist.
    ''' </summary>
    Public ReadOnly Property SupportScript(ByVal node As INodeEntity) As TItem
        Get
            If node Is Nothing Then
                Return Nothing
            End If
            For Each script As TItem In MyBase.Items
                If script.IsModelMatch(node.ModelNumber) AndAlso script.IsSupportScript Then
                    Return script
                End If
            Next
            Return Nothing
        End Get
    End Property

    Private disposedValue As Boolean = False    ' To detect redundant calls
    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then

                ' free other state (managed objects).
                For Each script As TItem In MyBase.Items()
                    script.Dispose()
                Next
                MyBase.Clear()
            End If

            ' free your own state (unmanaged objects).

            ' set large fields to null.

        End If
        Me.disposedValue = True
    End Sub

#Region " FIRMWARE "

    Private _identifiedScript As TItem
    ''' <summary>
    ''' Return any script identified using the test methods.
    ''' </summary>
    Public ReadOnly Property IdentifiedScript() As TItem
        Get
            Return Me._identifiedScript
        End Get
    End Property

    Private _outcomeDetails As System.Text.StringBuilder
    ''' <summary>
    ''' Gets or sets the status message. Used with reading to identify any problem
    ''' </summary>
    Public ReadOnly Property OutcomeDetails() As String
        Get
            Return Me._outcomeDetails.ToString
        End Get
    End Property

    ''' <summary>
    ''' Reads the firmware version of all scripts.
    ''' </summary>
    ''' <param name="node">Specified the node.</param>
    ''' <param name="instrument">Specifies the TSP instrument</param>
    ''' <returns>False if any exception had occurred</returns>
    Public Function ReadFirmwareVersions(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If

        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each script As TItem In MyBase.Items
            If script.IsModelMatch(node.ModelNumber) Then
                ' clear the embedded version.
                script.EmbeddedFirmwareVersion = ""
                If Not script.IsBootScript AndAlso Not String.IsNullOrWhiteSpace(script.Name) Then
                    If instrument.IsNil(node, script.Name) Then
                        Me._identifiedScript = script
                        If Me._outcomeDetails.Length > 0 Then
                            Me._outcomeDetails.AppendLine()
                        End If
                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "custom firmware '{0}' not found on '{1}' node {2}.",
                                                  script.Name, instrument.ResourceName, node.Number)
                    ElseIf instrument.IsNil(node, script.Namespaces) Then
                        Me._identifiedScript = script
                        If Me._outcomeDetails.Length > 0 Then
                            Me._outcomeDetails.AppendLine()
                        End If
                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "custom firmware '{0}' not executed on '{1}' node {2}.",
                                                  script.Name, instrument.ResourceName, node.Number)
                    ElseIf Not script.FirmwareVersionDefined(node, instrument) Then
                        ' existing script must be out of date because it does not support the firmware version command.
                        Me._identifiedScript = script
                        If Me._outcomeDetails.Length > 0 Then
                            Me._outcomeDetails.AppendLine()
                        End If
                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "custom firmware '{0}' version function not defined on '{1}' node {2}.",
                                                  script.Name, instrument.ResourceName, node.Number)
                    Else
                        script.ReadFirmwareVersion(node, instrument)
                    End If
                End If
            End If
        Next
        Return Me._outcomeDetails.Length = 0

    End Function

    ''' <summary>
    ''' Returns true if all script versions are current.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public ReadOnly Property AllVersionsCurrent(ByVal node As INodeEntity) As Boolean
        Get
            If node Is Nothing Then
                Return False
            End If
            Me._outcomeDetails = New System.Text.StringBuilder
            Me._identifiedScript = Nothing
            For Each script As TItem In MyBase.Items
                If script.IsModelMatch(node.ModelNumber) AndAlso Not script.IsBootScript Then
                    Dim outcome As Tsp.FirmwareVersionStatus = script.ValidateFirmware()
                    Select Case outcome
                        Case FirmwareVersionStatus.Current
                        Case FirmwareVersionStatus.Missing
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware '{0}' version function not defined.",
                                                         script.Name)
                        Case FirmwareVersionStatus.Newer
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware '{0}' embedded version '{1}' is newer than the released version '{2}' indicating that this program is out-dated. You must obtain a newer version of this program.",
                                    script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion)
                        Case FirmwareVersionStatus.Older
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware '{0}' embedded version '{1}' is older than the released version '{2}' indicating that this script is out-dated.",
                                    script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion)
                        Case FirmwareVersionStatus.ReferenceUnknown
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware {0} released version not specified.",
                                                         script.Name)
                        Case FirmwareVersionStatus.Unknown
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware {0} failed reading embedded version.",
                                                         script.Name)
                        Case Else
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "unhandled outcome reading custom firmware '{0}' version.", script.Name)
                    End Select
                    If outcome <> FirmwareVersionStatus.Current Then
                        Me._identifiedScript = script
                    End If
                End If
            Next
            Return Me._outcomeDetails.Length = 0
        End Get
    End Property

    ''' <summary>
    ''' Returns true if all script versions are current.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public ReadOnly Property VersionsUnspecified(ByVal node As INodeEntity) As Boolean
        Get
            If node Is Nothing Then
                Return False
            End If
            Me._outcomeDetails = New System.Text.StringBuilder
            Me._identifiedScript = Nothing
            For Each script As TItem In MyBase.Items
                If script.IsModelMatch(node.ModelNumber) AndAlso Not script.IsBootScript Then
                    Dim outcome As FirmwareVersionStatus = script.ValidateFirmware()
                    Select Case outcome
                        Case FirmwareVersionStatus.Missing
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware '{0}' version function not defined.",
                                                         script.Name)
                        Case FirmwareVersionStatus.Unknown
                            Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                         "custom firmware {0} failed reading embedded version.",
                                                         script.Name)
                        Case Else
                    End Select
                    If outcome <> FirmwareVersionStatus.Current Then
                        Me._identifiedScript = script
                    End If
                End If
            Next
            Return Me._outcomeDetails.Length > 0
        End Get
    End Property

    ''' <summary>
    ''' Returns true if any script version is newer than its released version.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    Public ReadOnly Property IsProgramOutdated(ByVal node As INodeEntity) As Boolean
        Get
            If node Is Nothing Then
                Return False
            End If
            Me._outcomeDetails = New System.Text.StringBuilder
            Me._identifiedScript = Nothing
            For Each script As TItem In MyBase.Items
                If script.IsModelMatch(node.ModelNumber) AndAlso Not script.IsBootScript AndAlso script.ValidateFirmware() = FirmwareVersionStatus.Newer Then
                    Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                 "custom firmware '{0}' embedded version '{1}' is newer than the released version '{2}' indicating that this program is out-dated. You must obtain a newer version of this program.",
                                                 script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion)
                    Me._identifiedScript = script
                    Return True
                End If
            Next
            Return False
        End Get
    End Property

#End Region

#Region " UPDATE "

    ''' <summary>
    ''' Returns true if any script requires update from file.
    ''' </summary>
    Public Function RequiresReadParseWrite() As Boolean

        For Each script As TItem In MyBase.Items
            If script.RequiresReadParseWrite Then
                Return True
            End If
        Next
        Return False

    End Function

#End Region

#Region " ACTIONS "

    ''' <summary>
    ''' Runs existing scripts if they did not ran so their versions can be checked.
    ''' Running exits after the first script that failed running assuming that scripts 
    ''' depend on previous scripts.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    ''' <param name="node">Specifies the node.</param>
    ''' <returns>False if any exception had occurred</returns>
    Public Function RunScripts(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If

        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each script As TItem In MyBase.Items
            If script.IsModelMatch(node.ModelNumber) AndAlso Not script.IsBootScript AndAlso Not String.IsNullOrWhiteSpace(script.Name) Then
                If instrument.IsNil(node, script.Name) Then
                    Me._identifiedScript = script
                    If Me._outcomeDetails.Length > 0 Then
                        Me._outcomeDetails.AppendLine()
                    End If
                    Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                 "custom firmware '{0}' not found on '{1}' on node {2}.",
                                                 script.Name, instrument.ResourceName, node.Number)
                    ' if script did not run, do not run subsequent scripts.
                    Return False
                ElseIf instrument.IsNil(node, script.Namespaces) Then

                    ' if script not ran, run it now.
                    If Not instrument.RunScript(script, node) Then
                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "Instrument '{0}' failed running {1} on node {2}.",
                                                     instrument.ResourceName, script.Name, node.Number)
                        ' if script did not run, do not run subsequent scripts.
                        Return False
                    End If

                    If instrument.IsNil(node, script.Name) Then

                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "Instrument '{0}' script {1} not found after running on node {2}.",
                                                     instrument.ResourceName, script.Name, node.Number)

                        ' if script did not run, do not run subsequent scripts.
                        Return False
                    ElseIf instrument.IsNil(node, script.Namespaces) Then

                        Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                     "some of the namespace(s) {0} are nil after running {1} on '{2}' node {3}",
                                                     script.NamespaceList, script.Name, instrument.ResourceName, node.Number)

                        ' if script did not run, do not run subsequent scripts.
                        Return False
                    End If
                End If
            End If
        Next
        Return Me._outcomeDetails.Length = 0

    End Function

    ''' <summary>
    ''' Check if all scripts exist.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    ''' <param name="node">Specifies the node.</param>
    ''' <returns>False if any script does not exist</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification:="Requires instrument interface method")>
    Public Function FindScripts(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If

        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each script As TItem In MyBase.Items
            If script.IsModelMatch(node.ModelNumber) AndAlso Not String.IsNullOrWhiteSpace(script.Name) Then
                If instrument.IsNil(node, script.Name) Then
                    Me._identifiedScript = script
                    If Me._outcomeDetails.Length > 0 Then
                        Me._outcomeDetails.AppendLine()
                    End If
                    Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                 "custom firmware '{0}' not found on '{1}' on node {2}.",
                                                 script.Name, instrument.ResourceName, node.Number)
                    ' if script not found return false
                    Return False
                End If
            End If
        Next
        Return Me._outcomeDetails.Length = 0

    End Function

    ''' <summary>
    ''' Check if all scripts were saved.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="refreshScriptCatalog">Specifies the condition for updating the catalog of saved scripts before
    ''' checking the status of these scripts.</param>
    ''' <returns>False if any script does not exist</returns>
    Public Function FindSavedScripts(ByVal node As INodeEntity, ByVal instrument As ITspInstrument, ByVal refreshScriptCatalog As Boolean) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If

        If refreshScriptCatalog Then
            instrument.FetchSavedScripts()
        End If
        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each script As TItem In MyBase.Items
            If script.IsModelMatch(node.ModelNumber) AndAlso Not String.IsNullOrWhiteSpace(script.Name) Then
                If Not instrument.SavedScriptExists(script.Name, node, False) Then
                    Me._identifiedScript = script
                    If Me._outcomeDetails.Length > 0 Then
                        Me._outcomeDetails.AppendLine()
                    End If
                    Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                 "saved custom firmware '{0}' not found on '{1}' on node {2}.",
                                                 script.Name, instrument.ResourceName, node.Number)
                    ' if script not found return false
                    Return False
                End If
            End If
        Next
        Return Me._outcomeDetails.Length = 0

    End Function

    ''' <summary>
    ''' Check if any script exists on the specified instrument and node.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    ''' <param name="node">Specifies the node.</param>
    ''' <returns>False if any exception had occurred</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification:="Requires instrument interface method")>
    Public Function FindAnyScript(ByVal node As INodeEntity, ByVal instrument As ITspInstrument) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If
        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each script As TItem In MyBase.Items
            If script.IsModelMatch(node.ModelNumber) AndAlso Not String.IsNullOrWhiteSpace(script.Name) Then
                If Not instrument.IsNil(node, script.Name) Then
                    Me._identifiedScript = script
                    If Me._outcomeDetails.Length > 0 Then
                        Me._outcomeDetails.AppendLine()
                    End If
                    Me._outcomeDetails.AppendFormat(Globalization.CultureInfo.CurrentCulture,
                                                 "custom firmware '{0}' found on '{1}' node {2}.",
                                                 script.Name, instrument.ResourceName, node.Number)
                    ' if script found return true
                    Return True
                End If
            End If
        Next
        Return False

    End Function

    ''' <summary>
    ''' Check if any script exists on all nodes from the specified instrument.
    ''' </summary>
    ''' <param name="instrument">Specifies the <see cref="ITspInstrument">TSP instrument.</see></param>
    ''' <returns>False if any exception had occurred</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification:="Requires instrument interface method")>
    Public Function FindAnyScript(ByVal instrument As ITspInstrument) As Boolean

        If instrument Is Nothing Then
            Throw New ArgumentNullException("instrument")
        End If

        Me._outcomeDetails = New System.Text.StringBuilder
        Me._identifiedScript = Nothing
        For Each node As INodeEntity In instrument.NodeEntities
            If FindAnyScript(node, instrument) Then
                Return True
            End If
        Next
        Return False

    End Function

#End Region

End Class

#Region " ENUMERATIONS "

''' <summary>
''' Enumerates the script file formats.
''' </summary>
<Flags()> Public Enum ScriptFileFormats
    <System.ComponentModel.Description("Uncompressed Human Readable")> None = 0
    <System.ComponentModel.Description("Binary format")> Binary = 1
    <System.ComponentModel.Description("Compressed")> Compressed = 2
End Enum

''' <summary>
''' Enumerates the validation status. 
''' </summary>
Public Enum FirmwareVersionStatus

    <System.ComponentModel.Description("No Specified")> None = 0
    ''' <summary>
    ''' Firmware is older than expected (released) indicating the the firmware needs to be updated.
    ''' </summary>
    <System.ComponentModel.Description("Version as older than expected")> Older = 1
    ''' <summary>
    ''' Firmware is current. 
    ''' </summary>
    <System.ComponentModel.Description("Version is current")> Current = 2
    ''' <summary>
    ''' Embedded firmware is newer than expected indicating that the distributed program is out of date.
    ''' </summary>
    <System.ComponentModel.Description("Version as new than expected")> Newer = 3
    ''' <summary>
    ''' Embedded firmware version was not set.
    ''' </summary>
    <System.ComponentModel.Description("Expected version not known (empty)")> Unknown = 4
    ''' <summary>
    ''' Released firmware was not set.
    ''' </summary>
    <System.ComponentModel.Description("Expected version not known (empty)")> ReferenceUnknown = 5
    ''' <summary>
    ''' Version command function does not exist.
    ''' </summary>
    <System.ComponentModel.Description("Version command is missing -- version is nil")> Missing = 6
End Enum

#End Region
