''' <summary>
''' Provides an interface for a TSP VISA instrument.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/16/2009" by="David" revision="3.0.3576.x">
''' Removes option to fetch source with trimmed spaces as this caused problems loading source.
''' </history>
''' <history date="02/21/2009" by="David" revision="3.0.3339.x">
''' Created
''' </history>
Public Interface ITspInstrument

    Inherits IDisposable, isr.Visa.IInstrument, isr.Core.IInitializable

#Region " IRESETTABLE EXTENTION "

    ''' <summary>
    ''' Reset known state of this instance.
    ''' </summary>
    Sub ResetLocalKnownState()

#End Region

#Region " TSP: DISPLAY "

    ''' <summary>
    ''' Gets the display existence indicator. Some TSP instruments (e.g., 3706) may have no display.
    ''' </summary>
    ReadOnly Property IsDisplayExists() As Boolean

    ''' <summary>
    ''' Clears the display
    ''' </summary>
    Function ClearDisplay() As Boolean

    ''' <summary>
    ''' Display message on line one of the display.
    ''' </summary>
    Function DisplayLine(ByVal lineNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean

    ''' <summary>
    ''' Display message on line one of the display.
    ''' </summary>
    Function DisplayLine(ByVal lineNumber As Integer, ByVal value As String) As Boolean

    ''' <summary>
    ''' Restores the instrument display to its main mode.
    ''' </summary>
    Function RestoreDisplay() As Boolean

#End Region

#Region " TSP: ERRORS "

    ''' <summary>Gets the condition for showing errors.
    ''' </summary>
    ReadOnly Property ShowErrors() As Nullable(Of Boolean)

    ''' <summary>Returns the condition for showing errors.
    ''' When true, the unit will automatically display the errors stored in the
    ''' error queue, and then clear the queue. Errors will be processed at the
    ''' end of executing a command message (just prior to issuing a prompt if
    ''' prompts are enabled).
    ''' When false, errors will not display.  Errors will be left in the error
    ''' queue and must be explicitly read or cleared. The error prompt (TSP?)
    ''' is enabled.
    ''' </summary>
    Function ShowErrorsGetter(ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

    ''' <summary>Sets the condition for showing errors.
    ''' When true, the unit will automatically display the errors stored in the
    ''' error queue, and then clear the queue. Errors will be processed at the
    ''' end of executing a command message (just prior to issuing a prompt if
    ''' prompts are enabled).
    ''' When false, errors will not display.  Errors will be left in the error
    ''' queue and must be explicitly read or cleared. The error prompt (TSP?)
    ''' is enabled.
    ''' </summary>
    Function ShowErrorsSetter(ByVal value As Boolean, ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

#End Region

#Region " TSP: GLOBAL "

    ''' <summary>
    ''' Sends a collect garbage command.
    ''' </summary>
    Function CollectGarbage() As Boolean

    ''' <summary>
    ''' Collects garbage on the specified node.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the node number.</param>
    Function CollectGarbage(ByVal nodeNumber As Integer) As Boolean

#End Region

#Region " TSP: NODE "

    ''' <summary>
    ''' Sets the connect rule on the specified node.
    ''' </summary>
    ''' <param name="nodeNumber"></param>
    Function ConnectRuleSetter(ByVal nodeNumber As Integer, ByVal value As Integer) As Boolean

    ''' <summary>
    ''' Deletes the <paramref name="name">specified</paramref> script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number.</param>
    ''' <param name="name">Specifies the script name</param>
    Function DeleteScript(ByVal nodeNumber As Integer, ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

    ''' <summary>
    ''' Deletes the <paramref name="name">specified</paramref> saved script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number.</param>
    ''' <param name="name">Specifies the script name</param>
    Function DeleteSavedScript(ByVal nodeNumber As Integer, ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

    ''' <summary>
    ''' Executes a command on the remote node. This leaves an item in the input buffer that must be retried.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number to validate.</param>
    ''' <param name="format">Specifies the command format.</param>
    ''' <param name="args">Specifies the command arguments.</param>
    Function ExecuteValueGetter(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean

    ''' <summary>
    ''' Fetches the list of saved scripts and saves it in the <see cref="LastFetchedSavedScripts"></see>
    ''' </summary>
    Function FetchSavedScripts(ByVal nodeNumber As Integer) As Boolean

    ''' <summary>
    ''' Executes a command on the remote node and retrieves a string.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number to validate.</param>
    ''' <param name="format">Specifies the command format.</param>
    ''' <param name="args">Specifies the command arguments.</param>
    Function GetString(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary>
    ''' Executes a command on the remote node and retrieves a string.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number to validate.</param>
    ''' <param name="format">Specifies the command format.</param>
    ''' <param name="args">Specifies the command arguments.</param>
    Function GetBoolean(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean?

    ''' <summary>
    ''' Checks the series of values and return true if any one of them is nil.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number to validate.</param>
    ''' <param name="values">Specifies a list of nil objects to check.</param>
    Function IsNil(ByVal nodeNumber As Integer, ByVal ParamArray values() As String) As Boolean

    ''' <summary>
    ''' Gets a comma-separated and comma-terminated list of the saved scripts 
    ''' that was fetched last from the remote node.  A new script is fetched after save and delete.
    ''' </summary>
    ReadOnly Property LastFetchedSavedRemoteScripts() As String

    ''' <summary>
    ''' Returns true if the specified script exists as a saved script on the remote node.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function LastSavedRemoteScriptExists(ByVal name As String) As Boolean

    ''' <summary>
    ''' Makes a script Nil
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function NilifyScript(ByVal nodeNumber As Integer, ByVal name As String) As Boolean

    ''' <summary>
    ''' Saves the specifies script to non-volatile memory.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="name">Specifies the script name</param>
    ''' <param name="isSaveAsBinary">Specifies the condition requesting saving the source as binary</param>
    ''' <param name="isBootScript">Specifies the condition indicating if this is a boot script</param>
    Function SaveScript(ByVal name As String, ByVal node As INodeEntity, ByVal isSaveAsBinary As Boolean, ByVal isBootScript As Boolean) As Boolean

    ''' <summary>
    ''' Returns true if the specified script exists as a saved script.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the remote node number to validate.</param>
    ''' <param name="name">Specifies the script name</param>
    ''' <param name="refreshScriptCatalog">True to refresh the list of saved scripts.</param>
    Function SavedScriptExists(ByVal nodeNumber As Integer, ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

#End Region

#Region " TSP: PROMPTS "

    ''' <summary>Gets the condition for showing prompts.
    ''' </summary>
    ReadOnly Property ShowPrompts() As Nullable(Of Boolean)

    ''' <summary>Return the condition for showing prompts.
    ''' Controls prompting.
    ''' When true, prompts are issued after each command message is
    ''' processed by the instrument.
    ''' When false prompts are not issued.
    ''' Command messages do not generate prompts.  Rather, the TSP instrument
    ''' generates prompts in response to command messages.
    ''' When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be
    ''' returned:
    ''' “TSP>” is the standard prompt. This prompt indicates that everything is
    ''' normal and the command is done processing.
    ''' “TSP?” is issued if there are entries in the error queue when the prompt
    ''' is issued. Like the “TSP>” prompt, it indicates the command is done
    ''' processing. It does not mean the previous command generated an error,
    ''' only that there are still errors in the queue when the command was done
    ''' processing.
    ''' “>>>>” is the continuation prompt. This prompt is used when downloading
    ''' scripts or flash images. When downloading scripts or flash images, many
    ''' command messages must be sent as a unit. The continuation prompt indicates
    ''' that the instrument is expecting more messages as part of the current
    ''' command.
    ''' </summary>
    Function ShowPromptsGetter(ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

    ''' <summary>Sets the condition for showing prompts.
    ''' Controls prompting.
    ''' When true, prompts are issued after each command message is
    ''' processed by the instrument.
    ''' When false prompts are not issued.
    ''' Command messages do not generate prompts.  Rather, the TSP instrument
    ''' generates prompts in response to command messages.
    ''' When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be
    ''' returned:
    ''' “TSP>” is the standard prompt. This prompt indicates that everything is
    ''' normal and the command is done processing.
    ''' “TSP?” is issued if there are entries in the error queue when the prompt
    ''' is issued. Like the “TSP>” prompt, it indicates the command is done
    ''' processing. It does not mean the previous command generated an error,
    ''' only that there are still errors in the queue when the command was done
    ''' processing.
    ''' “>>>>” is the continuation prompt. This prompt is used when downloading
    ''' scripts or flash images. When downloading scripts or flash images, many
    ''' command messages must be sent as a unit. The continuation prompt indicates
    ''' that the instrument is expecting more messages as part of the current
    ''' command.
    ''' </summary>
    Function ShowPromptsSetter(ByVal value As Boolean, ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

#End Region

#Region " TSP SCRIPTS "

    ''' <summary>
    ''' Deletes the <paramref name="name">specified</paramref> script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function DeleteScript(ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

    ''' <summary>
    ''' Deletes the <paramref name="name">specified</paramref> saved script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function DeleteSavedScript(ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

    ''' <summary>
    ''' Fetches the list of saved scripts and saves it in the <see cref="LastFetchedSavedScripts"></see>
    ''' </summary>
    Function FetchSavedScripts() As Boolean

    ''' <summary>
    ''' Fetches a <paramref name="value">specified</paramref> script and saves it 
    ''' as the <see cref="LastFetchScriptSource">last script source</see>
    ''' </summary>
    ''' <param name="name">Specifies the script name.</param>
    ''' <remarks>
    ''' Do not remove spaces as this caused a problem loading binary scripts to the 2600.
    ''' </remarks>
    Function FetchScriptSource(ByVal name As String) As Boolean

    ''' <summary>
    ''' Gets a comma-separated and comma-terminated list of the saved scripts 
    ''' that was fetched last.  A new script is fetched after save and delete.
    ''' </summary>
    ReadOnly Property LastFetchedSavedScripts() As String

    ''' <summary>
    ''' Gets the last source fetched from the instrument
    ''' </summary>
    ReadOnly Property LastFetchScriptSource() As String

    ''' <summary>
    ''' Makes a script nil
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function NilifyScript(ByVal name As String) As Boolean

    ''' <summary>
    ''' Runs the named script.
    ''' </summary>
    ''' <param name="name">Specifies the script name.</param>
    ''' <param name="timeout">Specifies the time to wait for the instrument to 
    ''' return operation completed.</param>
    Function RunScript(ByVal name As String, ByVal timeout As Integer) As Boolean

    ''' <summary>
    ''' Runs the named script.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the subsystem node.</param>
    ''' <param name="name">Specifies the script name.</param>
    ''' <param name="timeout">Specifies the time to wait for the instrument to 
    ''' return operation completed.</param>
    ''' <remarks>
    ''' Waits for operation completion.
    ''' </remarks>
    Function RunScript(ByVal nodeNumber As Integer, ByVal name As String, ByVal timeout As Integer) As Boolean

    ''' <summary>
    ''' Runs the script.
    ''' </summary>
    ''' <param name="node">Specifies the subsystem node.</param>
    ''' <param name="script">Specifies the script.</param>
    ''' <remarks>
    ''' Waits for operation completion.
    ''' </remarks>
    Function RunScript(ByVal script As IScriptEntity, ByVal node As INodeEntity) As Boolean

    ''' <summary>
    ''' Returns true if the specified script exists in the last saved scripts.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    Function LastSavedScriptExists(ByVal name As String) As Boolean

    ''' <summary>
    ''' Returns true if the specified script exists as a saved script.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    ''' <param name="refreshScriptCatalog">True to refresh the list of saved scripts.</param>
    Function SavedScriptExists(ByVal name As String, ByVal refreshScriptCatalog As Boolean) As Boolean

    ''' <summary>
    ''' Returns true if the specified script exists as a saved script.
    ''' </summary>
    ''' <param name="name">Specifies the script name</param>
    ''' <param name="node">Specifies the node to validate.</param>
    ''' <param name="refreshScriptCatalog">True to refresh the list of saved scripts.</param>
    Function SavedScriptExists(ByVal name As String, ByVal node As INodeEntity, ByVal refreshScriptCatalog As Boolean) As Boolean

#End Region

#Region " TSPX: SCRIPT COLLECTION "

    ''' <summary>
    ''' Gets the list of scripts.
    ''' </summary>
    Function Scripts() As ScriptEntityCollection

    ''' <summary>
    ''' Adds a new script to the list of scripts.
    ''' </summary>
    ''' <param name="name">Specifies the name of the script</param>
    ''' <param name="modelMask">Specifies the family of instrument models for this script.</param>
    Function AddScript(ByVal name As String, ByVal modelMask As String) As IScriptEntity

    ''' <summary>
    ''' Create a new instance of the scripts
    ''' </summary>
    Sub NewScripts()

#End Region

#Region " TSPX: NODE COLLECTION "

    ''' <summary>
    ''' Gets the list of scripts.
    ''' </summary>
    Function NodeEntities() As NodeEntityCollection

#End Region

#Region " TSPX: SCRIPT COLLECTION  - LEGACY "

    ''' <summary>
    ''' Gets the list of legacy scripts.
    ''' </summary>
    Function LegacyScripts() As ScriptEntityCollection

    ''' <summary>
    ''' Adds a new script to the list of legacy scripts.
    ''' </summary>
    Function AddLegacyScript(ByVal name As String) As IScriptEntity

    ''' <summary>
    ''' Create a new instance of the legacy scripts
    ''' </summary>
    Sub NewLegacyScripts()

#End Region

#Region " TSP SCRIPTS: LOAD "

    ''' <summary>
    ''' Load the code. Code could be embedded as a comma separated string table format, in 
    ''' which case the script should be concatenated first.
    ''' </summary>
    ''' <param name="value">Includes the script as a long string.</param>
    Function LoadString(ByVal value As String) As Boolean

    ''' <summary>
    ''' Load the code. Code could be embedded as a comma separated string table format, in 
    ''' which case the script should be concatenated first.
    ''' </summary>
    ''' <param name="scriptLines">Includes the script in lines.</param>
    Function LoadString(ByVal scriptLines() As String) As Boolean

    ''' <summary>
    ''' Load the script embedded in the string.
    ''' </summary>
    ''' <param name="scriptLines">Contains the script code line by line</param>
    Function LoadScript(ByVal name As String, ByVal scriptLines() As String) As Boolean

    ''' <summary>
    ''' Writes the script to file.
    ''' </summary>
    ''' <param name="folderPath">Specifies the script file folder</param>
    ''' <param name="script">Specifies the script.</param>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="compress">Specifies the compression condition. True to compress the source before saving.</param>
    Function WriteScriptFile(ByVal folderPath As String, ByVal script As IScriptEntity,
                             ByVal node As INodeEntity, ByVal compress As Boolean) As Boolean

#End Region

#Region " TSP: SYNTAX "

    ''' <summary>
    ''' Checks the series of values and return true if any one of them is Nil.
    ''' </summary>
    ''' <param name="values">Specifies a list of nil objects to check.</param>
    Function IsNil(ByVal ParamArray values() As String) As Boolean

    ''' <summary>
    ''' Checks the series of values and return true if any one of them is nil.
    ''' </summary>
    ''' <param name="node">Specifies the node.</param>
    ''' <param name="values">Specifies a list of nil objects to check.</param>
    Function IsNil(ByVal node As INodeEntity, ByVal ParamArray values() As String) As Boolean

    ''' <summary>
    ''' Returns true if the validation command returns true.
    ''' </summary>
    ''' <param name="format">Specifies the format statement for constructing the assertion.</param>
    ''' <param name="args">Specifies the arguments to use when formatting the assertion.</param>
    Function IsStatementTrue(ByVal format As String, ByVal ParamArray args() As Object) As Boolean

    ''' <summary>
    ''' Sends a print command to the instrument using the specified format and argument.
    ''' The format conforms to the 'C' query command and returns the Boolean outcome.
    ''' </summary>
    ''' <param name="format">Specifies the format statement</param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    ''' <remarks>
    ''' The format string follows the same rules as the printf family of standard C functions. 
    ''' The only differences are that the options or modifiers *, l, L, n, p, and h are not 
    ''' supported and that there is an extra option, q. The q option formats a string in a 
    ''' form suitable to be safely read back by the Lua interpreter: the string is written 
    ''' between double quotes, and all double quotes, newlines, embedded zeros, and backslashes 
    ''' in the string are correctly escaped when written. For instance, the 
    ''' call string.format('%q', 'a string with ''quotes'' and [BS]n new line') will produce the string: 
    ''' a string with [BS]''quotes[BS]'' and [BS]new line
    ''' The options c, d, E, e, f, g, G, i, o, u, X, and x all expect a number as argument, 
    ''' whereas q and s expect a string. This function does not accept string values containing embedded zeros. 
    ''' </remarks>
    Function QueryPrintFormat(ByVal format As String, ByVal ParamArray args() As String) As String

#End Region

#Region " ERROR MANAGEMENT "

    ''' <summary>
    ''' Reports if a device error occurred as set the <see cref="isr.Visa.Ieee4882.Instrument.LastOperationOkay">success condition</see>
    ''' Can only be used after receiving a full reply from the instrument.
    ''' </summary>
    ''' <param name="nodeNumber">Specifies the node number.</param>
    ''' <param name="synopsis">Specifies a synopsis for the warning if an error occurred.</param>
    ''' <param name="format">Specifies the report format.</param>
    ''' <param name="args">Specifies the report arguments.</param>
    ''' <returns>True if success</returns>
    Function ReportDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal synopsis As String,
                                       ByVal format As String, ByVal ParamArray args() As Object) As Boolean

#End Region

End Interface

