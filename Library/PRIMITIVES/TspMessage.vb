''' <summary>
''' Manages TSP messages.
''' </summary>
''' <remarks>
''' TSP Messages are designed to augment the service request messaging.
''' This may be required whenever the SRQ messaging is insufficient due to
''' the fact that the SRQ messages are unable to report on individual
''' units or nodes.   <p>
'''                   </p><p>
''' With TSP only a single register is available for displaying
''' the status of one or more nodes with one or more units (SMU or switches).
''' This the master is unable to report status on each node or unit.
''' For example, the master can raise only a single OPC flag. Thus there is
''' now way to tell which unit finished.  Only that an operation finished.
''' In that case, the output message must be much simpler more like the
''' SRQ bit but for the entire operation rather than the specific command.
''' We need to know the node, unit, and status values.  That could be
''' followed by contents or data.
'''                   </p><p>
''' Message Structure:
'''                   </p><p>
''' Each TSP message consists of a preamble and potential follow up messages.
'''                   </p><p>
''' Preamble:
'''                   </p><p>
''' The preamble is designed to by succinct providing minimal sufficient
''' information to determine status and what to do next. To accomplish this,
''' without reinventing much, the messaging system reports the following
''' register information for the node or the unit:
'''                   </p><p>
''' Service Request Register byte: SRQ, status byte, STB, or status.condition
'''                   </p><p>
''' Status Register Byte:  ESR, or event status register, or status.standard.event
'''                   </p><p>
''' Finally the information included a message available, data available,
''' and buffer available bits.  These help determine whether additional
''' information is already in the output queue or is in the messaging queue.
'''                   </p><p>
''' Format:  All status information is in Hex as follows:
'''                   </p><p>
''' NODE , UNIT , STB , ESR , INFO [, CONTENTS
'''   40 ,    b ,  BF ,  FF , FFFF [, FF, message]
'''                   </p><p>
''' Contents:
'''                   </p><p>
''' Message contents consists of a message type number following
''' by a message contents.
'''                   </p><p>
''' TYPE, MESSAGE
'''   FF, some text
'''                   </p>
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x">
''' Created
''' </history>
Public Class TspMessage
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        Me.New(String.Empty)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    Public Sub New(ByVal instanceName As String)

        MyBase.New()

        Me._instanceName = instanceName

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " PROPERRIES "

    ''' <summary>Gets or sets the message contents.
    ''' </summary>
    Private _contents As String

    ''' <summary>Gets or sets the message contents.
    ''' </summary>
    Public Property Contents() As String
        Get
            Return Me._contents
        End Get
        Set(ByVal Value As String)
            Me._contents = Value
        End Set
    End Property

    ''' <summary>Gets or sets the failure message parsing the message.
    ''' </summary>
    Private _failureMessage As String

    ''' <summary>Gets or sets the failure message parsing the message.
    ''' </summary>
    Public ReadOnly Property FailureMessage() As String
        Get
            Return Me._failureMessage
        End Get
    End Property

    ''' <summary>Gets or sets the device information
    ''' <see cref="isr.Tsp.TspMessageInfoBits">info status bits</see>.
    ''' </summary>
    Private _infoStatus As isr.Tsp.TspMessageInfoBits

    ''' <summary>Gets or sets the device information
    ''' <see cref="isr.Tsp.TspMessageInfoBits">status bits</see>.
    ''' </summary>
    Public Property InfoStatus() As isr.Tsp.TspMessageInfoBits
        Get
            Return Me._infoStatus
        End Get
        Set(ByVal Value As isr.Tsp.TspMessageInfoBits)
            Me._infoStatus = Value
        End Set
    End Property

    ''' <summary>Gets or sets the last message.
    ''' </summary>
    Private _lastMessage As String

    ''' <summary>Gets or sets the last message.
    ''' </summary>
    Public Property LastMessage() As String
        Get
            Return Me._lastMessage
        End Get
        Set(ByVal Value As String)
            Me._lastMessage = Value
        End Set
    End Property

    ''' <summary>Gets or sets the
    ''' <see cref="isr.Tsp.TspMessageTypes">message type</see>.
    ''' </summary>
    Private _messageType As isr.Tsp.TspMessageTypes

    ''' <summary>Gets or sets the
    ''' <see cref="isr.Tsp.TspMessageTypes">message type</see>.
    ''' </summary>
    Public Property MessageType() As isr.Tsp.TspMessageTypes
        Get
            Return Me._messageType
        End Get
        Set(ByVal Value As isr.Tsp.TspMessageTypes)
            Me._messageType = Value
        End Set
    End Property

    ''' <summary>Gets or sets the one-based node number.
    ''' </summary>
    Private _nodeNumber As Integer

    ''' <summary>Gets or sets the one-based node number.
    ''' </summary>
    Public Property NodeNumber() As Integer
        Get
            Return Me._nodeNumber
        End Get
        Set(ByVal Value As Integer)
            Me._nodeNumber = Value
        End Set
    End Property

    ''' <summary>Gets or sets the service request
    ''' <see cref="isr.Visa.Ieee4882.ServiceRequests">status bits</see>.
    ''' </summary>
    Private _serviceRequests As isr.Visa.Ieee4882.ServiceRequests

    ''' <summary>Gets or sets the service request
    ''' <see cref="isr.Visa.Ieee4882.ServiceRequests">status bits</see>.
    ''' </summary>
    Public Property ServiceRequests() As isr.Visa.Ieee4882.ServiceRequests
        Get
            Return Me._serviceRequests
        End Get
        Set(ByVal Value As isr.Visa.Ieee4882.ServiceRequests)
            Me._serviceRequests = Value
        End Set
    End Property

    ''' <summary>Gets or sets the standard event
    ''' <see cref="isr.Visa.Ieee4882.StandardEvents">status bits</see>.
    ''' </summary>
    Private _standardEvents As isr.Visa.Ieee4882.StandardEvents

    ''' <summary>Gets or sets the standard event
    ''' <see cref="isr.Visa.Ieee4882.StandardEvents">status bits</see>.
    ''' </summary>
    Public Property StandardEvents() As isr.Visa.Ieee4882.StandardEvents
        Get
            Return Me._standardEvents
        End Get
        Set(ByVal Value As isr.Visa.Ieee4882.StandardEvents)
            Me._standardEvents = Value
        End Set
    End Property

    ''' <summary>Gets or sets the unit number, e.g., a or b.
    ''' </summary>
    Private _unitNumber As String

    ''' <summary>Gets or sets the unit number (a or b).
    ''' </summary>
    Public Property UnitNumber() As String
        Get
            Return Me._unitNumber
        End Get
        Set(ByVal Value As String)
            Me._unitNumber = Value
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Creates a copy of itself to pass to another class..
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Function Clone() As isr.Tsp.TspMessage

        Dim myClone As isr.Tsp.TspMessage = New isr.Tsp.TspMessage

        myClone.NodeNumber = Me._nodeNumber
        myClone.UnitNumber = Me._unitNumber
        myClone.StandardEvents = Me._standardEvents
        myClone.InfoStatus = Me._infoStatus
        myClone.ServiceRequests = Me._serviceRequests
        myClone.MessageType = Me._messageType
        myClone.Contents = Me._contents
        myClone.LastMessage = Me._lastMessage

        ' return the copy
        Return myClone

    End Function

    ''' <summary>
    ''' Parses the message received from the instrument.
    ''' </summary>
    ''' <remarks>
    ''' The TSP Message consists of the following elements:       <p>
    ''' NODE , UNIT , STB , ESR , INFO [, CONTENTS                </p><p>
    '''   40 ,    b ,  BF ,  FF , FFFF [, FF, message]            </p><p>
    ''' node - node Number                                        </p><p>
    ''' unit - unit number                                        </p><p>
    ''' stb  - status register bits                               </p><p>
    ''' esr  - standard register bits                             </p><p>
    ''' info - message information bits                           </p><p>
    ''' contents - message data                                   </p><p>
    '''                                                           </p><p>
    '''                                                           </p>
    ''' </remarks>
    ''' <param name="value">Specifies message received from the instrument.</param>
    Public Function Parse(ByVal value As String) As Boolean

        Me._lastMessage = value
        Me._unitNumber = String.Empty ' CStr(Nothing)
        Me._nodeNumber = 0
        Me._messageType = isr.Tsp.TspMessageTypes.None
        Me._infoStatus = isr.Tsp.TspMessageInfoBits.None
        Me._standardEvents = isr.Visa.Ieee4882.StandardEvents.None
        Me._serviceRequests = isr.Visa.Ieee4882.ServiceRequests.None

        Me._contents = String.Empty ' CStr(Nothing)

        Me._failureMessage = String.Empty ' CStr(Nothing)
        If Not String.IsNullOrWhiteSpace(value) Then

            Dim values As String() = value.Split(","c)

            If values Is Nothing OrElse values.Length <= 0 Then
                Exit Function
            End If

            ' trim all values.
            For index As Integer = 0 To values.Length - 1
                values(index) = values(index).Trim

                Select Case index
                    Case 0
                        Me._nodeNumber = CInt("&H" & values(index))
                    Case 1
                        Me._unitNumber = values(index)
                    Case 2
                        Me._serviceRequests = CType(CInt("&H" & values(index)), isr.Visa.Ieee4882.ServiceRequests)
                    Case 3
                        Me._standardEvents = CType(CInt("&H" & values(index)), isr.Visa.Ieee4882.StandardEvents)
                    Case 4
                        Me._infoStatus = CType(CInt("&H" & values(index)), isr.Tsp.TspMessageInfoBits)
                    Case 5
                        Me._messageType = CType(CInt("&H" & values(index)), isr.Tsp.TspMessageTypes)
                    Case 6
                        Me._contents = values(index)
                End Select
            Next index

        End If

        Return True

    End Function

#End Region

End Class