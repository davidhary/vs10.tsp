''' <summary>
''' Loads and runs TSP scripts.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x">
''' Created
''' </history>
Public Class TspScript
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private _tspStatus As TspStatus
    ''' <summary>
    ''' Initializes a new instance of the <see cref="TspScript" /> class.
    ''' </summary>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    ''' <param name="status">The status.</param>
    Public Sub New(ByVal io As isr.Tsp.TspVisaIO, ByVal status As TspStatus)
        Me.New(String.Empty, io)
        Me._tspStatus = status
    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    Public Sub New(ByVal instanceName As String, ByVal io As isr.Tsp.TspVisaIO)

        MyBase.New()

        Me._instanceName = instanceName

        Me._visaIO = io

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                    ' terminate reference to the resources.
                    If Me._visaIO IsNot Nothing Then
                        Me._visaIO = Nothing
                    End If

                    If Me._tspStatus IsNot Nothing Then
                        Me._tspStatus.Dispose()
                        Me._tspStatus = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.
    ''' </summary>
    Private _visaIO As isr.Tsp.TspVisaIO

    ''' <summary>Gets or sets the error message returned by the
    ''' Call Function method.</summary>
    Private _callErrorMessage As String
    ''' <summary>Gets or sets the error message returned by the
    ''' Call Function method.</summary>
    Public ReadOnly Property CallErrorMessage() As String
        Get
            Return Me._callErrorMessage
        End Get
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Calls the function with the given arguments in protected mode. This means
    ''' that any error inside the function is not propagated; instead, the call
    ''' (Lua pcall) catches the error and returns a status code. Its first result
    ''' is the status code (a boolean), which is true if the call succeeds without
    ''' errors. In such case, pcall also returns all results from the call, after
    ''' this first result. In case of any error, pcall returns false plus the error
    ''' message.
    ''' </summary>
    ''' <param name="functionName">Specifies the function name.</param>
    ''' <param name="args">Specifies the function arguments.</param>
    Public Function CallFunction(ByVal functionName As String, ByVal args As String) As Boolean

        Me._callErrorMessage = String.Empty ' CStr(Nothing)

        ' Check name
        Dim callStatement As String
        If String.IsNullOrWhiteSpace(functionName) Then
            Me._callErrorMessage = "Function name not provided"
            Return False
        Else
            callStatement = TspSyntax.CallFunctionCommand(functionName, args)
            callStatement = TspSyntax.PrintCommand(callStatement)
            Return Me._visaIO.WriteQueryLine(callStatement)
        End If

    End Function

    ''' <summary>
    ''' Gets all users scripts from the instrument.
    ''' </summary>
    Public Function FetchUserScriptNames() As Boolean

        ' .4050  replace My.MyLibrary.TspStatus with Me._TspStatus.
        ' store state of prompts and errors.
        Me._tspStatus.StoreStatus()

        Try

            Dim deviceErrors As String = String.Empty

            ' load the function which to execute and get its name.
            Dim functionName As String
            functionName = Me.LoadPrintUserScriptNames()

            If String.IsNullOrWhiteSpace(functionName) Then

                ' now report the error to the calling module
                If Me._visaIO.HandleInstrumentErrorIfError(False) Then
                    deviceErrors = Me._visaIO.DeviceErrors
                End If
                Throw New UserScriptFetchException(Me._visaIO.ResourceName, deviceErrors)

            End If

            ' Disable automatic display of errors - leave error messages in queue and enable error Prompt.
            Me._tspStatus.ShowErrorsSetter(False, Visa.ResourceAccessLevels.Device)

            ' Turn off prompts
            Me._tspStatus.ShowPromptsSetter(False, Visa.ResourceAccessLevels.Device)

            ' run the function
            Dim delimiter As Char = ","c
            Dim callState As isr.Tsp.TspExecutionState
            If Me.CallFunction(functionName, "'" & delimiter & "'") Then

                ' wait till we get a reply from the instrument or timeout.
                Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(2000))
                Do
                    Threading.Thread.Sleep(50)
                Loop Until endTime < DateTime.Now Or Me._visaIO.IsMessageAvailable
                If endTime < DateTime.Now Then
                    Throw New UserScriptFetchException("Timeout waiting for user script name response from the instrument.")
                End If

                ' read the names
                Dim names As String = Me._visaIO.ReadLine()
                If String.IsNullOrWhiteSpace(names) Then

                    ' check if return value is an error a prompt.  This will
                    ' happened if the call is bad.
                ElseIf names = TspSyntax.NilValue Then

                    ' read the next value, which should be true
                    Me._visaIO.ReadBoolean()

                    ' now read the status.
                    Me._tspStatus.ReadAndParseState()

                Else

                    callState = Me._tspStatus.ParseState(names)
                    If callState = isr.Tsp.TspExecutionState.IdleError Then

                        ' now report the error to the calling module
                        If Me._visaIO.HandleInstrumentErrorIfError(False) Then
                            deviceErrors = Me._visaIO.DeviceErrors
                        End If
                        Throw New FunctionCallException(Me._visaIO.ResourceName, functionName, deviceErrors)

                        ' check if return value is false.  This will happen if the function failed.
                    ElseIf names.Substring(0, 4) = TspSyntax.FalseValue Then

                        ' if failure, get the failure message
                        Me._callErrorMessage = names.Split(delimiter)(1)
                        If Me._visaIO.HandleInstrumentErrorIfError(False) Then
                            deviceErrors = Me._visaIO.DeviceErrors
                        End If
                        Throw New FunctionCallException(Me._visaIO.ResourceName, functionName,
                                                         Me._callErrorMessage, deviceErrors)

                    Else

                        ' split the return values
                        Me._userScriptNames = names.Split(delimiter)

                        ' read the next value, which should be true
                        Me._visaIO.ReadBoolean()

                        ' now read the status.
                        Me._tspStatus.ReadAndParseState()

                    End If
                End If
            End If

            ' return true if we got one or more.
            Return Me._userScriptNames.Length > 0

        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        Finally

            ' restore state of prompts and errors.
            Me._tspStatus.RestoreStatus()

            ' add a wait to ensure the system returns the last status.
            Threading.Thread.Sleep(100)

            ' flush the buffer until empty to completely reading the status.
            Me._visaIO.DiscardUnreadData(10, 100, True)

        End Try

    End Function

    ''' <summary>Returns true if the specified global exists.</summary>
    ''' <param name="value">Specifies the global which to look for.</param>
    Public Function IsGlobalExists(ByVal value As String) As Boolean

        Return Me._visaIO.PrintAndQueryValue(value) <> TspSyntax.NilValue

    End Function

    ''' <summary>
    ''' Loads the 'printUserScriptNames' function and return the function name.
    ''' The function is loaded only if it does not exists already.
    ''' </summary>
    Public Function LoadPrintUserScriptNames() As String

        Me._tspStatus = New TspStatus(Me._visaIO)
        ' .4050  replace My. MyLibrary .TspStatus with Me._TspStatus.
        ' store state of prompts and errors.
        Me._tspStatus.StoreStatus()

        Try

            Dim functionName As String
            functionName = "printUserScriptNames"

            Dim functionCode As String
            If Me.IsGlobalExists(functionName) Then

                Return functionName

            Else

                functionCode = " function printUserScriptNames( delimiter ) local scripts = nil for i,v in _G.pairs(_G.script.user.scripts) do if scripts == nil then scripts = i else scripts = scripts .. delimiter .. i end end _G.print (scripts) _G.waitcomplete() end "

                ' load the function
                If Me._visaIO.WriteLine(functionCode) Then

                    Return functionName

                Else

                    Return String.Empty

                End If

            End If

        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        Finally

            ' restore state of prompts and errors.
            Me._tspStatus.RestoreStatus()

        End Try

    End Function

    ''' <summary>
    ''' Returns a string from the parameter array of arguments for use
    ''' when running the function.
    ''' </summary>
    ''' <param name="args">Specifies a parameter array of arguments.</param>
    ''' <history>
    ''' </history>
    Public Shared Function Parameterize(ByVal ParamArray args() As String) As String

        Dim arguments As New System.Text.StringBuilder
        Dim i As Integer
        If args IsNot Nothing AndAlso args.Length >= 0 Then
            For i = 0 To args.Length - 1
                If (i > 0) Then
                    arguments.Append(",")
                End If
                arguments.Append(args(i))
            Next i

        End If
        Return arguments.ToString

    End Function

    ''' <summary>Stores user script names.</summary>
    Private _userScriptNames() As String

    ''' <summary>Returns the list of user script names.</summary>
    Public Function UserScriptNames() As String()
        Return Me._userScriptNames
    End Function

#End Region

End Class