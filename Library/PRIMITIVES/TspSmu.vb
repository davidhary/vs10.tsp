''' <summary>
''' Manages TSP SMU.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x">
''' Created
''' </history>
Public Class TspSmu
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    ''' <remarks>Note that the local node status clear command
    ''' only clears the SMU status.  So, issue a Cls and Rst as necessary
    ''' when adding an SMU.
    ''' </remarks>
    Public Sub New(ByVal io As isr.Tsp.TspVisaIO)
        Me.New(String.Empty, io, 0, "a")
    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    ''' <param name="node">Specifies the node number.</param>
    ''' <param name="smu">Specifies the SMU (either 'a' or 'b'.
    ''' </param>
    ''' <remarks>Note that the local node status clear command
    ''' only clears the SMU status.  So, issue a Cls and Rst as necessary
    ''' when adding an SMU.
    ''' </remarks>
    Public Sub New(ByVal io As isr.Tsp.TspVisaIO, ByVal node As Integer, ByVal smu As String)
        Me.New(String.Empty, io, node, smu)
    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    ''' <param name="node">Specifies the node number.</param>
    ''' <param name="smu">Specifies the SMU (either 'a' or 'b'.
    ''' </param>
    ''' <remarks>Note that the local node status clear command
    ''' only clears the SMU status.  So, issue a Cls and Rst as necessary
    ''' when adding an SMU.
    ''' </remarks>
    Public Sub New(ByVal instanceName As String, ByVal io As isr.Tsp.TspVisaIO, ByVal node As Integer, ByVal smu As String)

        MyBase.New()

        Me._instanceName = instanceName

        Me._visaIO = io
        Me.NodeNumber = node
        Me.UnitNumber = smu

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                    ' terminate reference to the resources.
                    If Me._visaIO IsNot Nothing Then
                        Me._visaIO = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " PROPERRIES "

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.
    ''' </summary>
    Private _visaIO As isr.Tsp.TspVisaIO

    ''' <summary>Gets the condition telling if this is the master node.
    ''' </summary>
    Public ReadOnly Property IsMasterNode() As Boolean
        Get
            Return Me._nodeNumber = 1
        End Get
    End Property

    Private _nodeNumber As Integer
    ''' <summary>Gets or sets the one-based node number.
    ''' </summary>
    Public Property NodeNumber() As Integer
        Get
            Return Me._nodeNumber
        End Get
        Set(ByVal Value As Integer)
            Me._nodeNumber = Value
            Me._smuReference = TspSyntax.SmuReference(Me._nodeNumber, Me._unitNumber)
        End Set
    End Property

    Private _smuReference As String
    ''' <summary>Gets or sets the smu reference string, e.g., '_G.node[ 1 ].smua'.
    ''' </summary>
    Public ReadOnly Property SmuReference() As String
        Get
            Return Me._smuReference
        End Get
    End Property

    ''' <summary>Gets or sets the smu number.
    ''' </summary>
    Private _unitNumber As String

    ''' <summary>Gets or sets the SMU unit number (a or b).
    ''' </summary>
    Public Property UnitNumber() As String
        Get
            Return Me._unitNumber
        End Get
        Set(ByVal Value As String)
            Me._unitNumber = Value
            Me._smuReference = TspSyntax.SmuReference(Me._nodeNumber, Me._unitNumber)
        End Set
    End Property

#End Region

#Region " CONTACT CHECK "

    Dim _contactResistances As String
    ''' <summary>
    ''' Gets the contact resistances.
    ''' </summary>
    ''' 
    Public ReadOnly Property ContactResistances() As String
        Get
            Return Me._contactResistances
        End Get
    End Property

    ''' <summary>
    ''' Determines whether contact resistances are below the specified threshold.
    ''' </summary>
    ''' <param name="threshold">The threshold.</param>
    ''' <returns>
    ''' <c>True</c> if passed, <c>False</c> if failed or null if failed configuring contact check.
    ''' </returns>
    Public Function IsContactChecks(ByVal threshold As Integer) As Boolean?
        Me._contactResistances = "-1,-1"
        Dim okay As Boolean = True
        okay = Me._visaIO.WriteLine("{0}.contact.speed = smua.CONTACT_FAST", Me.SmuReference)
        okay = okay AndAlso Me._visaIO.WriteLine("{0}.contact.threshold = {1}", Me.SmuReference, threshold)
        If okay Then
            okay = Me._visaIO.IsStatementTrue("{0}smua.contact.check()", Me.SmuReference)
            If okay Then
                Me._visaIO.OnMessageAvailable(TraceEventType.Verbose, "CONTACT CHECK PASSED",
                                           "Contact check test passed")
                Return True
            Else
                Me._contactResistances = Me._visaIO.QueryLine("print({0}.contact.r())", Me.SmuReference)
                If String.IsNullOrWhiteSpace(Me._contactResistances) Then
                    Me._visaIO.OnMessageAvailable(TraceEventType.Error, "CONTACT CHECK FAILED. FAILED FETCHING CONTACT RESISTANCES.",
                                               "Contact check failed. Failed fetching contact resistances. The following command failed: '{0}'", Me._visaIO.Session.Writer.TransmitBuffer)
                End If
                Return False
            End If
        Else
            Me._visaIO.OnMessageAvailable(TraceEventType.Error, "FAILED SETTING UP CONTACT CHECK - COMMAND ERROR",
                                       "Failed setting up contact check. The following command failed: '{0}'", Me._visaIO.Session.Writer.TransmitBuffer)
            Return New Boolean?
        End If
    End Function
#End Region

End Class