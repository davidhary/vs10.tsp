''' <summary>
''' Manages the TSP System States and statuses. This includes keeping
''' tab on the system state as indicated by the TSP prompts, error handling
''' mode, or prompting.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x">
''' Created
''' </history>
Public Class TspStatus
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="TspVisaIO">VISA Instrument</see>.</param>
    Public Sub New(ByVal io As TspVisaIO)

        Me.New(String.Empty, io)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="TspVisaIO">VISA Instrument</see>.</param>
    Public Sub New(ByVal instanceName As String, ByVal io As TspVisaIO)

        MyBase.New()

        Me._instanceName = instanceName

        ' instantiate the saved error in case we need it
        showErrorsStack = New System.Collections.Generic.Stack(Of Boolean)
        showPromptsStack = New System.Collections.Generic.Stack(Of Boolean)
        Me._state = isr.Tsp.TspExecutionState.Closed

        ' tag as not showing prompt so as to get the status only after
        ' Rst where the actual state is determined.
        ' Me._isShowPrompts = False
        Me._state = isr.Tsp.TspExecutionState.Closed

        Me._io = io
        If io IsNot Nothing AndAlso Me._io.IsConnected Then
            Me.ResetKnownState()
        End If

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._io IsNot Nothing Then
                        Me._io = Nothing
                    End If

                    If showErrorsStack IsNot Nothing Then
                        showErrorsStack = Nothing
                    End If

                    If showPromptsStack IsNot Nothing Then
                        showPromptsStack = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIO">Gpib Base Instrument</see>.
    ''' </summary>
    Private _io As isr.Tsp.TspVisaIO

#End Region

#Region " ERRORS "

    ''' <summary>Gets or sets the stack for storing the show errors states.
    ''' </summary>
    Private showErrorsStack As System.Collections.Generic.Stack(Of Boolean)

    Private _showErrors As Nullable(Of Boolean)
    ''' <summary>Gets the condition for showing errors.
    ''' </summary>
    Public ReadOnly Property ShowErrors() As Nullable(Of Boolean)
        Get
            Return Me._showErrors
        End Get
    End Property

    ''' <summary>Returns the condition for showing errors.
    ''' When true, the unit will automatically display the errors stored in the
    ''' error queue, and then clear the queue. Errors will be processed at the
    ''' end of executing a command message (just prior to issuing a prompt if
    ''' prompts are enabled).
    ''' When false, errors will not display.  Errors will be left in the error
    ''' queue and must be explicitly read or cleared. The error prompt (TSP?)
    ''' is enabled.
    ''' </summary>
    Public Function ShowErrorsGetter(ByVal access As isr.Visa.ResourceAccessLevels) As Boolean
        If (access = Visa.ResourceAccessLevels.Device) OrElse Not Me._showErrors.HasValue Then
            If Me._io.WriteQueryLine(TspSyntax.ShowErrorsQueryCommand) Then
                Me._showErrors = 1 = Me._io.ReadInt32().Value
                Me.ReadAndParseState()
            Else
                Throw New isr.Visa.VisaException(Me._io.LastVisaStatus, "Failed getting the condition for showing errors.")
            End If
        End If
        Return Me._showErrors.Value
    End Function

    ''' <summary>Sets the condition for showing errors.
    ''' When true, the unit will automatically display the errors stored in the
    ''' error queue, and then clear the queue. Errors will be processed at the
    ''' end of executing a command message (just prior to issuing a prompt if
    ''' prompts are enabled).
    ''' When false, errors will not display.  Errors will be left in the error
    ''' queue and must be explicitly read or cleared. The error prompt (TSP?)
    ''' is enabled.
    ''' </summary>
    Public Function ShowErrorsSetter(ByVal value As Boolean, ByVal access As isr.Visa.ResourceAccessLevels) As Boolean
        If (access = Visa.ResourceAccessLevels.Device) OrElse (Not Me._showErrors.HasValue) OrElse (value <> ShowErrors.Value) Then
            If Me._io.WriteLine(TspSyntax.ShowErrorsCommand(value)) Then
                Me._showErrors = value
            Else
                Throw New isr.Visa.VisaException(Me._io.LastVisaStatus, "Failed setting the condition for showing errors.")
            End If
        End If
        Me.ReadAndParseState()
        Return Me._showErrors.Value
    End Function

#End Region

#Region " PROMPTS AND EXECUTION STATE "

    ''' <summary>
    ''' Parses the state of the TSP prompt and saves it
    ''' in the state cache value.
    ''' </summary>
    ''' <param name="value">Specifies the read buffer.</param>
    Public Function ParseState(ByVal value As String) As TspExecutionState

        If Not String.IsNullOrWhiteSpace(value) AndAlso value.Length >= 4 Then
            value = value.Substring(0, 4)
            If value.StartsWith(isr.Tsp.TspSyntax.ReadyPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                Me._state = isr.Tsp.TspExecutionState.IdleReady
            ElseIf value.StartsWith(isr.Tsp.TspSyntax.ContinuationPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                Me._state = isr.Tsp.TspExecutionState.IdleContinuation
            ElseIf value.StartsWith(isr.Tsp.TspSyntax.ErrorPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                Me._state = isr.Tsp.TspExecutionState.IdleError
            Else
                ' no change from the last state?
            End If
        End If
        Return Me._state

    End Function

    ''' <summary>
    ''' Reads the state of the TSP prompt and saves it
    ''' in the state cache value.
    ''' </summary>
    Public Function ReadAndParseState() As TspExecutionState

        ' check status of the prompt flag.
        If Me._showPrompts.HasValue Then

            ' if prompts are on, 
            If Me._showPrompts.Value Then

                ' do a read. This raises an event that parses the state
                If Me._io.IsMessageAvailable Then
                    Me._io.ReadLine()
                End If

            Else

                Me._state = isr.Tsp.TspExecutionState.Unknown

            End If

        Else

            ' check if we have data in the output buffer.  
            If Me._io.IsMessageAvailable Then

                ' if data exists in the buffer, it may indicate that the prompts are already on 
                ' so just go read the output buffer. Once read, the status will be parsed.
                Me._io.ReadLine()

            Else

                ' if we have no value then we must first read the prompt status
                ' once read, the status will be parsed.
                ShowPromptsGetter(Visa.ResourceAccessLevels.None)

            End If

        End If

        Return Me._state

    End Function

    ''' <summary>Gets or sets the stack for storing the show prompts states.
    ''' </summary>
    Private showPromptsStack As System.Collections.Generic.Stack(Of Boolean)

    Private _showPrompts As Nullable(Of Boolean)
    ''' <summary>Gets the condition for showing prompts.
    ''' </summary>
    Public ReadOnly Property ShowPrompts() As Nullable(Of Boolean)
        Get
            Return Me._showPrompts
        End Get
    End Property

    ''' <summary>Return the condition for showing prompts.
    ''' Controls prompting.
    ''' When true, prompts are issued after each command message is
    ''' processed by the instrument.
    ''' When false prompts are not issued.
    ''' Command messages do not generate prompts.  Rather, the TSP instrument
    ''' generates prompts in response to command messages.
    ''' When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be
    ''' returned:
    ''' �TSP>� is the standard prompt. This prompt indicates that everything is
    ''' normal and the command is done processing.
    ''' �TSP?� is issued if there are entries in the error queue when the prompt
    ''' is issued. Like the �TSP>� prompt, it indicates the command is done
    ''' processing. It does not mean the previous command generated an error,
    ''' only that there are still errors in the queue when the command was done
    ''' processing.
    ''' �>>>>� is the continuation prompt. This prompt is used when downloading
    ''' scripts or flash images. When downloading scripts or flash images, many
    ''' command messages must be sent as a unit. The continuation prompt indicates
    ''' that the instrument is expecting more messages as part of the current
    ''' command.
    ''' </summary>
    Public Function ShowPromptsGetter(ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

        If (access = Visa.ResourceAccessLevels.Device) OrElse Not Me._showPrompts.HasValue Then
            If Me._io.WriteQueryLine(TspSyntax.ShowPromptsQueryCommand) Then
                Me._showPrompts = 1 = Me._io.ReadInt32().Value
                Me.ReadAndParseState()
            Else
                Throw New isr.Visa.VisaException(Me._io.LastVisaStatus, "Failed getting the condition for showing prompts.")
            End If
        End If
        Return Me._showPrompts.Value

    End Function

    ''' <summary>Sets the condition for showing prompts.
    ''' Controls prompting.
    ''' When true, prompts are issued after each command message is
    ''' processed by the instrument.
    ''' When false prompts are not issued.
    ''' Command messages do not generate prompts.  Rather, the TSP instrument
    ''' generates prompts in response to command messages.
    ''' When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be
    ''' returned:
    ''' �TSP>� is the standard prompt. This prompt indicates that everything is
    ''' normal and the command is done processing.
    ''' �TSP?� is issued if there are entries in the error queue when the prompt
    ''' is issued. Like the �TSP>� prompt, it indicates the command is done
    ''' processing. It does not mean the previous command generated an error,
    ''' only that there are still errors in the queue when the command was done
    ''' processing.
    ''' �>>>>� is the continuation prompt. This prompt is used when downloading
    ''' scripts or flash images. When downloading scripts or flash images, many
    ''' command messages must be sent as a unit. The continuation prompt indicates
    ''' that the instrument is expecting more messages as part of the current
    ''' command.
    ''' </summary>
    Public Function ShowPromptsSetter(ByVal value As Boolean, ByVal access As isr.Visa.ResourceAccessLevels) As Boolean

        If (access = Visa.ResourceAccessLevels.Device) OrElse (Not Me._showPrompts.HasValue) OrElse (value <> ShowPrompts.Value) Then
            Dim previousValue As Nullable(Of Boolean) = Me._showErrors
            Me._showPrompts = value
            ' now send the command.  This will also update the status.
            If Not Me._io.WriteLine(TspSyntax.ShowPromptsCommand(value)) Then
                Me._showErrors = previousValue
                Throw New isr.Visa.VisaException(Me._io.LastVisaStatus, "Failed setting the condition for showing prompts.")
            End If
        End If
        Me.ReadAndParseState()
        Return Me._showPrompts.Value

    End Function

    ''' <summary>Gets or sets the current TSP execution state.
    ''' </summary>
    Private _state As TspExecutionState

    ''' <summary>Gets or sets the last TSP state.  Setting the last state
    ''' is useful when closing the Tsp System.
    ''' </summary>
    Public Property LastState() As TspExecutionState
        Get
            Return Me._state
        End Get
        Set(ByVal Value As TspExecutionState)
            Me._state = Value
        End Set
    End Property

    ''' <summary>Gets or sets the instrument Execution State caption
    ''' </summary>
    Public ReadOnly Property StateCaption() As String
        Get
            Return isr.Core.EnumExtensions.Description(Me._state)
        End Get
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Sends wait complete to the instrument.
    ''' </summary>
    Public Function IssueWaitComplete() As Boolean
        Return Me._io.WriteLine(TspSyntax.WaitCommand)
    End Function

    ''' <summary>
    ''' Clears the status.
    ''' </summary>
    Public Function ClearStatus() As Boolean

        ' clear the stacks
        showErrorsStack.Clear()
        showPromptsStack.Clear()

        If Me._io.IsConnected Then

            Me._state = isr.Tsp.TspExecutionState.IdleReady

        Else

            Me._state = isr.Tsp.TspExecutionState.Closed

        End If

        Return True

    End Function

    ''' <summary>Restores the status of errors and prompts.
    ''' </summary>
    Public Sub RestoreStatus()

        Me.ShowErrorsSetter(showErrorsStack.Pop, Visa.ResourceAccessLevels.Device)
        Me.ShowPromptsSetter(showPromptsStack.Pop, Visa.ResourceAccessLevels.Device)

    End Sub

    ''' <summary>
    ''' Resets the defaults status.  Initializes properties
    ''' based on the status of the instrument.
    ''' Call after the instrument is reset.
    ''' </summary>
    Public Function ResetKnownState() As Boolean

        ' clear elements.
        Me.ClearStatus()

        ' get prompt and errors status
        If Me._io.IsConnected Then

            ' read the prompts status
            Me.ShowPromptsGetter(Visa.ResourceAccessLevels.Device)

            ' read the errors status
            Me.ShowErrorsGetter(Visa.ResourceAccessLevels.Device)

        End If

        Return True

    End Function

    ''' <summary>
    ''' Saves the current status of errors and prompts.
    ''' </summary>
    Public Sub StoreStatus()

        showErrorsStack.Push(Me.ShowErrorsGetter(Visa.ResourceAccessLevels.None))
        showPromptsStack.Push(Me.ShowPromptsGetter(Visa.ResourceAccessLevels.None))

    End Sub

#End Region

End Class