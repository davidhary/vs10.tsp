''' <summary>Defines a SCPI Base Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public NotInheritable Class TspSyntax

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Private Sub New()
        ' instantiate the base class
        MyBase.New()
    End Sub

#End Region

#Region " CONSTANTS "

    ''' <summary>Gets the unknown values for values that go to the datalog
    ''' but not for creating the data file name.
    ''' </summary>
    ''' <history>
    ''' 1.14.2265  David Hary
    ''' </history>
    Public Const UnknownValue As String = "N/A"

    ''' <summary>Gets the set of characters that should not be used in a
    ''' script name.</summary>
    Public Const IllegalScriptNameCharacters As String = "./\\"

    ''' <summary>
    ''' Gets the global node reference.
    ''' </summary>
    Public Const GlobalNode As String = "_G"

    ''' <summary>
    ''' Gets the global node reference.
    ''' </summary>
    Public Const LocalNode As String = "_G.localnode"

    ''' <summary>
    ''' Represents the LUA nil value.
    ''' </summary>
    Public Const NilValue As String = "nil"

    ''' <summary>
    ''' Represents the LUA true value
    ''' </summary>
    Public Const TrueValue As String = "true"

    ''' <summary>
    ''' Represents the LUA false value
    ''' </summary>
    Public Const FalseValue As String = "false"

    ''' <summary>
    ''' Gets the continuation prompt.
    ''' </summary>
    Public Const ContinuationPrompt As String = ">>>>"

    ''' <summary>
    ''' Gets the ready prompt.
    ''' </summary>
    Public Const ReadyPrompt As String = "TSP>"

    ''' <summary>
    ''' Gets the error prompt.
    ''' </summary>
    Public Const ErrorPrompt As String = "TSP?"

#End Region

#Region " CHUNK CONSTANTS "

    ''' <summary>Gets the chunk defining a start of comment block.</summary>
    Public Const StartCommentChunk As String = "--[["

    ''' <summary>Gets the chunk defining an end of comment block.</summary>
    Public Const EndCommentChunk As String = "]]--"

    ''' <summary>Gets the chunk defining a comment.</summary>
    Public Const CommentChunk As String = "--"

    ''' <summary>Gets the signature of a chunk line defining a chunk name.</summary>
    Public Const DeclareChunkNameSignature As String = "local chunkName ="

    ''' <summary>Gets the signature of a chunk line defining a require statement for the chunk name.</summary>
    Public Const RequireChunkNameSignature As String = "require("""

    ''' <summary>Gets the signature of a chunk line defining a loaded statement for the chunk name.</summary>
    Public Const LoadedChunkNameSignature As String = "_G._LOADED["

#End Region

#Region " NODE COMMANDS "

    ''' <summary>Gets the status clear (Cls) command message. Requires a node number argument.</summary>
    Public Const CollectNodeGarbage As String = "_G.node[{0}].execute('collectgarbage()') _G.waitcomplete({0})"

    ''' <summary>Gets the execute command.  Requires node number and command arguments.</summary>
    Public Const ExecuteCommand As String = "_G.node[{0}].execute(""{1}"") _G.waitcomplete({0})"

    ''' <summary>Gets the value returned by executing a command on the node.
    ''' Requires node number and value to get arguments.</summary>
    Public Const ValueGetterCommand1 As String = "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete({0}) _G.waitcomplete() _G.print( _G.node[{0}].dataqueue.next())"
    '  3517 "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete(0) _G.print( _G.node[{0}].dataqueue.next())"

    ''' <summary>Gets the value returned by executing a command on the node.
    ''' Requires node number, command, and value to get arguments.</summary>
    Public Const ValueGetterCommand2 As String = "_G.node[{0}].execute(""do {1} dataqueue.add({2}) end"") _G.waitcomplete({0}) _G.waitcomplete() _G.print( _G.node[{0}].dataqueue.next())"
    '  3517 "_G.node[{0}].execute(""do {1} dataqueue.add({2}) end"") _G.waitcomplete(0) _G.print( _G.node[{0}].dataqueue.next())"

    ''' <summary>Gets the connect rule command. Requires node number and value arguments.</summary>
    Public Const ConnectRuleSetter As String = "_G.node[{0}].channel.connectrule = {1}  _G.waitcomplete({0})"

    ''' <summary>
    ''' Gets a command to retrieve a catalog from the local node.
    ''' This command must be enclosed in a 'do end' construct.
    ''' a print(names) or dataqueue.add(names) needs to be added to get the data through.
    ''' </summary>
    Public Const ScriptCatalogGetterCommand As String = "local names='' for name in script.user.catalog() do names = names .. name .. ',' end"

#End Region

#Region " SYSTEM COMMANDS "

    ''' <summary>Gets the status clear (Cls) command message.</summary>
    Public Const CollectGarbageCommand As String = "_G.collectgarbage()"

    ''' <summary>Gets the IDN query command.</summary>
    Private Const _identifyQueryCommandFormat As String = "_G.print(""Keithley Instruments Inc., Model ""..{0}.model.."", ""..{0}.serialno.."", ""..{0}.revision)"

    ''' <summary>
    ''' Returns the identity query command.
    ''' </summary>
    Public Shared Function IdentifyQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._identifyQueryCommandFormat, subsystem)
    End Function

    ''' <summary>
    ''' Gets or set the operation completed command.
    ''' </summary>
    Public Const OperationCompletedCommand As String = "_G.opc()"

    ''' <summary>
    ''' Gets or set the operation completed query command.
    ''' </summary>
    Public Const OperationCompletedQueryCommand As String = "_G.waitcomplete() print(""1"")"

    ''' <summary>
    ''' Gets or set the wait command.
    ''' </summary>
    Public Const WaitCommand As String = "_G.waitcomplete()"

    ''' <summary>
    ''' Gets the function call command for a function w/o arguments.
    ''' </summary>
    Private Const _callFunctionCommandFormat As String = "_G.pcall( {0} )"

    ''' <summary>
    ''' Gets the function call command for a function with arguments.
    ''' </summary>
    Private Const _callFunctionArgumentsCommandFormat As String = "_G.pcall( {0} , {1} )"

    ''' <summary>
    ''' Returns a command to run the specified function with arguments.
    ''' </summary>
    ''' <param name="functionName">Specifies the function name.</param>
    ''' <param name="args">Specifies the function arguments.</param>
    Public Shared Function CallFunctionCommand(ByVal functionName As String, ByVal args As String) As String

        If String.IsNullOrWhiteSpace(args) Then
            Return Build(TspSyntax._callFunctionCommandFormat, functionName)
        Else
            Return Build(TspSyntax._callFunctionArgumentsCommandFormat, functionName, args)
        End If

    End Function

    Private Const _printCommandFormat As String = "_G.print({0})"
    ''' <summary>
    ''' Returns the print command for the specified arguments.
    ''' </summary>
    Public Shared Function PrintCommand(ByVal args As String) As String
        Return Build(TspSyntax._printCommandFormat, args)
    End Function

    Public Const ShowErrorsQueryCommand As String = "_G.print( _G.localnode.showerrors)"
    Private Const _showErrorsCommandFormat As String = "_G.localnode.showerrors = {0}"
    ''' <summary>
    ''' Returns the show error command.
    ''' </summary>
    Public Shared Function ShowErrorsCommand(ByVal value As Boolean) As String
        Return TspSyntax.Build(TspSyntax._showErrorsCommandFormat, isr.Core.IIf(Of Integer)(value, 1, 0))
    End Function

    Public Const ShowPromptsQueryCommand As String = "_G.print( _G.localnode.prompts)"
    Private Const _showPromptsCommandFormat As String = "_G.localnode.prompts = {0}"
    ''' <summary>
    ''' Returns the show prompts command.
    ''' </summary>
    Public Shared Function ShowPromptsCommand(ByVal value As Boolean) As String
        Return TspSyntax.Build(TspSyntax._showPromptsCommandFormat, isr.Core.IIf(Of Integer)(value, 1, 0))
    End Function

#End Region

#Region " NODE COMMANDS "

    ''' <summary>Gets the reset command.</summary>
    Private Const _resetKnownStateCommandFormat As String = "{0}.reset()"

    ''' <summary>
    ''' Returns the reset command.
    ''' </summary>
    Public Shared Function ResetKnownStateCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._resetKnownStateCommandFormat, subsystem)
    End Function

#End Region

#Region " REGISTER COMMANDS "

    ''' <summary>Gets the status clear (Cls) command message.</summary>
    Private Const _clearExecutionStateCommandFormat As String = "{0}.status.reset()"

    ''' <summary>
    ''' Returns the clear execution state command.
    ''' </summary>
    Public Shared Function ClearExecutionStateCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._clearExecutionStateCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the error queue clear command message.</summary>
    Private Const _clearErrorQueueCommandFormat As String = "{0}.errorqueue.clear()"

    ''' <summary>
    ''' Returns the clear error queue command.
    ''' </summary>
    Public Shared Function ClearErrorQueueCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._clearErrorQueueCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the error queue query command message.</summary>
    Private Const _ErrorQueueQueryCommandFormat As String = "_G.print(string.format('%d,%s,level=%d',{0}.errorqueue.next()))"

    ''' <summary>
    ''' Returns the error queue query command.
    ''' </summary>
    Public Shared Function ErrorQueueQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._ErrorQueueQueryCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the error queue count query command message.</summary>
    Private Const _ErrorQueueCountQueryCommandFormat As String = "_G.print({0}.errorqueue.count)"

    ''' <summary>
    ''' Returns the error queue count query command.
    ''' </summary>
    Public Shared Function ErrorQueueCountQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._ErrorQueueCountQueryCommandFormat, subsystem)
    End Function

#End Region

#Region " OPERATION EVENTS "

    ''' <summary>Gets the operation event enable command.</summary>
    Private Const _operationEventEnableCommandFormat As String = "{0}.status.operation.enable = {{0}}"

    ''' <summary>
    ''' Returns the operation event enable command.
    ''' </summary>
    Public Shared Function OperationEventEnableCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._operationEventEnableCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the operation event enable query command.</summary>
    Private Const _operationEventEnableQueryCommandFormat As String = "_G.print( _G.tostring({0}.status.operation.enable))"

    ''' <summary>
    ''' Returns the operation event enable query command.
    ''' </summary>
    Public Shared Function OperationEventEnableQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._operationEventEnableQueryCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the operation event status query command.</summary>
    Private Const _operationEventStatusQueryCommandFormat As String = "_G.print( _G.tostring({0}.status.operation.event))"

    ''' <summary>
    ''' Returns the operation event status query command.
    ''' </summary>
    Public Shared Function OperationEventStatusQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._operationEventStatusQueryCommandFormat, subsystem)
    End Function

#End Region

#Region " STANDARD EVENTS "

    ''' <summary>Gets the standard event enable command.</summary>
    Private Const _standardEventEnableCommandFormat As String = "{0}.status.standard.enable = {{0}}"

    ''' <summary>
    ''' Returns the standard event enable command.
    ''' </summary>
    Public Shared Function StandardEventEnableCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._standardEventEnableCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the standard event enable query command.</summary>
    Private Const _standardEventEnableQueryCommandFormat As String = "_G.print( _G.tostring({0}.status.standard.enable))"

    ''' <summary>
    ''' Returns the standard event enable query command.
    ''' </summary>
    Public Shared Function StandardEventEnableQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._standardEventEnableQueryCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the standard event status query command.</summary>
    Private Const _standardEventStatusQueryCommandFormat As String = "_G.waitcomplete() _G.print( _G.tostring({0}.status.standard.event))"
    '  3517 "_G.waitcomplete(0) _G.print( _G.tostring({0}.status.standard.event))"

    ''' <summary>
    ''' Returns the standard event status query command.
    ''' </summary>
    Public Shared Function StandardEventStatusQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._standardEventStatusQueryCommandFormat, subsystem)
    End Function

#End Region

#Region " SERVICE REQUEST "

    ''' <summary>Gets the service request enable command.</summary>
    Private Const _serviceRequestEnableCommandFormat As String = "{0}.status.request_enable = {{0}}"

    ''' <summary>
    ''' Returns the service request enable command.
    ''' </summary>
    Public Shared Function ServiceRequestEnableCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._serviceRequestEnableCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the service request enable query command.</summary>
    Private Const _serviceRequestEnableQueryCommandFormat As String = "_G.print( _G.tostring({0}.status.request_enable))"

    ''' <summary>
    ''' Returns the service request enable query command.
    ''' </summary>
    Public Shared Function ServiceRequestEnableQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._serviceRequestEnableQueryCommandFormat, subsystem)
    End Function

    ''' <summary>Gets the service request enable query command.</summary>
    Private Const _serviceRequestStatusQueryCommandFormat As String = "_G.print( _G.tostring({0}.status.condition))"

    ''' <summary>
    ''' Returns the service request enable query command.
    ''' </summary>
    Public Shared Function ServiceRequestStatusQueryCommand(ByVal subsystem As String) As String
        Return TspSyntax.Build(TspSyntax._serviceRequestStatusQueryCommandFormat, subsystem)
    End Function

#End Region

#Region " ELEMENT BUILDERS "

    Private Const _nodeReferenceFormat As String = "_G.node[{0}]"
    ''' <summary>Returns a TSP reference to the specified node.
    ''' </summary>
    ''' <param name="node">Specifies the one-based node number.
    ''' </param>
    Public Shared Function NodeReference(ByVal node As Integer) As String

        If node = 1 Then
            ' if node is number one, use the local node as reference in case
            ' we do not have other nodes.
            Return TspSyntax.LocalNode
        Else
            Return TspSyntax.Build(TspSyntax._nodeReferenceFormat, node)
        End If

    End Function

    ''' <summary>
    ''' Gets the global node reference.
    ''' </summary>
    Private Const _localNodeSmuFormat As String = "_G.localnode.smu{0}"

    Private Const _smuReferenceFormat As String = "_G.node[{0}].smu{1}"
    ''' <summary>Returns a TSP reference to the specified SMU on the specified node.
    ''' </summary>
    ''' <param name="node">Specifies the one-based node number.
    ''' </param>
    ''' <param name="smu">Specifies the SMU (either 'a' or 'b'.
    ''' </param>
    Public Shared Function SmuReference(ByVal node As Integer, ByVal smu As String) As String

        If node = 1 Then
            ' if node is number one, use the local node as reference in case
            ' we do not have other nodes.
            Return TspSyntax.Build(TspSyntax._localNodeSmuFormat, smu)
        Else
            Return TspSyntax.Build(TspSyntax._smuReferenceFormat, node, smu)
        End If

    End Function

#End Region

#Region " COMMAND BUILDERS "

    ''' <summary>
    ''' Builds a command.
    ''' </summary>
    ''' <param name="format">Specifies a format string for the command.</param>
    ''' <param name="args">Specifies the arguments for the command.</param>
    Public Shared Function Build(ByVal format As String, ByVal ParamArray args() As Object) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
    End Function

#End Region

End Class
