Imports isr.Core.EventHandlerExtensions
''' <summary>
''' Defines a Test Script Processor (TSP) System consists of one or more
''' TSP nodes.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/21/2007" by="David" revision="1.15.2636.x">
''' Created
''' </history>
Public Class TspSystem
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        Me.New(String.Empty)

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    Public Sub New(ByVal instanceName As String)

        MyBase.New()

        Me._instanceName = instanceName

        nodes = New isr.Tsp.DisposableCollection(Of TspNode)
        smus = New isr.Tsp.DisposableCollection(Of TspSmu)

        Me._VisaIO = New isr.Tsp.TspVisaIO(instanceName)

        ' Set the default values
        Me.UniqueId = Me._UniqueId

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me.ConnectionChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ConnectionChangedEvent.GetInvocationList
                            RemoveHandler Me.ConnectionChanged, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                        Next
                    End If

                    If Me.MessageAvailableEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                            RemoveHandler Me.MessageAvailable, CType(d, Global.System.EventHandler(Of isr.Core.MessageEventArgs))
                        Next
                    End If

                    If Me._TspScript IsNot Nothing Then
                        Me._TspScript.Dispose()
                        Me._TspScript = Nothing
                    End If

                    If Me._TspUserScript IsNot Nothing Then
                        Me._TspUserScript.Dispose()
                        Me._TspUserScript = Nothing
                    End If

                    If Me._TspData IsNot Nothing Then
                        Me._TspData.Dispose()
                        Me._TspData = Nothing
                    End If

                    If Me._VisaIO IsNot Nothing Then
                        If Me._VisaIO.IsConnected Then
                            Me._VisaIO.Disconnect()
                        End If
                        Me._VisaIO.Dispose()
                        Me._VisaIO = Nothing
                    End If

                    If nodes IsNot Nothing Then
                        nodes.Dispose()
                        nodes = Nothing
                    End If

                    If smus IsNot Nothing Then
                        smus.Dispose()
                        smus = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

    Private _lastMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    <System.ComponentModel.Browsable(False)> Public ReadOnly Property LastMessage() As String
        Get
            Return Me._lastMessage
        End Get
    End Property

#End Region

#Region " PROPERRIES "

    ''' <summary>Gets or sets reference to the collection of
    ''' <see cref="isr.Tsp.DisposableCollection(Of TspNode)">TSP nodes</see>.
    ''' </summary>
    Private nodes As isr.Tsp.DisposableCollection(Of TspNode)

    ''' <summary>Gets or sets reference to the collection of
    ''' <see cref="isr.Tsp.DisposableCollection(Of TspSmu)">TSP Smus</see>.
    ''' </summary>
    Private smus As isr.Tsp.DisposableCollection(Of TspSmu)

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA I/O Interface</see>.
    ''' </summary>
    Private WithEvents _VisaIO As isr.Tsp.TspVisaIO

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">Gpib Base Instrument</see>.
    ''' </summary>
    Public ReadOnly Property VisaIO() As isr.Tsp.TspVisaIO
        Get
            Return Me._VisaIO
        End Get
    End Property

    ''' <summary>Gets or sets the condition for determining if the
    ''' instrument is connected.
    ''' </summary>
    Public ReadOnly Property IsConnected() As Boolean
        Get
            ' Get the connected state
            Return Me._VisaIO.IsConnected
        End Get
    End Property

    ''' <summary>Gets or sets the condition for using actual hardware.
    ''' </summary>
    Public Property UsingDevices() As Boolean
        Get
            Return Me._VisaIO.UsingDevices
        End Get
        Set(ByVal Value As Boolean)
            Me._VisaIO.UsingDevices = Value
        End Set
    End Property

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspData">TSP data manager</see>.
    ''' </summary>
    Private WithEvents _TspData As isr.Tsp.TspData

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspData">TSP data manager</see>.
    ''' </summary>
    Public ReadOnly Property TspData() As isr.Tsp.TspData
        Get
            Return Me._TspData
        End Get
    End Property

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspScript">TSP script manager</see>.
    ''' </summary>
    Private WithEvents _TspScript As isr.Tsp.TspScript

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspScript">TSP script manager</see>.
    ''' </summary>
    Public ReadOnly Property TspScript() As isr.Tsp.TspScript
        Get
            Return Me._TspScript
        End Get
    End Property

    ''' <summary>Gets reference to the
    ''' <see cref="isr.Tsp.TspStatus">TSP Status Manager</see>.</summary>
    Public ReadOnly Property TspStatus() As isr.Tsp.TspStatus
        Get
            Return Me.VisaIO.TspStatus
        End Get
    End Property

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspUserScript">TSP Use Script manager</see>.
    ''' </summary>
    Private WithEvents _TspUserScript As isr.Tsp.TspUserScript

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspUserScript">TSP User script manager</see>.
    ''' </summary>
    Public ReadOnly Property TspUserScript() As isr.Tsp.TspUserScript
        Get
            Return Me._TspUserScript
        End Get
    End Property

    ''' <summary>Gets or sets the unique object ID.</summary>
    Private _UniqueId As Integer

    ''' <summary>Gets or sets the value of the object ID.
    ''' This must be a unique property that can serve to
    ''' index the object in a collection of objects.
    ''' </summary>
    Public Property UniqueId() As Integer
        Get
            Return Me._UniqueId
        End Get
        Set(ByVal Value As Integer)

            ' Set the object ID
            Me._UniqueId = Value

            ' Set the ID string of this object
            Me._uniqueKey = TspSystem.BuildUniqueKey(Me._UniqueId)

        End Set
    End Property

    Public Const DefaultUniqueKeyFormat As String = "00"

    ''' <summary>
    ''' Builds a default unique key based on an ID or serial number using 
    ''' the <see cref="DefaultUniqueKeyFormat">default format.</see>.
    ''' </summary>
    ''' <param name="uniqueId">Specifies the unique ID for the format.</param>
    Public Shared Function BuildUniqueKey(ByVal uniqueId As Integer) As String

        Return TspSystem.BuildUniqueKey(uniqueId, DefaultUniqueKeyFormat)

    End Function

    ''' <summary>
    ''' Builds a default unique key based on an ID or serial number using 
    ''' the <see cref="DefaultUniqueKeyFormat">default format.</see>.
    ''' </summary>
    ''' <param name="uniqueId">Specifies the unique ID for the format.</param>
    ''' <param name="format">Specifies the numeric format string for creating the KEY.</param>
    Public Shared Function BuildUniqueKey(ByVal uniqueId As Integer, ByVal format As String) As String

        Return uniqueId.ToString(format, Globalization.CultureInfo.CurrentCulture)

    End Function


    ''' <summary>Gets or sets the unique object key.</summary>
    Private _uniqueKey As String

    ''' <summary>Gets or sets the unique object key.</summary>
    Public ReadOnly Property UniqueKey() As String
        Get
            Return Me._uniqueKey
        End Get
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Adds a Node to the TSP instrument.
    ''' </summary>
    ''' <param name="node">Specifies the one-based node number.
    ''' </param>
    Public Function AddTspNode(ByVal node As Integer) As isr.Tsp.TspNode

        Return nodes.AddItem(New isr.Tsp.TspNode(Me._VisaIO, node))

    End Function

    ''' <summary>
    ''' Adds a SMU to the TSP instrument.
    ''' </summary>
    ''' <param name="node">Specifies the one-based node number.
    ''' </param>
    ''' <param name="smu">Specifies the SMU (either 'a' or 'b'.
    ''' </param>
    Public Function AddTspSmu(ByVal node As Integer, ByVal smu As String) As isr.Tsp.TspSmu

        Return smus.AddItem(New isr.Tsp.TspSmu(Me._VisaIO, node, smu))

    End Function

    ''' <summary>
    ''' Clears all status data structure registers (enable, event, NTR and PTR)
    ''' to their power up states.  The naked TSP clear does not clear the
    ''' error queue.  THis command adds clear for error queue.
    ''' </summary>
    Public Function ClearExecutionState() As Boolean

        ' clear the base instrument.
        ClearExecutionState = Me._VisaIO.ClearExecutionState
        Me.TspStatus.ReadAndParseState()

        ' Set all cached values that get reset by Cls
        Me.TspStatus.ClearStatus()

    End Function

    ''' <summary>
    ''' Sends a collect garbage command.
    ''' </summary>
    Public Function CollectGarbage() As Boolean

        ' send the message
        Return Me.VisaIO.CollectGarbage()

    End Function

    ''' <summary>Connects to the instrument.
    ''' Opens an I/O session to the instrument making driver methods and properties
    ''' accessible. Performs a Reset and queries the instrument to validate the
    ''' instrument model.
    ''' </summary>
    ''' <returns>
    ''' </returns>
    ''' <remarks>
    ''' Use this method to connect to the instrument.
    ''' Creates the connection between the
    ''' control and a device.
    ''' Once the isr27xx object is created by the
    ''' container application, this method is first one that
    ''' needs to be invoked to make the initial connection
    ''' to the instrument.
    ''' </remarks>
    ''' <example>
    ''' This example initializes the instrument and sends
    ''' and receives some data.
    '''   Dim K2600 As isr.Tsp.TspSystem
    '''   Set K2600 = New isr.Tsp.TspSystem
    '''   K2600.UniqueID = 1
    '''   K2600.IsUsingDevices = True
    '''   If K2600.Connect("GPIB0::26:INSTR") Then
    '''     K2600.ResetKnownState
    '''     K2600.ClearStatus
    '''     Me.Print K2600.VisaIO.QueryString("*IDN?", True)
    '''     K2600.TspStatus.ReadAndParseState
    '''     K2600.Disconnect
    '''     K2600.Dispose
    '''     Set K2600 = Nothing
    '''   End If
    ''' </example>
    Public Function Connect(ByVal resourceName As String) As Boolean

        ' close the device if open
        If Me.IsConnected Then
            Me.Disconnect()
        End If

        Return Me._VisaIO.Connect(resourceName)

    End Function

    ''' <summary>Closes the device.
    ''' Performs the necessary termination
    ''' functions, which will cleanup and disconnect the
    ''' interface connection.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub Disconnect()

        If Me._VisaIO IsNot Nothing Then

            ' Close the device
            Me._VisaIO.Disconnect()

        End If

    End Sub

    ''' <summary>Removes the script from the device.
    ''' Updates the script list.
    ''' </summary>
    Public Function RemoveScript(ByVal scriptName As String) As Boolean

        If Me.VisaIO.WriteLine(scriptName & " = nil") Then
            Me.CollectGarbage()
            Me.TspScript.FetchUserScriptNames()
            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary>Resets and clears the device.
    ''' Issues selective-device-clear,
    ''' reset known state (Rst), Clear Execution State (Cls), and clear error
    ''' queue.
    ''' </summary>
    Public Function ResetAndClear() As Boolean

        Return Me._VisaIO.ResetAndClear

    End Function

    ''' <summary>Returns the device to the reset defaults.
    ''' </summary>
    Public Function ResetKnownState() As Boolean

        ' Rst issues the reset() message, which returns all the nodes
        ' on the TSP LInk system to the original factory defaults:
        Return Me._VisaIO.ResetKnownState

    End Function

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>Raised to update the instrument connection ExecutionState.</summary>
    ''' <param name="sender">Specifies reference to the <see cref="isr.Tsp.TspSystem">TSP system.</see>.
    ''' </param>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event ConnectionChanged As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises an event to alert on change of connection.
    ''' </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Public Sub OnConnectionChanged(ByVal e As System.EventArgs)

        Dim evt As EventHandler(Of EventArgs) = Me.ConnectionChangedEvent
        If evt IsNot Nothing Then evt.SafeInvoke(Me, e)

    End Sub

    ''' <summary>
    ''' Instantiate the data and script manages and raises the connection changed event.
    ''' </summary>
    Private Sub _VisaIO_ConnectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _VisaIO.ConnectionChanged

        If Me._VisaIO.IsConnected Then

            Me._VisaIO.OnMessageAvailable(TraceEventType.Verbose, "Opening a data manager for {0}", Me._VisaIO.ResourceName)
            Me._TspData = New isr.Tsp.TspData(Me._VisaIO)

            Me._VisaIO.OnMessageAvailable(TraceEventType.Verbose, "Opening a user script manager manager for {0}", Me._VisaIO.ResourceName)
            Me._TspUserScript = New isr.Tsp.TspUserScript(Me._VisaIO, Me.TspStatus)

            Me._VisaIO.OnMessageAvailable(TraceEventType.Verbose, "Opening a script manager manager for {0}", Me._VisaIO.ResourceName)
            Me._TspScript = New isr.Tsp.TspScript(Me._VisaIO, Me.TspStatus)

        Else

            ' clear the notes and SMU collections.
            nodes = New isr.Tsp.DisposableCollection(Of TspNode)
            smus = New isr.Tsp.DisposableCollection(Of TspSmu)

            If Me._TspScript IsNot Nothing Then
                Me._TspScript.Dispose()
                Me._TspScript = Nothing
            End If

            If Me._TspUserScript IsNot Nothing Then
                Me._TspUserScript.Dispose()
                Me._TspUserScript = Nothing
            End If

            If Me._TspData IsNot Nothing Then
                Me._TspData.Dispose()
                Me._TspData = Nothing
            End If

            ' set the state to closed
            Me.TspStatus.LastState = isr.Tsp.TspExecutionState.Closed

        End If

        Me.OnConnectionChanged(e)
        System.Windows.Forms.Application.DoEvents()

    End Sub

    ''' <summary>Occurs when the element or instrument has a message
    ''' </summary>
    ''' <param name="sender">Specifies reference to the <see cref="isr.Tsp.TspSystem">TSP system.</see>.
    ''' </param>
    ''' <param name="e">Specifies the <see cref="EventArgs">event arguments</see>.
    ''' </param>
    Public Event MessageAvailable As EventHandler(Of isr.Core.MessageEventArgs)

    ''' <summary>
    ''' Raises the message available event.
    ''' </summary>
    Private Sub _VisaIO_MessageAvailable(ByVal sender As Object, ByVal e As isr.Core.MessageEventArgs) Handles _VisaIO.MessageAvailable
        Me._lastMessage = e.Details
        Dim evt As EventHandler(Of isr.Core.MessageEventArgs) = Me.MessageAvailableEvent
        If evt IsNot Nothing Then evt.SafeBeginInvoke(Me, e)
    End Sub

#End Region

End Class