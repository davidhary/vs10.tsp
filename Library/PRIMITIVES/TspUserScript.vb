''' <summary>
''' Loads and runs TSP scripts.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x">
''' Created
''' </history>
Public Class TspUserScript
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Private _tspStatus As TspStatus
    ''' <summary>
    ''' Initializes a new instance of the <see cref="TspUserScript" /> class.
    ''' </summary>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    ''' <param name="status">The status.</param>
    Public Sub New(ByVal io As isr.Tsp.TspVisaIO, ByVal status As TspStatus)
        Me.New(String.Empty, io)
        Me._tspStatus = status
    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    ''' <param name="io">Specifies reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.</param>
    Public Sub New(ByVal instanceName As String, ByVal io As isr.Tsp.TspVisaIO)

        MyBase.New()

        Me._instanceName = instanceName

        Me._visaIO = io
        Me._lastFetchedSavedScripts = ""

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                    ' terminate reference to the resources.
                    If Me._visaIO IsNot Nothing Then
                        Me._visaIO = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " PROPERRIES "

    ''' <summary>Gets or sets reference to the
    ''' <see cref="isr.Tsp.TspVisaIo">VISA IO</see>.
    ''' </summary>
    Private _visaIO As isr.Tsp.TspVisaIO

    ''' <summary>Gets or sets the script name.
    ''' </summary>
    Private _name As String

    ''' <summary>Gets or sets the script name.
    ''' </summary>
    Public Property Name() As String
        Get
            Return Me._name
        End Get
        Set(ByVal Value As String)

            If isr.Core.IncludesAny(Value, TspSyntax.IllegalScriptNameCharacters) Then
                ' now report the error to the calling module
                Throw New isr.Tsp.IllegalScriptNameException(Value, TspSyntax.IllegalScriptNameCharacters)
            Else
                Me._name = Value
            End If

        End Set
    End Property

    ''' <summary>Gets or sets the script file name.
    ''' </summary>
    Public Property FilePath() As String

#End Region

#Region " SCRIPT SOURCE "

    ''' <summary>
    ''' Clears the the <see cref="Name">specified</see> script source.
    ''' </summary>
    Public Function ClearScriptSource() As Boolean
        Return Me.ClearScriptSource(Me._name)
    End Function

    ''' <summary>
    ''' Clears the <paramref name="value">specified</paramref> script source.
    ''' </summary>
    ''' <param name="value">Specifies the script name.</param>
    Public Function ClearScriptSource(ByVal value As String) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        Return Me._visaIO.WriteLine("{0}.source = nil ", value)
    End Function

    ''' <summary>
    ''' Lists the <paramref name="value">specified</paramref> script.
    ''' </summary>
    ''' <param name="value">Specifies the script name.</param>
    ''' <param name="trimSpaces">Specifies a directive to trim leading and trailing spaces from each line</param>
    Public Function FetchScriptSource(ByVal value As String, ByVal trimSpaces As Boolean) As String
        If String.IsNullOrWhiteSpace(value) Then
            Return ""
        End If
        If Me._visaIO.WriteLine("print({0}.source)", value) Then
            Return Me._visaIO.ReadLines(10, 400, trimSpaces, Not trimSpaces)
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Check if a user script was saved as a binary script.
    ''' </summary>
    Public Function IsScriptSavedAsBinary(ByVal name As String) As Boolean
        Return Me._visaIO.IsStatementTrue("string.sub({0}.source,1,24) == 'loadstring(table.concat('", name)
    End Function

    ''' <summary>
    ''' Runs the named script.
    ''' </summary>
    ''' <param name="timeout">Specifies the time to wait for the instrument to 
    ''' return operation completed.</param>
    Public Function RunScript(ByVal timeout As Integer) As Boolean

        ' Check name
        If String.IsNullOrWhiteSpace(Me._name) Then
            Return False
        End If

        ' store state of prompts and errors.
        Me._tspStatus.StoreStatus()

        Try

            ' Disable automatic display of errors - leave error messages in queue and enable error Prompt.
            Me._tspStatus.ShowErrorsSetter(False, Visa.ResourceAccessLevels.Device)

            ' Turn off prompts
            Me._tspStatus.ShowPromptsSetter(False, Visa.ResourceAccessLevels.Device)

            Dim returnedValue As Integer = 1
            If Me._visaIO.WriteLine("{0}.run() waitcomplete() print('{1}') ", Me._name, returnedValue) Then

                ' wait till we get a reply from the instrument or timeout.
                Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))
                Do
                    Threading.Thread.Sleep(50)
                Loop Until endTime < DateTime.Now Or Me._visaIO.IsMessageAvailable
                If Me._visaIO.IsMessageAvailable Then
                    Dim value As Integer? = Me._visaIO.ReadInt32()
                    Return value.HasValue AndAlso value.Value = returnedValue
                Else
                    Throw New isr.Tsp.ScriptCallException("Timeout waiting operation completion running the script '" & Me._name & "'")
                End If

            Else

                Return False

            End If

        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        Finally

            ' restore state of prompts and errors.
            Me._tspStatus.RestoreStatus()

            ' add a wait to ensure the system returns the last status.
            Threading.Thread.Sleep(100)

            ' flush the buffer until empty to completely reading the status.
            Me._visaIO.DiscardUnreadData(10, 100, True)

        End Try

    End Function

#End Region

#Region " SAVED SCRIPTS "

    ''' <summary>
    ''' Makes a script nil
    ''' </summary>
    Public Function NilifyScript(ByVal value As String) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        If Me._visaIO.IsStatementTrue("{0} == nil", value) Then
            ' not supported to happen
            Return True
        Else
            If Me._visaIO.WriteLine("{0} = nil", value) Then
                Return Me._visaIO.IsStatementTrue("{0} == nil", value)
            Else
                Return False
            End If
        End If
    End Function

    ''' <summary>
    ''' Deletes the <paramref name="value">specified</paramref> script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    Public Function DeleteScript(ByVal value As String, ByVal refreshScriptCatalog As Boolean) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If

        If Me.SavedScriptExists(value, refreshScriptCatalog) Then
            Return Me.DeleteSavedScript(value, False)
        Else
            Return Me.NilifyScript(value)
        End If

    End Function

    ''' <summary>
    ''' Deletes the <paramref name="value">specified</paramref> saved script.
    ''' Also nilifies the script if delete command worked.
    ''' </summary>
    Public Function DeleteSavedScript(ByVal value As String, ByVal refreshScriptCatalog As Boolean) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        If Me.SavedScriptExists(value, refreshScriptCatalog) Then
            If Me._visaIO.WriteLine("script.delete( '{0}' )", value) Then
                ' make script nil
                If Me.NilifyScript(value) Then
                    ' make sure to re-check that script is gone.
                    Return Not Me.SavedScriptExists(value, True)
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Deletes the <see cref="Name">specified</see> script.
    ''' </summary>
    Public Function DeleteSavedScript(ByVal refreshScriptCatalog As Boolean) As Boolean
        Return Me.DeleteSavedScript(Me._name, refreshScriptCatalog)
    End Function

    ''' <summary>
    ''' Fetches the list of saved scripts and saves it in the <see cref="LastFetchedSavedScripts"></see>
    ''' </summary>
    Public Function FetchSavedScripts() As Boolean
        Me._lastFetchedSavedScripts = ""
        If Me._visaIO.WriteQueryLine("do local names='' for name in script.user.catalog() do names = names .. name .. ',' end print( names ) end") Then
            Me._lastFetchedSavedScripts = Me._visaIO.ReadLineTrimEnd
            If String.IsNullOrWhiteSpace(Me._lastFetchedSavedScripts) Then
                Me._lastFetchedSavedScripts = ""
            End If
            Return True
        End If
        Return False
    End Function

    Private _lastFetchedSavedScripts As String
    ''' <summary>
    ''' Gets a comma-separated and comma-terminated list of the saved scripts 
    ''' that was fetched last.  A new script is fetched after save and delete.
    ''' </summary>
    Public ReadOnly Property LastFetchedSavedScripts() As String
        Get
            Return Me._lastFetchedSavedScripts
        End Get
    End Property

    ''' <summary>
    ''' Saves the specifies script to non-volatile memory.
    ''' </summary>
    Public Function SaveScript(ByVal value As String) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        If Me._visaIO.WriteLine("{0}.save()", value) Then
            Return Me.SavedScriptExists(value, True)
        End If
    End Function

    ''' <summary>
    ''' Returns true if the script is not nil.
    ''' </summary>
    Public Function ScriptExists(ByVal value As String) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        Return Not Me._visaIO.IsNil(value)
    End Function

    ''' <summary>
    ''' Returns true if the specified script exists as a saved script.
    ''' </summary>
    ''' <param name="value">Specifies the script name</param>
    ''' <param name="refreshScriptCatalog">True to refresh the list of saved scripts.</param>
    Public Function SavedScriptExists(ByVal value As String, ByVal refreshScriptCatalog As Boolean) As Boolean
        If String.IsNullOrWhiteSpace(value) Then
            Return False
        End If
        If refreshScriptCatalog Then
            Me.FetchSavedScripts()
        End If
        Return Me._lastFetchedSavedScripts.IndexOf(value & ",", 0, StringComparison.OrdinalIgnoreCase) >= 0
    End Function

#End Region

#Region " LOAD SCRIPT "

    ''' <summary>
    ''' Load the code. Code could be embedded as a comma separated string table format, in 
    ''' which case the script should be concatenated first.
    ''' </summary>
    ''' <param name="scriptLines">Includes the script in lines.</param>
    Public Function LoadString(ByVal scriptLines() As String) As Boolean
        If scriptLines IsNot Nothing Then
            For Each scriptLine As String In scriptLines
                scriptLine = scriptLine.Trim
                If Not String.IsNullOrWhiteSpace(scriptLine) Then
                    If Not Me._visaIO.WriteLine(scriptLine) Then
                        Return False
                    End If
                End If
            Next
        End If
        Return True
    End Function

    ''' <summary>
    ''' Load the script embedded in the string.
    ''' </summary>
    ''' <param name="scriptLines">Contains the script code line by line</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function LoadScript(ByVal scriptLines() As String) As Boolean

        Try

            If Me._visaIO.WriteLine("loadscript " & Me._name) Then
                If Me.LoadString(scriptLines) Then
                    Return Me._visaIO.WriteLine("endscript")
                Else
                    Return False
                End If
            Else
                Return False
            End If

        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        End Try

    End Function

    ''' <summary>Loads a named script into the instrument.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function LoadScriptFileSimple() As Boolean

        Dim passed As Boolean

        Try
            Using tspFile As System.IO.StreamReader = Me.OpenScriptFile()
                passed = Not tspFile Is Nothing
                Dim isFirstLine As Boolean
                Dim line As String
                If passed Then
                    isFirstLine = True
                    Do While passed AndAlso Not tspFile.EndOfStream
                        line = tspFile.ReadLine.Trim
                        If Not String.IsNullOrWhiteSpace(line) Then
                            If isFirstLine Then
                                passed = Me._visaIO.WriteLine("loadscript " & Me._name)
                                isFirstLine = False
                            End If
                            If passed Then
                                passed = Me._visaIO.WriteLine(line)
                            End If
                        End If
                    Loop
                    If passed Then
                        passed = Me._visaIO.WriteLine("endscript")
                    End If
                End If
            End Using
            Return passed
        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        Finally
        End Try

    End Function

    ''' <summary>
    ''' Loads a named script into the instrument allowing control over how
    ''' errors and prompts are handled.
    ''' For loading a script that does not includes functions, turn off errors
    ''' and turn on the prompt.
    ''' </summary>
    ''' <param name="showErrors">Specifies the condition for turning off or on
    ''' error checking while the script is loaded.</param>
    ''' <param name="showPrompts">Specifies the condition for turning off or on
    ''' the TSP prompts while the script is loaded.</param>
    ''' <param name="retainOutline">Specifies if the code outline is retained or trimmed.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function LoadScriptFile(ByVal showErrors As Boolean, ByVal showPrompts As Boolean, ByVal retainOutline As Boolean) As Boolean

        ' store the status
        Me._tspStatus.StoreStatus()

        Dim chunkLine As String

        Try

            Dim deviceErrors As String = String.Empty

            Dim isInCommentBlock As Boolean
            isInCommentBlock = False

            Dim lineNumber As Integer
            lineNumber = 0

            ' flush the buffer until empty and update the TSP status.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            ' store state of prompts and errors.
            ' Me._TspStatus.StoreStatus()

            ' Disable automatic display of errors - leave error messages in queue
            ' and enable error Prompt. or otherwise...
            Me._tspStatus.ShowErrorsSetter(showErrors, Visa.ResourceAccessLevels.Device)

            ' Turn on prompts
            Me._tspStatus.ShowPromptsSetter(showPrompts, Visa.ResourceAccessLevels.Device)

            ' flush the buffer until empty and update the TSP status.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Dim isFirstLine As Boolean
            isFirstLine = True
            Dim commandLine As String
            Dim wasInCommentBlock As Boolean
            Dim lineType As isr.Tsp.TspChunkLineContentType

            Using tspFile As System.IO.StreamReader = Me.OpenScriptFile()

                If tspFile Is Nothing Then

                    ' now report the error to the calling module
                    Throw New BaseException("Failed opening TSP Script File '" & Me._FilePath & "'.")

                End If

                Using debugFile As System.IO.StreamWriter = New System.IO.StreamWriter(Me.FilePath & ".debug")

                    Do While Not tspFile.EndOfStream

                        chunkLine = TspUserScript.TrimTspChuckLine(tspFile.ReadLine, retainOutline)
                        lineNumber = lineNumber + 1
                        wasInCommentBlock = isInCommentBlock
                        lineType = TspUserScript.ParseTspChuckLine(chunkLine, isInCommentBlock)

                        If lineType = TspChunkLineContentType.None Then

                            ' if no data, nothing to do.

                        ElseIf wasInCommentBlock Then

                            ' if was in a comment block exit the comment block if
                            ' received a end of comment block
                            If lineType = isr.Tsp.TspChunkLineContentType.EndCommentBlock Then
                                isInCommentBlock = False
                            End If

                        ElseIf lineType = TspChunkLineContentType.StartCommentBlock Then

                            isInCommentBlock = True

                        ElseIf lineType = TspChunkLineContentType.Comment Then

                            ' if comment line do nothing

                        ElseIf lineType = TspChunkLineContentType.Syntax OrElse lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                            If lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                                chunkLine = chunkLine.Substring(0, chunkLine.IndexOf(TspSyntax.StartCommentChunk, StringComparison.OrdinalIgnoreCase))

                            End If

                            ' end each line with a space
                            chunkLine &= " "

                            If isFirstLine Then
                                ' issue a start of script command.  The command
                                ' 'loadscript' identifies the beginning of the named script.
                                commandLine = "loadscript " & Me._name & " "
                                If Me._visaIO.WriteLine(commandLine) AndAlso (Me._tspStatus.LastState <> isr.Tsp.TspExecutionState.IdleError) Then
                                    isFirstLine = False
                                Else
                                    ' now report the error to the calling module
                                    If Me._visaIO.HandleInstrumentErrorIfError(True) Then
                                        deviceErrors = Me._visaIO.DeviceErrors
                                    End If
                                    Dim messageFormat As String = "{0} failed sending a 'loadscript' for script '{1}' from file '{2}'.{4}Device Errors: {3}"
                                    Throw New ScriptLoadingException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                   messageFormat, Me._visaIO.ResourceName, Me._name,
                                                                                   Me._FilePath, deviceErrors, Environment.NewLine))
                                End If
                            End If

                            Select Case Me._tspStatus.LastState
                                Case isr.Tsp.TspExecutionState.IdleContinuation
                                    ' Continuation prompt. TSP received script line successfully; waiting for next line.
                                Case isr.Tsp.TspExecutionState.IdleError
                                    System.Diagnostics.Debug.Assert(Not Debugger.IsAttached, "this should not happen :)")
                                Case isr.Tsp.TspExecutionState.IdleReady
                                    ' Ready prompt. TSP received script successfully; ready for next command.
                                    Exit Do
                                Case Else
                                    ' do nothing
                            End Select

                            If (Not Me._visaIO.WriteLine(chunkLine)) OrElse
                               (Me._tspStatus.LastState = isr.Tsp.TspExecutionState.IdleError) Then
                                ' now report the error to the calling module
                                If Me._visaIO.HandleInstrumentErrorIfError(True) Then
                                    deviceErrors = Me._visaIO.DeviceErrors
                                End If
                                Dim messageFormat As String = "{0} failed sending failed sending a syntax line: {5}{1}{5} for script '{2}' from file '{3}'.{5}Device Errors: {4}"
                                Throw New ScriptLoadingException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                               messageFormat, Me._visaIO.ResourceName, chunkLine, Me._name, Me._FilePath, deviceErrors, Environment.NewLine))
                            Else
                                ' increment debug line number
                                debugFile.WriteLine(chunkLine)
                            End If

                            Select Case Me._tspStatus.LastState
                                Case isr.Tsp.TspExecutionState.IdleError
                                    System.Diagnostics.Debug.Assert(Not Debugger.IsAttached, "this should not happen :)")
                                Case isr.Tsp.TspExecutionState.IdleReady
                                    Exit Do
                                Case Else
                                    ' do nothing
                            End Select

                            If lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                                isInCommentBlock = True

                            End If

                        End If

                    Loop

                End Using

            End Using


            ' Tell TSP complete script has been downloaded.
            commandLine = "endscript waitcomplete() print('1') "
            If (Not Me._visaIO.WriteLine(commandLine)) OrElse
               (Me._tspStatus.LastState = isr.Tsp.TspExecutionState.IdleError) Then
                ' now report the error to the calling module
                If Me._visaIO.HandleInstrumentErrorIfError(True) Then
                    deviceErrors = Me._visaIO.DeviceErrors
                End If
                Dim messageFormat As String = "{0} failed sending an 'endscript' for script '{1}' from file '{2}'.{4}Device Errors: {3}."
                Throw New ScriptLoadingException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                               messageFormat, Me._visaIO.ResourceName, Me._name,
                                                               Me._FilePath, deviceErrors, Environment.NewLine))
            End If

            ' wait till we get a reply from the instrument or timeout.

            ' The command above does not seem to work!  It looks like the print does not get executed!
            Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(3000))
            Dim value As String = String.Empty
            Do
                Do
                    Threading.Thread.Sleep(50)
                Loop Until endTime < DateTime.Now Or Me._visaIO.IsMessageAvailable
                If Me._visaIO.IsMessageAvailable Then
                    value = Me._visaIO.ReadLine
                    If Not value.StartsWith("1", StringComparison.OrdinalIgnoreCase) Then
                        Me._tspStatus.ParseState(value)
                    End If
                End If
            Loop Until endTime < DateTime.Now Or value.StartsWith("1", StringComparison.OrdinalIgnoreCase)
            If endTime < DateTime.Now Then
                'Throw New isr.Tsp.ScriptCallException("Timeout waiting operation completion loading the script '" & Me._name & "'")
            End If

            ' add a wait to ensure the system returns the last status.
            Threading.Thread.Sleep(100)

            ' flush the buffer until empty to completely reading the status.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            ' get the script state if showing prompts
            Select Case Me._tspStatus.LastState
                Case isr.Tsp.TspExecutionState.IdleError
                    System.Diagnostics.Debug.Assert(Not Debugger.IsAttached, "this should not happen :)")
                Case Else
                    ' do nothing
            End Select

            Return True

        Catch

            ' remove any remaining values.
            Me._visaIO.DiscardUnreadData(10, 100, True)

            Throw

        Finally

            ' restore state of prompts and errors.
            Me._tspStatus.RestoreStatus()

        End Try

    End Function

    ''' <summary>
    ''' Opens a script file as a text reader.
    ''' </summary>
    ''' <returns>A reference to an open
    ''' <see cref="System.IO.TextReader">Text Stream</see>.
    ''' </returns>
    Public Shared Function OpenScriptFile(ByVal filePath As String) As System.IO.StreamReader

        ' Check name
        If String.IsNullOrWhiteSpace(filePath) OrElse Not System.IO.File.Exists(filePath) Then
            Return Nothing
            Exit Function
        End If
        Return New System.IO.StreamReader(filePath)

    End Function

    ''' <summary>
    ''' Opens a script file as a text reader.
    ''' </summary>
    ''' <returns>A reference to an open
    ''' <see cref="System.IO.TextReader">Text Stream</see>.
    ''' </returns>
    Public Function OpenScriptFile() As System.IO.StreamReader

        ' Check name
        If String.IsNullOrWhiteSpace(Me._name) OrElse String.IsNullOrWhiteSpace(Me._FilePath) OrElse Not System.IO.File.Exists(Me.FilePath) Then
            Return Nothing
            Exit Function
        End If

        Return New System.IO.StreamReader(Me.FilePath)

    End Function

#End Region

#Region " PARSE SCRIPT "

    ''' <summary>
    ''' Parses the script.
    ''' </summary>
    ''' <param name="source">specifies the source code for the script.</param>
    ''' <param name="retainOutline">Specifies if the code outline is retained or trimmed.</param>
    Public Shared Function ParseScript(ByVal source As String, ByVal retainOutline As Boolean) As String

        If source Is Nothing Then
            Throw New ArgumentNullException("source")
        End If
        Dim sourceLines As String() = source.Split(Environment.NewLine.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
        Dim newSource As New System.Text.StringBuilder
        Dim isInCommentBlock As Boolean = False
        Dim wasInCommentBlock As Boolean
        Dim lineType As isr.Tsp.TspChunkLineContentType
        For Each chunkLine As String In sourceLines

            chunkLine = TspUserScript.TrimTspChuckLine(chunkLine, retainOutline)
            wasInCommentBlock = isInCommentBlock
            lineType = TspUserScript.ParseTspChuckLine(chunkLine, isInCommentBlock)

            If lineType = TspChunkLineContentType.None Then

                ' if no data, nothing to do.

            ElseIf wasInCommentBlock Then

                ' if was in a comment block exit the comment block if
                ' received a end of comment block
                If lineType = isr.Tsp.TspChunkLineContentType.EndCommentBlock Then
                    isInCommentBlock = False
                End If

            ElseIf lineType = TspChunkLineContentType.StartCommentBlock Then

                isInCommentBlock = True

            ElseIf lineType = TspChunkLineContentType.Comment Then

                ' if comment line do nothing

            ElseIf lineType = TspChunkLineContentType.Syntax OrElse lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                If lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                    chunkLine = chunkLine.Substring(0, chunkLine.IndexOf(TspSyntax.StartCommentChunk, StringComparison.OrdinalIgnoreCase))

                End If

                If Not String.IsNullOrWhiteSpace(chunkLine) Then
                    newSource.AppendLine(chunkLine)
                End If

                If lineType = TspChunkLineContentType.SyntaxStartCommentBlock Then

                    isInCommentBlock = True

                End If

            End If

        Next

        Return newSource.ToString

    End Function

    ''' <summary>
    ''' Parses a TSP chuck line.  This assumes that the line was trimmed.
    ''' </summary>
    ''' <param name="value">Specifies the line.
    ''' </param>
    Public Shared Function ParseTspChuckLine(ByVal value As String, ByVal isInCommentBlock As Boolean) As isr.Tsp.TspChunkLineContentType

        If value Is Nothing Then value = ""

        value = value.Trim.Replace(Convert.ToChar(9, Globalization.CultureInfo.CurrentCulture), " ").Trim

        If String.IsNullOrWhiteSpace(value) Then

            Return isr.Tsp.TspChunkLineContentType.None

            ' check if start of comment block
        ElseIf value.StartsWith(TspSyntax.StartCommentChunk, StringComparison.OrdinalIgnoreCase) Then

            Return isr.Tsp.TspChunkLineContentType.StartCommentBlock

        ElseIf value.Contains(TspSyntax.StartCommentChunk) Then

            Return isr.Tsp.TspChunkLineContentType.SyntaxStartCommentBlock

            ' check if in a comment block
        ElseIf isInCommentBlock Then

            ' check if end of comment block
            If value.Contains(TspSyntax.EndCommentChunk) Then
                Return isr.Tsp.TspChunkLineContentType.EndCommentBlock
            End If

            ' skip comment lines.
        ElseIf value.StartsWith(TspSyntax.CommentChunk, StringComparison.OrdinalIgnoreCase) Then

            Return isr.Tsp.TspChunkLineContentType.Comment

        Else

            Return isr.Tsp.TspChunkLineContentType.Syntax

        End If

    End Function

    ''' <summary>
    ''' Trim the TSP chuck line.
    ''' </summary>
    ''' <param name="value">Specifies the line.
    ''' </param>
    ''' <param name="retainOutline">Specifies if the code outline is retained or trimmed.</param>
    Public Shared Function TrimTspChuckLine(ByVal value As String, ByVal retainOutline As Boolean) As String

        If String.IsNullOrWhiteSpace(value) Then

            Return String.Empty

        Else

            If retainOutline Then

                ' remove leading and lagging spaces and horizontal tabs
                Return value.Replace(Convert.ToChar(9, Globalization.CultureInfo.CurrentCulture), "  ").TrimEnd

            Else

                ' remove leading and lagging spaces and horizontal tabs
                Return value.Replace(Convert.ToChar(9, Globalization.CultureInfo.CurrentCulture), " ").Trim

            End If

            If String.IsNullOrWhiteSpace(value) Then

                Return String.Empty

                ' check if start of comment block
            ElseIf value.Contains(TspSyntax.StartCommentChunk) Then

                ' return the start of comment chunk
                Return TspSyntax.StartCommentChunk

                ' check if end of comment block
            ElseIf value.Contains(TspSyntax.EndCommentChunk) Then

                ' return the end of comment chunk
                Return TspSyntax.EndCommentChunk

                ' check if a comment line
            ElseIf value.Substring(0, 2) = TspSyntax.CommentChunk Then

                ' return the comment chunk
                Return TspSyntax.CommentChunk

            Else

                ' remove a trailing comment.  This cannot be easily done
                ' become of commands such as
                ' print( '----' )
                ' print( " --- -" )
                If value.Contains("""") Then
                    Return value
                ElseIf value.Contains("'") Then
                    Return value
                Else
                    ' if no text in the line, we can safely remove a trailing comment.
                    If value.Contains(TspSyntax.CommentChunk) Then
                        value = value.Substring(0, value.IndexOf(TspSyntax.CommentChunk, StringComparison.OrdinalIgnoreCase))
                    End If
                    Return value
                End If

            End If

        End If

    End Function

#End Region

#Region " READ AND WRITE "

    ''' <summary>
    ''' Reads the script from the script file.
    ''' </summary>
    ''' <param name="filePath">Specifies the script file path.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function ReadScript(ByVal filePath As String) As String

        Using tspFile As System.IO.StreamReader = TspUserScript.OpenScriptFile(filePath)
            Return tspFile.ReadToEnd()
        End Using

    End Function

    ''' <summary>
    ''' Write the script to file.
    ''' </summary>
    ''' <param name="filePath">Specifies the script file path.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function WriteScript(ByVal source As String, ByVal filePath As String) As Boolean

        If source Is Nothing Then
            Throw New ArgumentNullException("source")
        End If
        Using tspFile As System.IO.StreamWriter = New System.IO.StreamWriter(filePath)
            tspFile.Write(source)
            Return True
        End Using

    End Function

#End Region

End Class