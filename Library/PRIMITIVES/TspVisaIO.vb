''' <summary>
''' Defines the I/O driver for accessing the master node of a TSP Linked
''' system.
''' </summary>
''' <remarks>
''' This I/O layer is designed to later inherit the VISA instrument.
''' For this reason, explicit Rst and Cls commands were defined in both
''' the with idea that later on the TSP commands will overload the VISA
''' commands.
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">
''' Convert to .NET Framework.
''' </history>
''' <history date="03/09/2007" by="David" revision="1.15.2624.x">
''' Created
''' </history>
Public Class TspVisaIO
    Inherits isr.Visa.Scpi.Instrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="instanceName">Specifies the name of the instance.</param>
    Public Sub New(ByVal instanceName As String)

        MyBase.New(instanceName)

        Me._statusLatency = 1
        Me._statusRepeatCount = 3

        ' Creating TSP status manager..."
        '  4050 in TSP System .... My. MyLibrary  . TspStatus = New isr.Tsp.TspStatus(Me)
        Me._TspStatus = New isr.Tsp.TspStatus(Me)

        MyBase.ErrorAvailableBits = Visa.Scpi.ServiceRequests.ErrorAvailable
        MyBase.MessageAvailableBits = Visa.Scpi.ServiceRequests.MessageAvailable
        MyBase.IsCheckBufferOnReceive = True
        MyBase.IsCheckBufferOnSend = True
        MyBase.IsCheckOpcAfterSend = False
        MyBase.IsDelegateErrorHandling = False

        ' set default commands and query commands.

        MyBase.ErrorQueueQueryCommand = TspSyntax.ErrorQueueQueryCommand(TspSyntax.GlobalNode)

        MyBase.ClearErrorQueueCommand = TspSyntax.ClearErrorQueueCommand(TspSyntax.GlobalNode)

        ' include the error clear command for compatibility with 2400 instruments.
        MyBase.ClearExecutionStateCommand = TspSyntax.ClearExecutionStateCommand(TspSyntax.GlobalNode)

        MyBase.StandardEventEnableCommand = TspSyntax.StandardEventEnableCommand(TspSyntax.GlobalNode)

        MyBase.StandardEventEnableQueryCommand = TspSyntax.StandardEventEnableQueryCommand(TspSyntax.GlobalNode)

        MyBase.StandardEventStatusQueryCommand = TspSyntax.StandardEventStatusQueryCommand(TspSyntax.GlobalNode)

        ' *IDN? *WAI
        MyBase.IdentifyQueryCommand = isr.Visa.Ieee4882.Syntax.IdentifyQueryCommand & " " & isr.Visa.Ieee4882.Syntax.WaitCommand
        ' MyBase.IdentifyQueryCommand = TspSyntax.IdentifyQueryCommand(TspSyntax.LocalNode)

        MyBase.OperationCompletedCommand = isr.Visa.Ieee4882.Syntax.OperationCompletedCommand ' "*OPC"

        MyBase.OperationCompletedQueryCommand = isr.Visa.Ieee4882.Syntax.OperationCompletedQueryCommand ' "*OPC?"

        ' Use reset() to reset all devices on the TSP link.
        MyBase.ResetKnownStateCommand = TspSyntax.ResetKnownStateCommand(TspSyntax.GlobalNode)

        MyBase.ServiceRequestEnableCommand = TspSyntax.ServiceRequestEnableCommand(TspSyntax.GlobalNode)

        MyBase.ServiceRequestEnableQueryCommand = TspSyntax.ServiceRequestEnableQueryCommand(TspSyntax.LocalNode)

        MyBase.ServiceRequestStatusQueryCommand = TspSyntax.ServiceRequestStatusQueryCommand(TspSyntax.LocalNode)

        MyBase.WaitCommand = isr.Visa.Ieee4882.Syntax.WaitCommand ' "*WAI"

        ' Set the ID string of this object
        Me.UniqueId = Me._uniqueId

        ' TSP instrument Ready to Connect.

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                    If Me.IsConnected Then
                        Me.Disconnect()
                    End If

                    If Me._TspStatus IsNot Nothing Then
                        Me._TspStatus.Dispose()
                        Me._TspStatus = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' invoke the dispose method on the base class.
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " INSTANCE and STATUS MESSAGE "

    ''' <summary>
    ''' Overrides "M:MyBase.ToString" returning the instance name if not empty.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to return the instance name. If instance name is not set, 
    ''' returns the base class ToString value.
    ''' </remarks>
    Public Overrides Function ToString() As String
        If String.IsNullOrWhiteSpace(Me._instanceName) Then
            Return MyBase.ToString
        Else
            Return MyBase.ToString & "." & Me._instanceName
        End If
    End Function

    Private _instanceName As String = String.Empty
    ''' <summary>Gets or sets the name given to an instance of this class.</summary>
    ''' <value><c>InstanceName</c> is a String property.</value>
    Public Property InstanceName() As String
        Get
            If String.IsNullOrWhiteSpace(Me._instanceName) Then
                Return MyBase.ToString
            Else
                Return Me._instanceName
            End If
        End Get
        Set(ByVal value As String)
            Me._instanceName = value
        End Set
    End Property

#End Region

#Region " ICOLLECTIBLE "

    ''' <summary>Gets or sets the unique object ID.</summary>
    Private _uniqueId As Integer

    ''' <summary>Gets or sets the value of the object ID.
    ''' This must be a unique property that can serve to
    ''' index the object in a collection of objects.
    ''' </summary>
    Public Property UniqueId() As Integer
        Get
            Return Me._uniqueId
        End Get
        Set(ByVal Value As Integer)

            ' Set the object ID
            Me._uniqueId = Value

            ' Set the ID string of this object
            Me._uniqueKey = TspSystem.BuildUniqueKey(Me._uniqueId)

        End Set
    End Property

    ''' <summary>Gets or sets the unique object key.</summary>
    Private _uniqueKey As String

    ''' <summary>Gets or sets the unique object key.</summary>
    Public ReadOnly Property UniqueKey() As String
        Get
            Return Me._uniqueKey
        End Get
    End Property

#End Region

#Region " ICONNECTABLE "

    Private _TspStatus As isr.Tsp.TspStatus
    ''' <summary>Gets reference to the
    ''' <see cref="isr.Tsp.TspStatus">TSP Status Manager</see>.</summary>
    Public ReadOnly Property TspStatus() As isr.Tsp.TspStatus
        Get
            Return Me._TspStatus
        End Get
    End Property

    ''' <summary>Raises an event to alert on change of connection.
    ''' </summary>
    Protected Overrides Sub OnConnectionChanged(ByVal e As System.EventArgs)

        If Me.IsConnected Then
            Me.OnConnectedResetClear()
        Else
            ' set the state to closed
            Me.TspStatus.LastState = isr.Tsp.TspExecutionState.Closed
        End If
        MyBase.OnConnectionChanged(e)

    End Sub

#End Region

#Region " IEEE 488.2 COMMANDS "

    ''' <summary>Issues a hardware trigger.
    ''' </summary>
    Public Overrides Function AssertTrigger() As Boolean

        Me.TspStatus.LastState = isr.Tsp.TspExecutionState.IdleReady
        Return MyBase.AssertTrigger()

    End Function

    ''' <summary>Issues a selective-device-clear.
    ''' </summary>
    Public Overrides Function ClearActiveState() As Boolean

        Me.TspStatus.LastState = isr.Tsp.TspExecutionState.IdleReady
        Return MyBase.ClearActiveState

    End Function

    ''' <summary>
    ''' Clears all status data structure registers (enable, event, NTR and PTR)
    ''' to their power up states.  The naked TSP clear does not clear the
    ''' error queue.  THis command adds clear for error queue.
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean

        ' clear the base instrument.
        ClearExecutionState = MyBase.ClearExecutionState
        Me.TspStatus.ReadAndParseState()

        ' Set all cached values that get reset by Cls
        Me.TspStatus.ClearStatus()

    End Function

    ''' <summary>Reads the device status data byte and returns True
    ''' if the message available bits were set.  
    ''' Delays looking for the message status by the status latency to make sure
    ''' the instrument had enough time to process the previous command.
    ''' </summary>
    ''' <returns>True if the device has data in the queue
    ''' </returns>
    Public Overrides Function IsMessageAvailable() As Boolean

        ' wait for the instrument to process the previous command before checking the message status.
        If Me._statusLatency > 0 Then
            Threading.Thread.Sleep(Me._statusLatency)
            For i As Integer = 1 To Me.StatusRepeatCount
                MyBase.IsMessageAvailable()
            Next
        End If

        ' check if we have data in the queue
        Return MyBase.IsMessageAvailable

    End Function

    ''' <summary>Resets and clears the device.
    ''' Issues selective-device-clear (SDC),
    ''' reset to know state (Rst), Clear ExecutionState (Cls, and clear error
    ''' queue.
    ''' </summary>
    Public Overrides Function ResetAndClear() As Boolean

        Dim synopsis As String = "Reset and Clear"
        Dim affirmative As Boolean = MyBase.ResetAndClear

        ' enable service request on all events
        affirmative = affirmative AndAlso MyBase.EnableServiceRequestLong(Visa.Scpi.StandardEvents.All, Visa.Scpi.ServiceRequests.All)
        Me.TspStatus.ReadAndParseState()

        ' allow operations to complete
        affirmative = affirmative AndAlso MyBase.OperationCompleted(Visa.ResourceAccessLevels.Device)
        Me.TspStatus.ReadAndParseState()

        If affirmative Then
            MyBase.OnMessageAvailable(TraceEventType.Information, synopsis, "{0} completed reset and clear.", MyBase.ResourceName)
        Else
            MyBase.OnMessageAvailable(TraceEventType.Warning, synopsis, "{0} failed reset and clear.{1}{2}",
                                             MyBase.ResourceName, Environment.NewLine,
                                               isr.Core.StackTraceParser.UserCallStack(4, 8))
        End If

        Return affirmative

    End Function

    ''' <summary>Returns the device to the reset defaults.
    ''' </summary>
    Public Overrides Function ResetKnownState() As Boolean

        ' RST issues the reset() message, which returns all the nodes
        ' on the TSP LInk system to the original factory defaults:
        ResetKnownState = MyBase.ResetKnownState

        ' sets other systems
        Me.TspStatus.ResetKnownState()

    End Function

    Private _statusLatency As Integer
    ''' <summary>
    ''' Gets or sets the time to delay checking the instrument status.  This allows the instrument 
    ''' time to process a write command, return the status prompt and update the message and error available bits.
    ''' </summary>
    Public Property StatusLatency() As Integer
        Get
            Return Me._statusLatency
        End Get
        Set(ByVal value As Integer)
            Me._statusLatency = value
        End Set
    End Property

    Private _statusRepeatCount As Integer
    ''' <summary>
    ''' Gets or sets the number of time to check before returning status.
    ''' It seems that the instruments has some delay resetting the status that get cleared only after
    ''' repeated checks.
    ''' </summary>
    Public Property StatusRepeatCount() As Integer
        Get
            Return Me._statusRepeatCount
        End Get
        Set(ByVal value As Integer)
            Me._statusRepeatCount = value
        End Set
    End Property

#End Region

#Region " DISCONNECT "

    ''' <summary>Closes the device.
    ''' Performs the necessary termination
    ''' functions, which will cleanup and disconnect the
    ''' interface connection.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function Disconnect() As Boolean

        ' prepare to disconnect
        prepareDisconnect()

        ' Close the device
        Return MyBase.Disconnect()

    End Function

    ''' <summary>Prepares the device for disconnecting.
    ''' Turns off all prompts before disconnecting
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub prepareDisconnect()

        Dim tspCommand As New System.Text.StringBuilder

        ' turn off prompts
        tspCommand.AppendLine("_G.localnode.prompts = 0")

        ' turn off error messaging
        tspCommand.AppendLine("_G.localnode.showerrors = 0")

        ' send termination command
        Me.WriteLine(tspCommand.ToString)

    End Sub

#End Region

#Region " ERROR HANDLERS "

    ''' <summary>
    ''' Handles instrument error.  For compatibility
    ''' Clears the error queue and status by reading the error queue.
    ''' </summary>
    ''' <param name="flushReadFirst">Flushes the read buffer before processing the error.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overrides Function HandleInstrumentError(ByVal flushReadFirst As Boolean) As Boolean

        ' store the status
        Me.TspStatus.StoreStatus()

        Try

            Return MyBase.HandleInstrumentError(flushReadFirst)

        Catch

            Try
                ' remove any remaining values.
                Me.FlushRead(10, 100, True)
            Catch
            End Try
            Throw

        Finally

            ' restore the status
            Me.TspStatus.RestoreStatus()

        End Try

    End Function

#End Region

#Region " READ AND WRITE "

    ''' <summary>
    ''' Flushes the read buffers.
    ''' </summary>
    ''' <param name="pollDelay">Specifies time delay between checks of the status register</param>
    ''' <param name="reportUnreadData">Indicates if unread data must be reported using the 
    ''' message available construct.</param>
    Public Overrides Function FlushRead(ByVal pollDelay As Integer, ByVal timeout As Integer, ByVal reportUnreadData As Boolean) As Boolean

        ' flush the buffer until empty to completely reading the status.
        Do While Me.IsMessageAvailable
            If Me.TspStatus.ShowErrors.HasValue AndAlso Me.TspStatus.ShowErrors.Value Then
                Me.TspStatus.ReadAndParseState()
            Else
                Me.ReadLine()
            End If
        Loop
        ' flush buffers
        ' MyBase.Session.Reader.DiscardUnreadData()
        Return MyBase.IsLastVisaOperationSuccess
    End Function

    ''' <summary>
    ''' Sends a command and reads back the status.
    ''' </summary>
    ''' <param name="format">Specifies the format statement</param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    Public Overrides Function WriteLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If MyBase.WriteLine(format, args) Then
            Me.TspStatus.ReadAndParseState()
        End If
        Return MyBase.IsLastVisaOperationSuccess
    End Function

    ''' <summary>
    ''' Sends a command and reads back the status.
    ''' </summary>
    ''' <param name="value">Specifies the command string.
    ''' </param>
    Public Overrides Function WriteLine(ByVal value As String) As Boolean

        ' Send the command
        If MyBase.WriteLine(value) Then
            Me.TspStatus.ReadAndParseState()
        End If
        Return MyBase.IsLastVisaOperationSuccess
    End Function

#End Region

#Region " TSP COMMANDS "

    ''' <summary>
    ''' Sends a collect garbage command.
    ''' </summary>
    Public Function CollectGarbage() As Boolean

        ' send the message
        Return Me.WriteLine(TspSyntax.CollectGarbageCommand)

    End Function

    ''' <summary>
    ''' Checks the series of values and return true if any one of them is nil.
    ''' </summary>
    ''' <param name="values">Specifies a list of nil objects to check.</param>
    Public Function IsNil(ByVal ParamArray values() As String) As Boolean
        If values Is Nothing OrElse values.Length = 0 Then
            Throw New ArgumentNullException("values")
        Else
            For Each value As String In values
                If Not String.IsNullOrWhiteSpace(value) Then
                    If Me.IsStatementTrue("{0} == nil", value) Then
                        Return True
                    End If
                End If
            Next
        End If
        Return False
    End Function

    ''' <summary>
    ''' Returns true if the validation command returns true.
    ''' </summary>
    ''' <param name="format">Specifies the format statement for constructing the assertion.</param>
    ''' <param name="args">Specifies the arguments to use when formatting the assertion.</param>
    Public Function IsStatementTrue(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If String.IsNullOrWhiteSpace(format) Then
            Throw New ArgumentNullException("format")
        End If
        Dim outcome As Boolean? = MyBase.QueryBoolean("_G.print({0})", String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        Return outcome.HasValue AndAlso outcome.Value
    End Function

    ''' <summary>
    ''' Sends a print command to the instrument using the specified format and argument.
    ''' The format conforms to the 'C' query command and returns the Boolean outcome.
    ''' </summary>
    ''' <param name="format">Specifies the format statement</param>
    ''' <param name="args">Specifies the arguments to use when formatting the command.</param>
    ''' <remarks>
    ''' The format string follows the same rules as the printf family of standard C functions. 
    ''' The only differences are that the options or modifiers *, l, L, n, p, and h are not 
    ''' supported and that there is an extra option, q. The q option formats a string in a 
    ''' form suitable to be safely read back by the Lua interpreter: the string is written 
    ''' between double quotes, and all double quotes, newlines, embedded zeros, and backslashes 
    ''' in the string are correctly escaped when written. For instance, the 
    ''' call string.format('%q', 'a string with ''quotes'' and [BS]n new line') will produce the string: 
    ''' a string with [BS]''quotes[BS]'' and [BS]new line
    ''' The options c, d, E, e, f, g, G, i, o, u, X, and x all expect a number as argument, 
    ''' whereas q and s expect a string. This function does not accept string values containing embedded zeros. 
    ''' </remarks>
    Public Function QueryFormat(ByVal format As String, ByVal ParamArray args() As String) As String
        If MyBase.WriteQueryLine("_G.print( string.format('{0}',{1}) )", format, isr.Tsp.TspScript.Parameterize(args)) Then
            Return MyBase.ReadLineTrimEnd()
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Sends a print command to the instrument and returns the reply.</summary>
    ''' <param name="value">Specifies the value to print.</param>
    Public Function PrintAndQueryValue(ByVal value As String) As String

        If String.IsNullOrWhiteSpace(value) Then

            PrintAndQueryValue = String.Empty

        Else

            PrintAndQueryValue = MyBase.QueryTrimEnd(TspSyntax.PrintCommand(value))
            Me.TspStatus.ReadAndParseState()

        End If

    End Function

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>Raises the command received event.
    ''' </summary>
    Protected Overrides Sub OnCommandReceived()

        ' save the last command that was received
        Dim lastCommandReceived As String = MyBase.Session.ReceiveBuffer

        ' parse the command to get TSP status
        Me.TspStatus.ParseState(lastCommandReceived)

        MyBase.OnCommandReceived()

    End Sub

    ''' <summary>Raises the command sent event.
    ''' </summary>
    Protected Overrides Sub OnCommandSent()

        ' set the TSP status
        Me.TspStatus.LastState = isr.Tsp.TspExecutionState.Processing

        MyBase.OnCommandSent()

    End Sub

#End Region

End Class
