Imports isr.Core
Imports isr.Core.AssemblyInfoExtensions
''' <summary>
''' Switchboard panel.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/07/2007" by="David" revision="2.0.2597.x">
''' Created
''' </history>
Friend Class TestPanel
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' set large font for messages.
        'Me._messagesBox.Font = New Font(Me._messagesBox.Font.FontFamily, 10, FontStyle.Regular)
        'Me._messagesBox.TabCaption = "MESSA&GES"

        ' instantiate the timing timer
        Me._timingTimer = System.Diagnostics.Stopwatch.StartNew

        ' instantiate the instrument
        Me._tspSystem = New isr.Tsp.TspSystem

        ' update state
        Me._tspStatusLabel.Text = Me._tspSystem.TspStatus.StateCaption

        ' hide timer interval information - this could be activated if we use an
        ' service request driven rather than timer driver monitoring.
        Me._executionTimeTextBox.Visible = False
        Me._executionTimeTextBoxLabel.Visible = False

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shadow parameter.
    ''' </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> 
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
    Private Sub onDisposeManagedResources()

        ' dispose of the timer.
        If Me._refreshTimer IsNot Nothing Then
            Me._refreshTimer.Close()
            Me._refreshTimer.Dispose()
            Me._refreshTimer = Nothing
        End If

        ' dispose of the instrument
        If Me._tspSystem IsNot Nothing Then
            If Me._tspSystem.IsConnected Then
                Me._tspSystem.Disconnect()
            End If
            Me._tspSystem.Dispose()
            Me._tspSystem = Nothing
        End If

        Me._timingTimer = Nothing

    End Sub

#End Region

#Region " TYPES "

    ''' <summary>Gets or sets Tab index values.</summary>
    Private Enum MainTabsIndex
        ConsoleTabIndex = 0
        TspScripts = 1
        TspFunctions = 2
        TbdTabIndex = 3
        MessagesTabIndex = 4
    End Enum

#End Region

#Region " PROPERTIES "

    Private _isOpen As Boolean
    ''' <summary>
    ''' Gets or sets the Open condition enabling or disable panels.
    ''' </summary>
    Private ReadOnly Property isOpen() As Boolean
        Get
            Return Me._isOpen
        End Get
    End Property


    ''' <summary>
    ''' Opens or closes.
    ''' </summary>
    ''' <param name="value">if set to <c>True</c> open; otherwise, close.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")>
    Private Sub openClose(ByVal value As Boolean)

        Dim wasEnabled As Boolean = Me._connectToggle.Enabled
        If value <> isOpen Then
            If value Then

                ' get the instrument ID
                If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.IsConnected Then

                    ' flush the input buffer in case the instrument has some left overs.
                    If Me._tspSystem.VisaIO.IsMessageAvailable Then
                        Me.receive(True)
                    End If

                    ' set the error and prompt check boxes.
                    Me._showErrorsCheckBox.Checked = Me._tspSystem.TspStatus.ShowErrorsGetter(Visa.ResourceAccessLevels.Device)
                    Me._showErrorsCheckBox.Refresh()
                    Me._showPromptsCheckBox.Checked = Me._tspSystem.TspStatus.ShowPromptsGetter(Visa.ResourceAccessLevels.Device)
                    Me._showPromptsCheckBox.Refresh()

                    Me._messagesBox.PrependMessage(New isr.Core.ExtendedMessage(TraceEventType.Information, TraceEventType.Error, Me._tspSystem.VisaIO.Identity))
                    Me._tspSystem.TspStatus.ReadAndParseState()

                    ' list any user scripts.
                    Me._statusPanel.Text = "LISTING USER SCRIPTS..."
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                    listUserScripts()

                    Me._statusPanel.Text = "CONNECTED"
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                    If Me._refreshTimer IsNot Nothing Then
                        ' re-able the timer
                        Me._refreshTimer.Interval = 500
                        Me._refreshTimer.Start()
                    End If

                End If

            Else

                If Me._refreshTimer IsNot Nothing Then
                    ' stop the timer.
                    Me._refreshTimer.Stop()
                End If

            End If

        End If

        ' update the connect toggle state to reflect the connection state.
        If value <> Me._connectToggle.Checked Then
            Me._connectToggle.Enabled = False
            Me._connectToggle.Checked = value
            Me._connectToggle.Refresh()
            Me._connectToggle.Enabled = wasEnabled
        End If
        If value Then
            Me._connectToggle.Text = "DIS&CONNECT"
        Else
            Me._connectToggle.Text = "&CONNECT"
        End If
        Me._connectToggle.Invalidate()

        Me._isOpen = value
        Me._inputTextBox.Enabled = value
        Me._outputTextBox.Enabled = value
        Me._scriptsPanel1.Enabled = value
        Me._scriptsPanel2.Enabled = value
        Me._scriptsPanel3.Enabled = value
        Me._scriptsPanel4.Enabled = value
        Me._functionsPanel1.Enabled = value
        Me._loadFunctionButton.Enabled = value
        Me._instrumentPanel1.Enabled = value
        Me._instrumentPanel2.Enabled = value
        Me._instrumentPanel3.Enabled = value

    End Sub


    ''' <summary>Gets or sets the currently built query command string.</summary>
    Dim queryCommand As String = String.Empty

    ''' <summary>Gets or sets reference to the timing stop watch.</summary>
    Dim _timingTimer As System.Diagnostics.Stopwatch

    ''' <summary>Gets or sets reference to the TSP instrument.</summary>
    Private WithEvents _tspSystem As isr.Tsp.TspSystem

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Locates VISA resources.
    ''' </summary>
    Private Sub locateVisaResources()

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._statusPanel.Text = "LOCATING VISA RESOURCES. PLEASE WAIT"
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            Me._clearInterfaceButton.Enabled = False
            Me._connectToggle.Enabled = False

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

            If isr.Visa.GpibInterface.HasInterfaces Then

                ' allow some events to occur for refreshing the display.
                Application.DoEvents()

                If Me._tspSystem.VisaIO.HasResources Then

                    ' allow some events to occur for refreshing the display.
                    Application.DoEvents()

                    ' resources must be present for enabling the interface clear otherwise it times out.
                    Me._clearInterfaceButton.Enabled = True
                    If Me.ValidatedGpibAddress().HasValue Then
                        Me._statusPanel.Text = "READY"
                    Else
                        Me._statusPanel.Text = "GPIB INSTRUMENT IS NOT CONNECTED AT " & Me._gpibAddressTextBox.Text
                    End If
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                Else
                    If isr.Visa.My.MyLibrary.FailedFindLocalResources.Value Then
                        Me._statusPanel.Text = "FAILED LOCATING INSTRUMENT - TRY CONNECTING ANYHOW"
                    ElseIf Not isr.Visa.My.MyLibrary.LocatedLocalResources Then
                        Me._statusPanel.Text = "NO INSTRUMENT(S) ARE CONNECTED"
                    End If
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                End If
            Else
                If isr.Visa.My.MyLibrary.FailedFindLocalResources.Value Then
                    Me._statusPanel.Text = "FAILED LOCATING RESOURCES - TRY CONNECTING ANYHOW"
                ElseIf Not isr.Visa.My.MyLibrary.LocatedLocalResources Then
                    Me._statusPanel.Text = "GPIB INTERFACE NOT FOUND"
                End If
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            End If

        Catch

            Throw

        Finally

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>Gets the current line of the text box.
    ''' </summary>
    ''' <param name="editTextBox">Specifies reference to a text box.</param>
    Private Shared Function getLine(ByVal editTextBox As System.Windows.Forms.TextBox) As String

        Dim commandText As String
        Dim commandStart As Integer
        Dim commandEnd As Integer
        Dim commandLength As Integer

        ' save the text
        commandText = editTextBox.Text

        ' lookup the command start
        commandStart = editTextBox.SelectionStart

        If commandStart > 0 Then
            Do Until (commandStart = 1) Or (Asc(Mid(commandText, commandStart, 1)) = 10)
                commandStart = commandStart - 1
            Loop
            If commandStart > 1 Then
                commandStart = commandStart + 1
            End If
            ' check if operator is entering returns
            If commandStart < commandText.Length Then
                commandLength = commandText.Length
                commandEnd = editTextBox.SelectionStart
                Do Until (commandEnd = commandLength) Or (Asc(Mid(commandText, commandEnd, 1)) = 13)
                    commandEnd = commandEnd + 1
                Loop
                If commandEnd < commandLength Then
                    commandEnd = commandEnd - 1
                End If
                commandLength = commandEnd - commandStart + 1
                getLine = Mid(commandText, commandStart, commandLength)
            Else
                getLine = String.Empty ' CStr(Nothing)
            End If
        Else
            getLine = String.Empty ' CStr(Nothing)
        End If

    End Function

    ''' <summary>List the user scripts.
    ''' </summary>
    Private Function listUserScripts() As Boolean

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._statusPanel.Text = "FLUSHING THE READ BUFFER..."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            If Me._tspSystem.VisaIO.FlushRead(10, 100, True) Then

                Me._statusPanel.Text = "FETCHING SCRIPT NAMES..."
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                If Me._tspSystem.TspScript.FetchUserScriptNames Then
                    Me._statusPanel.Text = "LISTING SCRIPT NAMES..."
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                    Me._userScriptsList.DataSource = Nothing
                    Me._userScriptsList.Items.Clear()
                    Me._userScriptsList.DataSource = Me._tspSystem.TspScript.UserScriptNames
                    If Me._userScriptsList.Items.Count > 0 Then
                        Me._userScriptsList.SelectedIndex = 0
                    End If
                Else
                    Me._statusPanel.Text = "NO SCRIPTS."
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                    Me._userScriptsList.DataSource = Nothing
                    Me._userScriptsList.Items.Clear()
                End If
            Else
                Throw New isr.Tsp.BaseException("Failed discarding unread data")
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred listing scripts"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)
            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Function

    Private receiveLock As New Object
    ''' <summary>Receive a message from the instrument.
    ''' </summary>
    ''' <param name="updateConsole">True to update the instrument output text box.</param>
    Private Sub receive(ByVal updateConsole As Boolean)

        Dim lastAction As String = String.Empty

        SyncLock (receiveLock)

            Try

                ' read a message if we have messages.
                Dim receiveBuffer As String
                If updateConsole AndAlso Me._tspSystem.VisaIO.IsMessageAvailable Then

                    ' Turn on the form hourglass
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    lastAction = "READING"
                    Me._statusPanel.Text = lastAction
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                    receiveBuffer = Me._tspSystem.VisaIO.ReadLine()

                    lastAction = "DONE READING"
                    Me._statusPanel.Text = lastAction
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                    Me._outputTextBox.SelectionStart = Me._outputTextBox.Text.Length
                    Me._outputTextBox.SelectionLength = 0
                    Me._outputTextBox.SelectedText = receiveBuffer & vbCrLf
                    Me._outputTextBox.SelectionStart = Me._outputTextBox.Text.Length

                    ' stop the timer and get execution time in ms.
                    Me._timingTimer.Stop()
                    Me._executionTimeTextBox.Text = Me._timingTimer.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)

                End If

            Catch ex As Exception

                Dim synopsis As String = "Exception occurred receiving"

                ' display the exception
                Me._messagesBox.PrependException(ex)
                Me._messagesBox.PrependException(synopsis)
                Me._statusPanel.Text = synopsis

                TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

            Finally

                ' Turn off the form hourglass
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End SyncLock

    End Sub

    ''' <summary>Sends a message to the instrument.
    ''' </summary>
    ''' <param name="sendBuffer">Specifies the message to send.</param>
    ''' <param name="updateInputConsole">True to update the instrument input text box.</param>
    Private Sub send(ByVal sendBuffer As String, ByVal updateInputConsole As Boolean)

        Dim lastAction As String = String.Empty

        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            If Not String.IsNullOrWhiteSpace(sendBuffer) Then

                lastAction = "SENDING QUERY/COMMAND"
                Me._statusPanel.Text = lastAction
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                ' start the timing timer
                Me._timingTimer = System.Diagnostics.Stopwatch.StartNew

                Dim isQuery As Boolean = sendBuffer.Contains("print")
                If isQuery Then
                    Me._tspSystem.VisaIO.WriteQueryLine(sendBuffer)
                Else
                    Me._tspSystem.VisaIO.WriteLine(sendBuffer)
                End If
                If Me._tspSystem.VisaIO.IsLastVisaOperationSuccess Then
                    lastAction = "QUERY/COMMAND SENT"
                Else
                    lastAction = "FAILED " & lastAction
                End If

                Me._statusPanel.Text = "QUERY/COMMAND SENT"
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                If updateInputConsole Then
                    Me._inputTextBox.SelectionStart = Me._inputTextBox.Text.Length
                    Me._inputTextBox.SelectionLength = 0
                    Me._inputTextBox.SelectedText = sendBuffer & vbCrLf
                    Me._inputTextBox.SelectionStart = Me._inputTextBox.Text.Length
                End If

            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred sending"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private _validatedGpibAddress As Nullable(Of Integer)
    ''' <summary>
    ''' Enables connecting to the GPIB board.
    ''' </summary>
    Private Function ValidatedGpibAddress() As Nullable(Of Integer)
        Dim value As Integer
        If Integer.TryParse(Me._gpibAddressTextBox.Text, System.Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, value) Then
            If Not Me._validatedGpibAddress.HasValue OrElse Me._validatedGpibAddress.Value <> value OrElse Not Me._connectToggle.Enabled Then
                Me._connectToggle.Enabled = isr.Visa.My.MyLibrary.HasResources(isr.Visa.My.MyLibrary.GpibResourceBaseName, 0, value)
            End If
            Me._validatedGpibAddress = value
        Else
            Me._validatedGpibAddress = New Nullable(Of Integer)()
        End If
        Return Me._validatedGpibAddress
    End Function

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

        ' set default cursor.
        Me.Cursor = System.Windows.Forms.Cursors.Default

        Try

            ' 1.12.2005
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._statusPanel.Text = "UNLOADING."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            ' disable the timer
            If Me._refreshTimer IsNot Nothing AndAlso Me._refreshTimer.Stop() Then
                Me._statusPanel.Text = "TIMER STOPPED OK."
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            End If

            ' dispose of the timer.
            If Me._refreshTimer IsNot Nothing Then
                Me._refreshTimer.Dispose()
                Me._refreshTimer = Nothing
            End If

            ' dispose of the instrument
            If Me._tspSystem IsNot Nothing Then
                If Me._tspSystem.IsConnected Then
                    Me._tspSystem.Disconnect()
                End If
                Me._tspSystem.Dispose()
                Me._tspSystem = Nothing
            End If

            Me._statusPanel.Text = "SAVING SETTINGS."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            System.Windows.Forms.Application.DoEvents()

            ' terminate all the singleton objects.
            Me._statusPanel.Text = "CLOSING. PLEASE WAIT."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            ' delay a bit longer
            Dim timer As System.Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            Do Until timer.ElapsedMilliseconds > 200
                Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(10)
            Loop

        Catch ex As Exception

            ExceptionDisplay.ProcessException(ex)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>Occurs when the form is loaded.
    ''' Does all the pre processing before the form controls are rendered.
    ''' </summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._connectToggle.Enabled = False
            Me._refreshVisaButton.Enabled = False
            Me._clearInterfaceButton.Enabled = False

            ' instantiate form objects

            Me._statusPanel.Text = "LOADING..."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            ' display location of datalog.
            Me._messagesBox.PrependMessage(My.Application.Log.DefaultFileLogWriter.CustomLocation)
            Me._messagesBox.PrependMessage("Logging data to:")

            ' set the form caption
            Me.Text = My.Application.Info.ExtendedCaption() & ": TEST PANEL"

            ' set tool tips
            ' initializeUserInterface()

            ' center the form
            ' Me.CenterToScreen()

            ' show the query command tab.
            ' Me._tabControl.SelectedIndex = MainTabsIndex.ConsoleTabIndex

        Catch ex As System.Runtime.InteropServices.COMException

            Dim synopsis As String = "COM EXCEPTION. ERROR CODE = " & ex.ErrorCode.ToString(Globalization.CultureInfo.CurrentCulture)
            Me._statusPanel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Catch ex As Exception

            Dim synopsis As String = "FAILED LOADING"
            Me._statusPanel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._statusPanel.Text = "STARTING..."
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            ' instantiate form objects

            ' set tool tips
            ' initializeUserInterface()

            ' show the query command tab.
            ' Me._tabControl.SelectedIndex = MainTabsIndex.ConsoleTabIndex

            ' disable controls
            Me.openClose(False)

            ' clear the SRQ value.
            Me._srqStatusLabel.Text = "<srq>"

            ' enable connections
            Using worker As New System.ComponentModel.BackgroundWorker
                If Not (MyBase.IsDisposed OrElse worker.IsBusy) Then
                    ' Start the form's Background Worker to display the queued messages.
                    AddHandler worker.RunWorkerCompleted, AddressOf findResourcesHandler
                    worker.RunWorkerAsync()
                End If
            End Using

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

            ' set the refresh timer.
            Me._refreshTimer = New isr.Controls.StateAwareTimer
            Me._refreshTimer.SynchronizingObject = Me
            Me._refreshTimer.Interval = 1000
            Me._refreshTimer.AutoReset = True

            Me._connectToggle.Enabled = True
            Me._refreshVisaButton.Enabled = True
            Me._clearInterfaceButton.Enabled = True

        Catch ex As System.Runtime.InteropServices.COMException

            Dim synopsis As String = "COM EXCEPTION. ERROR CODE = " & ex.ErrorCode.ToString(Globalization.CultureInfo.CurrentCulture)
            Me._statusPanel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Catch ex As Exception

            Dim synopsis As String = "FAILED SHOWING"
            Me._statusPanel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub



#End Region

#Region " REFRESH TIMER "

    Private WithEvents _refreshTimer As isr.Controls.StateAwareTimer

    ''' <summary>
    ''' Serial polls and receives.
    ''' </summary>
      Private Sub _refreshTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _refreshTimer.Tick

        Try

            If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.IsConnected Then

                Me._tspSystem.VisaIO.ReadDeviceStatusByte()
                Me._srqStatusLabel.Text = Me._tspSystem.VisaIO.DeviceStatusByteCaption

                ' check if the console tab is open
                If Me._tabControl.SelectedIndex = MainTabsIndex.ConsoleTabIndex Then

                    Me.receive(True)

                End If

            End If

            Me._tspStatusLabel.Text = Me._tspSystem.TspStatus.StateCaption

        Catch ex As Exception

            ' stop the time on error
            Me._refreshTimer.Stop()

            ' append the error source
            Dim synopsis As String = "Exception occurred receiving"
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)      ' the state-aware timer event handler handles the exception maintaining the timer state.
            Throw

        Finally

        End Try

    End Sub

#End Region

#Region " TESTING TIMER "

    Private WithEvents _testingTimer As isr.Controls.StateAwareTimer

    ''' <summary>
    ''' Serial polls and receives.
    ''' </summary>
      Private Sub _testingTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _testingTimer.Tick

        Try

            Me._srqStatusLabel.Text = DateTime.Now.ToLongTimeString

        Catch ex As Exception

            ' stop the time on error
            Me._testingTimer.Stop()

            ' append the error source
            Dim synopsis As String = "Exception occurred timing"
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)      ' the state-aware timer event handler handles the exception maintaining the timer state.
            Throw

        Finally

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Terminates script execution.
    ''' </summary>
      Private Sub _abortButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _abortButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Terminates script execution when called from a script that is being
            ' executed. This command will not wait for overlapped commands to complete
            ' before terminating script execution. If overlapped commands are required
            ' to finish, use the waitcomplete function prior to calling exit.
            send("exit() ", True)

            ' update the instrument status.
            Me._tspSystem.TspStatus.ReadAndParseState()

            Dim synopsis As String = "ABORTED"
            Me._statusPanel.Text = synopsis
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred clearing device"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Displays the application information
    ''' </summary>
      Private Sub _aboutButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _aboutButton.Click

        ' Display the application information
        Using aboutScreen As New isr.WindowsForms.About
            aboutScreen.Image = Me.Icon
            aboutScreen.Show(System.Reflection.Assembly.GetExecutingAssembly,
              String.Empty, String.Empty, String.Empty, String.Empty)
        End Using

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred clearing device"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Toggle the font on the control to highlight selection of this control.
    ''' </summary>
      Private Sub _button_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Enter, _abortButton.Enter, _aboutButton.Enter, _callFunctionButton.Enter,
        _clearInterfaceButton.Enter, _connectToggle.Enter, _deviceClearButton.Enter, _exitButton.Enter, _groupTriggerButton.Enter,
        _refreshVisaButton.Enter, _removeScriptButton.Enter, _resetLocalNodeButton.Enter, _resetLocalNodeButton.Enter, _refreshUserScriptsListButton.Enter
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Font(thisButton.Font, FontStyle.Bold)
    End Sub

    ''' <summary>
    ''' Toggle the font on the control to highlight leaving this control.
    ''' </summary>
      Private Sub _button_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Leave, _abortButton.Leave, _aboutButton.Leave, _callFunctionButton.Leave,
        _clearInterfaceButton.Leave, _connectToggle.Leave, _deviceClearButton.Leave, _exitButton.Leave, _groupTriggerButton.Leave,
        _refreshVisaButton.Leave, _removeScriptButton.Leave, _resetLocalNodeButton.Leave, _resetLocalNodeButton.Leave, _refreshUserScriptsListButton.Leave
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Font(thisButton.Font, FontStyle.Regular)
    End Sub

    Private Sub _clearInterfaceButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _clearInterfaceButton.Click
        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            ' Me._tspSystem.VisaIO.ClearInterface()

            Dim synopsis As String = "INTERFACE CLEARED"
            Me._statusPanel.Text = synopsis
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred clearing interface"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _callFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _callFunctionButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim functionName As String
            functionName = Me._functionNameTextBox.Text

            Dim functionArgs As String
            functionArgs = Me._functionArgsTextBox.Text

            If Not String.IsNullOrWhiteSpace(functionName) Then
                If Me._tspSystem.TspScript.CallFunction(functionName, functionArgs) Then
                    Dim synopsis As String = "FUNCTION CALLED"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                Else
                    Dim synopsis As String = "FAILED CALLING FUNCTION '" & functionName & "'"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                End If
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception calling function"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Connects or disconnects.
    ''' </summary>
      Private Sub _connectToggle_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _connectToggle.CheckStateChanged

        Dim lastAction As String = String.Empty
        If Not Me._connectToggle.Enabled Then
            Return
        End If

        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.VisaIO IsNot Nothing Then

                If Me._connectToggle.Checked Then
                    Me._srqStatusLabel.Text = ""
                    lastAction = "CONNECTING"
                    Me._statusPanel.Text = lastAction
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                    Dim resourceName As String = isr.Visa.GpibSession.BuildGpibResourceName("0", Me._gpibAddressTextBox.Text)
                    Me._tspSystem.Connect(resourceName)
                Else
                    lastAction = "DISCONNECTING"
                    Me._tspSystem.Disconnect()
                    Me._srqStatusLabel.Text = "<srq>"
                End If

            End If

        Catch ex As Exception

            Me._messagesBox.PrependException(ex)
            Dim synopsis As String = "FAILED " & lastAction
            Me._messagesBox.PrependException(synopsis)
            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' enable the controls.
            Me.openClose(Me._tspSystem.IsConnected)

            ' turn prompts on.
            If Me.isOpen Then
                Me._showErrorsCheckBox.Checked = True
                Me._showPromptsCheckBox.Checked = True
            End If

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _deviceClearButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _deviceClearButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._tspSystem.VisaIO.ClearActiveState()

            Dim synopsis As String = "DEVICE CLEARED"
            Me._statusPanel.Text = synopsis
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred clearing device"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Unloads the form.
    ''' </summary>
    Private Sub _exitButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Click

        Me.Close()

    End Sub

    ''' <summary>
    ''' Enables connecting to the GPIB board.
    ''' </summary>
    Private Sub _gpibAddressTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _gpibAddressTextBox.Validating
        Me._errorProvider.SetError(Me._gpibAddressTextBox, String.Empty)
        e.Cancel = Not Me.ValidatedGpibAddress.HasValue
        If e.Cancel Then
            Me._errorProvider.SetError(Me._gpibAddressTextBox, "GPIB INSTRUMENT NOT FOUND AT " & Me._gpibAddressTextBox.Text)
        End If
    End Sub

    Private Sub _groupTriggerButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _groupTriggerButton.Click,
                                                                                                            _groupTriggerButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._tspSystem.VisaIO.AssertTrigger()

            Dim synopsis As String = "TRIGGER ASSERTED"
            Me._statusPanel.Text = synopsis
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred triggering device"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try
    End Sub

    ''' <summary>Handles key at the command console.
    ''' </summary>
    Private Sub _inputTextBox_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles _inputTextBox.KeyPress

        Dim KeyAscii As Integer = Asc(eventArgs.KeyChar)

        Select Case KeyAscii

            Case System.Windows.Forms.Keys.Return ' enter key

                ' User pressed enter key.  Send the string to the instrument.

                ' Disable the command console while waiting for command to process.
                Me._inputTextBox.Enabled = False

                ' Enable the abort button.
                Me._abortButton.Enabled = False

                ' dh 6.0.02 use command line at cursor
                queryCommand = getLine(Me._inputTextBox)

                ' Send the string to the instrument. Receive is invoked by the timer.
                Me.send(queryCommand, False)

                ' TO_DO: this is a perfect place for setting a service request before read and
                ' letting the service request invoke the next read.

                ' clear the command
                queryCommand = String.Empty

                ' Turn command console back on and display response.
                Me._abortButton.Enabled = False
                Me._inputTextBox.Enabled = True
                Me._inputTextBox.Focus()

            Case System.Windows.Forms.Keys.Back ' backspace key
                If queryCommand.Length < 2 Then
                    queryCommand = String.Empty
                Else
                    queryCommand = queryCommand.Substring(0, queryCommand.Length - 1)
                End If

            Case Else ' normal key
                ' Stash key in buffer.
                queryCommand = queryCommand & Chr(KeyAscii)
        End Select

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' _loads the and run script.
    ''' </summary>
    ''' <param name="scriptName">Name of the script.</param>
    ''' <param name="filePath">The file path.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _loadAndRunScript(ByVal scriptName As String, ByVal filePath As String)

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._tspSystem.TspUserScript.Name = scriptName
            Me._tspSystem.TspUserScript.FilePath = filePath
            If Me._tspSystem.TspUserScript.LoadScriptFile(True, True, Me._retainCodeOutlineToggle.Checked) Then

                Dim synopsis As String = "LOADED SCRIPT"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependException(Me._tspSystem.TspUserScript.FilePath)
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                listUserScripts()

                If Me._tspSystem.TspUserScript.RunScript(3000) Then

                    synopsis = "SCRIPT RUN OK"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                Else

                    synopsis = "FAILED RUNNING SCRIPT"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                End If

            Else

                Dim synopsis As String = "FAILED LOADING"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._tspSystem.TspUserScript.FilePath)
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred loading script"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _loadAndRunButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadAndRunButton.Click

        Me._loadAndRunScript(Me._scriptNameTextBox.Text, Me._tspScriptSelector.FilePath)

    End Sub

    Private Sub _loadFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadFunctionButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim functionCode As String
            functionCode = Me._functionCodeTextBox.Text.Replace(vbCrLf, Space(1))
            If Not String.IsNullOrWhiteSpace(functionCode) Then
                If Me._tspSystem.VisaIO.WriteLine(functionCode) Then
                    Dim synopsis As String = "FUNCTION LOADED"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                Else
                    Dim synopsis As String = "FAILED LOADING FUNCTION"
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                End If
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred loading function"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _loadScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadScriptButton.Click

        If Me._scriptNameTextBox.Text.IncludesIllegalFileCharacters() Then
            Return
        End If
        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            Me._tspSystem.TspUserScript.Name = Me._scriptNameTextBox.Text
            Me._tspSystem.TspUserScript.FilePath = Me._tspScriptSelector.FilePath
            If Me._tspSystem.TspUserScript.LoadScriptFile(True, True, Me._retainCodeOutlineToggle.Checked) Then
                listUserScripts()
                Dim synopsis As String = "SCRIPT LOADED"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            Else
                Dim synopsis As String = "FAILED LOADING SCRIPT"
                Me._statusPanel.Text = synopsis
                Me._messagesBox.PrependMessage(Me._tspSystem.TspUserScript.FilePath)
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            End If

        Catch ex As Exception

            ' append the error source
            Dim synopsis As String = "FAILED LOADING SCRIPT"
            Me._messagesBox.PrependException(ex)
            Me._statusPanel.Text = synopsis
            Me._messagesBox.PrependException(Me._statusPanel.Text)

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try


    End Sub

    ''' <summary>
    ''' Updates the tab caption.
    ''' </summary>
    Private Sub _MessagesBox_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _messagesBox.PropertyChanged
        If e.PropertyName = "TabCaption" Then
            Me._tabControl.TabPages.Item(MainTabsIndex.MessagesTabIndex).Text = Me._messagesBox.BuildTabCaption()
        ElseIf e.PropertyName = "TabCaptionFormat" Then
            Me._tabControl.TabPages.Item(MainTabsIndex.MessagesTabIndex).Text = Me._messagesBox.BuildTabCaption()
        ElseIf e.PropertyName = "NewMessagesCount" Then
            If Me._tabControl.SelectedTab Is Me._messagesTabPage Then
                Me._messagesBox.ClearNewMessagesCount()
            End If
            Me._tabControl.TabPages.Item(MainTabsIndex.MessagesTabIndex).Text = Me._messagesBox.BuildTabCaption()
        End If
    End Sub

    ''' <summary>
    ''' Try finding the resources after connecting the instrument.
    ''' </summary>
    Private Sub _refreshVisaButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _refreshVisaButton.Click
        Me.locateVisaResources()
    End Sub

    ''' <summary>
    ''' Update the list of user scripts.
    ''' </summary>
    Private Sub _refreshUserScriptsListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _refreshUserScriptsListButton.Click
        Me.listUserScripts()
    End Sub

    Private Sub _removeScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _removeScriptButton.Click,
                                                                                                                            _removeScriptButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._userScriptsList.SelectedItems.Count > 0 Then
                For Each scriptName As String In Me._userScriptsList.SelectedItems
                    Me._tspSystem.RemoveScript(scriptName)
                    Dim synopsis As String = "REMOVED " & scriptName
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                Next
                listUserScripts()
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred removing script"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _resetLocalNodeButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _resetLocalNodeButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.IsConnected Then

                send("localnode.reset() ", True)

                Dim synopsis As String = "LOCAL NODE RERSET"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                ' update the instrument status.
                Me._tspSystem.TspStatus.ReadAndParseState()

                send("localnode.errorqueue.clear() ", True)

                synopsis = "ERROR QUEUE CLEARED"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

                ' update the instrument status.
                Me._tspSystem.TspStatus.ReadAndParseState()

            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred setting prompts"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _runScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _runScriptButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._tspSystem.TspUserScript.Name = Me._scriptNameTextBox.Text
            If Me._tspSystem.TspUserScript.RunScript(3000) Then
                Dim synopsis As String = "SCRIPT RUN"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            Else
                Dim synopsis As String = "FAILE DRUNNING SCRIPT '" & Me._tspSystem.TspUserScript.Name & "'"
                Me._statusPanel.Text = synopsis
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred running script"""

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _showErrorsCheckBox_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showErrorsCheckBox.CheckStateChanged

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.IsConnected Then
                If Me._tspSystem.TspStatus.ShowErrorsGetter(Visa.ResourceAccessLevels.None) <> Me._showErrorsCheckBox.Checked Then
                    Me._tspSystem.TspStatus.ShowErrorsSetter(Me._showErrorsCheckBox.Checked, Visa.ResourceAccessLevels.None)
                    Dim synopsis As String = "ERROR OFF"
                    If Me._showErrorsCheckBox.Checked Then
                        synopsis = "ERROR ON"
                    End If
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                End If
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred setting error display"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _showPromptsCheckBox_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showPromptsCheckBox.CheckStateChanged

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._tspSystem IsNot Nothing AndAlso Me._tspSystem.IsConnected Then
                If Me._tspSystem.TspStatus.ShowPromptsGetter(Visa.ResourceAccessLevels.None) <> Me._showPromptsCheckBox.Checked Then
                    Me._tspSystem.TspStatus.ShowPromptsSetter(Me._showPromptsCheckBox.Checked, Visa.ResourceAccessLevels.None)
                    Dim synopsis As String = "PROMPTS OFF"
                    If Me._showPromptsCheckBox.Checked Then
                        synopsis = "PROMPTS ON"
                    End If
                    Me._statusPanel.Text = synopsis
                    Me._statusPanel.Invalidate()
                    Me._messagesBox.PrependMessage(Me._statusPanel.Text)
                End If
            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred setting prompts"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _tabControl_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _tabControl.SelectedIndexChanged

        If Me._tabControl.SelectedIndex = MainTabsIndex.MessagesTabIndex Then
            If Me._messagesBox.NewMessagesCount > 0 Then
                Me._messagesBox.ClearNewMessagesCount()
            End If
            Me._tabControl.TabPages.Item(MainTabsIndex.MessagesTabIndex).Text = Me._messagesBox.BuildTabCaption()
        End If

    End Sub

    Private Sub _tspScriptSelector_SelectedChanged(ByVal Sender As System.Object, ByVal e As EventArgs) Handles _tspScriptSelector.SelectedChanged
        If Not String.IsNullOrWhiteSpace(Me._tspScriptSelector.FileTitle) Then
            Me._scriptNameTextBox.Text = Me._tspScriptSelector.FileTitle.Replace(".", "_")
            Dim synopsis As String = "TEST SCRIPT SELECTED"
            Me._statusPanel.Text = synopsis
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)
        End If
    End Sub

#End Region

#Region " INSTRUMENT EVENT HANDLERS "

    Private Sub _tspSystem_ConnectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _tspSystem.ConnectionChanged

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._tspSystem.IsConnected Then
                Me._statusPanel.Text = "CONNECTING....."
            Else
                Me._statusPanel.Text = "DISCONNECTING....."
            End If
            Me._statusPanel.Invalidate()
            Me._messagesBox.PrependMessage(Me._statusPanel.Text)
            Me.openClose(Me._tspSystem.IsConnected)

            ' report status
            If Me.isOpen Then

                Me._statusPanel.Text = "CONNECTED"
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            Else

                Me._statusPanel.Text = "DISCONNECTED"
                Me._statusPanel.Invalidate()
                Me._messagesBox.PrependMessage(Me._statusPanel.Text)

            End If

        Catch ex As Exception

            Dim synopsis As String = "Exception occurred clearing device"

            ' display the exception
            Me._messagesBox.PrependException(ex)
            Me._messagesBox.PrependException(synopsis)

            Me._statusPanel.Text = synopsis

            TraceLogger.WriteExceptionDetails(ex, TraceEventType.Error, Windows.Forms.Application.ProductName & "," & synopsis)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub _tspSystem_MessageAvailable(ByVal sender As Object, ByVal e As EventArgs) Handles _tspSystem.MessageAvailable

        Dim message As String = "TSP System: "
        If String.IsNullOrWhiteSpace(Me._tspSystem.LastMessage) Then
            message &= "Empty"
        Else
            message &= Me._tspSystem.LastMessage
        End If
        Me._messagesBox.PrependMessage(TraceEventType.Verbose, message)
    End Sub

#End Region

#Region " WORKER THREAD HANDLERS "

    ''' <summary>
    ''' This event handler refreshes the VISA resources.
    ''' </summary>
    Private Sub findResourcesHandler(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

        If Not (MyBase.IsDisposed OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
            locateVisaResources()
            'Dim d As New workerCallback(AddressOf Me.findVisaResources)
            'MyBase.Invoke(d, New Object() {})
        End If

    End Sub

    ''' <summary>
    ''' Enables asynchronous calls for handling refresh.
    ''' </summary>
    Private Delegate Sub workerCallback()

#End Region

End Class