Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"

' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.Tsp.Testers", 
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 

<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.Tsp.Testers.My", 
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 

#End Region

#Region "CA1031:DoNotCatchGeneralExceptionTypes"

<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#form_Closed(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_callFunctionButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_runScriptButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#send(System.String,System.Boolean)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_connectToggle_CheckStateChanged(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_removeScriptButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_loadScriptButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_aboutButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_receive(System.Boolean)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_loadAndRunButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_deviceClearButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_showErrorsCheckBox_CheckStateChanged(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_abortButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_showPromptsCheckBox_CheckStateChanged(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_groupTriggerButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_loadFunctionButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_tspSystem_ConnectionChanged(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#listUserScripts()")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_clearInterfaceButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#_resetLocalNodeButton_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.My.MyApplication.#readSettings()")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.My.MyApplication.#instantiateObjects(System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.My.MyApplication.#ProcessException(System.Exception,System.String, isr.WindowsForms.ExceptionDisplayButtons)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.My.MyApplication.#parseCommandLine(System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#form_Load(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#receive(System.Boolean)")> 

#End Region

<Module: SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges", Scope:="member", Target:="isr.Tsp.Testers.TestPanel.#set_isOpen(System.Boolean)")> 