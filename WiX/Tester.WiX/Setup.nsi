; Setup.nsi
;
; A container for the TTM installer.
; Drops a copy of the files into the temp folder and executes setup.
; Can install the .NET framework.
;--------------------------------

Var Message

!Include "${NSISDIR}\Include\LogicLib.nsh"
!Include $%my%\scripts\nsis\!PostExec.nsh
!Include $%my%\scripts\nsis\!GetVersionLocal.nsh

!define MyVer_
!ifdef VersionFile
!else
  !define VersionFile "$%my%\LIBRARIES\VS10\TTM\TTM.3.x\isr.TTM\bin\debug\isr.TTM.exe"
!endif

!insertmacro GetVersionLocal ${VersionFile} MyVer_

!define VERSION "${MyVer_1}.${MyVer_2}.${MyVer_3}"
!define FileVersion "${MyVer_1}.${MyVer_2}.${MyVer_3}.${MyVer_4}"
!define TRADEMARK "TTM"
!define PRODUCT "${TRADEMARK}.2013"
!define DESCRIPTION "Thermal Transient Meter"

!define VisaLink "http://joule.ni.com/nidu/cds/view/p/id/3826/lang/en"
!define VisaVersion "5.3"

; .NET Option
; Must define NETVersion; NETInstaller
!define NETVersion "4.0"
!define NETlabel "Microsoft .NET Framework v${NETVersion}"
!ifdef NETInstaller
  !define FileDescription "${DESCRIPTION} Full Setup"
  !define TITLE "${PRODUCT}.FullSetup"
!else
  !define TITLE "${PRODUCT}.Setup"
  !define FileDescription "${DESCRIPTION} Setup"
!endif

!define AUTHOR "Integrated Scientific Resources, Inc."
!define COMPANY "Integrated.Scientific.Resources"

; The name of the installer
!define NAME "${TITLE}-${VERSION}"
Name "${NAME}"

; The file to write
!define OUTFILE "$%my%\BUILDS\TTM\TTM.2013\Console\${NAME}.exe"
OutFile ${OUTFILE}

; sign the installer after it has been created
${PostExec3} "%my%\PRIVATE\Certificate\SignAssemblyLocalLog.bat" "${TITLE}" ${OUTFILE}

; The default installation directory
InstallDir $Temp\${PRODUCT}

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

;Page directory
;Page instfiles

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;--------------------------------
;Version Information

  VIProductVersion "${VERSION}.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${DESCRIPTION}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${COMPANY}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "${TRADEMARK} is a trademark"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2005 ${AUTHOR}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${FileDescription}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${FileVersion}"

;--------------------------------
; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  SetOverwrite on

  ; .NET Option
  ; Must define NETVersion; NETInstaller
  !Include "${NSISDIR}\Include\DotNetVer.nsh"
  !ifdef NETInstaller
    !Include $%my%\scripts\nsis\DotNetFramework.nsh
  !endif

!ifdef NETInstaller
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			Goto HASDOTNET
		${EndIf}
	${EndIf}
	StrCpy $Message "${NETlabel} did not install. Select OK to allow the installer to download and install ${NETlabel}."
!Else
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			Goto HASDOTNET
		${EndIf}
	${EndIf}
	StrCpy $Message "${NETlabel} not found. Select OK to allow the installer to download and install ${NETlabel}."
!endif

DetailPrint $Message
MessageBox MB_OKCANCEL|MB_ICONQUESTION $Message IDOK okay IDCANCEL cancel
cancel:
	StrCpy $Message "Installation aborted. ${NETlabel} not found."
	DetailPrint $Message
	Abort $Message
okay:
  
HASDOTNET:  

  ; NI-VISA Option
  ; Must define VisaVersion; VisaInstaller; VisaFolder
  !ifdef VisaInstaller
    !Include $%my%\scripts\nsis\NI-VISA.nsh
  !endif
  
  IfFileExists "$PROGRAMFILES\National Instruments\Shared\NI-VISA\niVISAsys.dll" HASNIVISA 0
  !ifdef VisaInstaller
    DetailPrint "NI-VISA v${VisaVersion} did not install."
    MessageBox MB_OK|MB_ICONSTOP \
      "NI-VISA v${VisaVersion} did not install. Try to restart."
  !Else
    DetailPrint "NI-VISA  v${VIsaVersion} not found."
    MessageBox MB_OK|MB_ICONSTOP \
      "National Instruments Visa v${VisaVersion} not found. Download NI VISA from ${VisaLink}."
  !endif
  ; Abort "Installation aborted. National Instruments NI-VISA v${VisaVersion} not found."

HASNIVISA:  

  ; Put file there
  File "debug\setup.exe"
  File "debug\setup.msi"
  Exec "$INSTDIR\setup.exe /l* ${TITLE}.log"

SectionEnd ; end the section
